New Travel Request


Hello,
New Travel Request ({id}) has been submited by {name_accept}

Travel details:
{transp}
Start from: {start_city} ({start_country})	To: {end_city}({end_country})	Date: {start_date}
Return from: {end_city} ({end_country})	To: {start_city}({start_country})	Date: {end_date}
{hotel}
Comments:{comment}

Should You have any questions please contact {name_employee} {email_employee} {tel_employee}

Best Regards,
Travel Team