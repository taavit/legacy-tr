CASH Advancement request


Hello,
{name_employee} has requested cash advance for travel

Travel details:
From: {start_date} To: {end_date}
Destination: {country}
Requested Amount: {value} {currency}
Amount to be paid in {cash}
Please contact {name_employee} {email_employee} {tel_employee} for details

Your,
Travel Team