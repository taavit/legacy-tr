<?php
/// plik zawiera klasę kontrolki 'TPage'
/** @file TPage.php */


///Abstrakcyjna klasa do obsługi strony
        /**Klasa definiująca podstawowe parametry wyświetlanych stron.
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
        @date 17-11-2007
        */
abstract class TPage
{
    ///id aktualnej strony
    protected $page_id;
    ///Nazwa strony do dalszego wyświetlania
    protected $page_name;
    ///Link do danej strony
    protected $page_link;
    ///Kod strony
    protected $page_code;
    ///Grupa mogąca uzyskać dostęp do strony.
    protected $allow;
    ///Id Strony nadrzędnej
    protected $id_parent;
    ///Zmienna odpowiadająca za templatkę powiązaną z konkretną stroną.
    protected $template;
    ///Zmienna trzymająca dostęp do danych
    protected $db;
    ///Zmienna trzymająca użytkownika
    protected $usr;
    ///Obiekt AJAXA;
    protected $xaj;
    
    
    public function __construct()
    {
        $this->db=new MainPDO;
        $this->db->connect();
        $this->usr=$_SESSION['user'];
    }
    ///Metoda zwracająca nazwę strony
    /**
    @return nazwa strony
    */
    
    public function getName()
    {
        return $this->page_name;
    }
    ///Metoda zwracająca id strony nadrzędnej
    /**
    @return id strony nadrzędnej
    */
    public function getId_Parent()
    {
        return $this->id_parent;
    }
        ///Metoda zwracająca idstrony
    /**
    @return id strony
    */
    public function getId()
    {
        return $this->id;
    }
}
