<?php
/// plik zawierający niezbędne do działania funkcje
/** @file commonfunctions.php */


///Zbiór niezbędnych funkcji oraz modułów do prawidłowej pracy systemu
require_once('classes/TTemplate.php');
require_once('classes/TButton.php');
require_once('classes/TPage.php');
require_once('classes/TDataGrid.php');
require_once('classes/TMenuButton.php');
// require_once ('languages/pl.php');
require_once('languages/en.php');
require_once('classes/TAuth.php');
require_once('config.php');
require_once('pdo/mainPDO.class.php');
require_once('smtp/main_mail.class.php');
require_once('xajax.inc.php');

use Taavit\TravelRequest\Controller\MainMenuController;
use Taavit\TravelRequest\Controller\DelegationController;
use Taavit\TravelRequest\Controller\TripDetController;
use Taavit\TravelRequest\Controller\TripFormController;
use Taavit\TravelRequest\Controller\Administration\AdmMenuController;
use Taavit\TravelRequest\Controller\Administration\UsermanController;

///Funkcja realizująca przekierowanie
/**
@param $url adres strony w zdefiniowanym adresie
*/
function redirect($url)
{
    header("Location: ".WWW_HOST_NAME.'/'.$url);
}

///Funkcja wczytująca zadany moduł spod podanej ścieszki
/**
@param $path Ścieżka do katalogu z modułami
@param $modname nazwa modułu do wczytania
@return tresc strony, lub komunikat o błędzie
*/
function LoadnRunMod($path, $modname)
{
    global $db;
    global $twig;
    global $xajax;
    global $request;

    switch ($modname) {
        case 'MainMenu':
            $page = new MainMenuController($_SESSION['user'], $db, $twig);
            return $page->Show();
        case 'delegation':
            $page = new DelegationController($_SESSION['user'], $xajax, $twig);
            return $page->Show();
        case 'tripdet':
            $page = new TripDetController($xajax, $twig);
            return $page->Show($request);
        case 'TripForm':
            $page = new TripFormController($xajax, $twig);
            return $page->show($request);
        case 'adm_menu':
            $page = new AdmMenuController($twig);
            return $page->show();
        case 'userman':
            $page = new UsermanController($twig);
            return $page->show();
    }

    trigger_error("LoadnRunMod used for {$path} {$modname}", E_USER_NOTICE);
    if (file_exists('classes/'.$path.'/'.$modname.'.php')) {
        include('classes/'.$path.'/'.$modname.'.php');
        $act_mod=new $modname;
        $content=$act_mod->Show();
    } else {
        $content=TR_TEXT_ERROR_PAGE_NOT_FOUND;
    }
    return $content;
}


///Funkcja generująca nagłówek podstrony
/**
@return nagłówek danej podstrony
*/
function generateHeader(\Symfony\Component\HttpFoundation\Request $request, $twig)
{
    $db=new MainPDO;
    $db->connect();
    $pagename='';
    $last='';
    $path='';
    $appname=$db->MgetByShortName($request->query->get('app'));
    $link='?appname='.$appname['sshort_name'];
    $path='<a href="'.$link.'">'.$appname['sfull_name'].'</a>';
    if ($request->query->has('page') && $request->query->get('page') !== '') {
        $last=$link;
        $appname=$db->MgetByShortName($request->query->get('page'));
        $link.="&page=".$appname['sshort_name'];
        $path.='->'.'<a href="'.$link.'">'.$appname['sfull_name'].'</a>';
        $pagename=$appname['sfull_name'];
    }

    if ($request->query->has('a') && $request->query->get('a') !== '') {
        $last=$link;
        $appname=$db->MgetByShortName($request->query->get('a'));
        $link.="&a=".$appname['sshort_name'];
        $path.='->'.'<a href="'.$link.'">'.$appname['sfull_name'].'</a>';
        $pagename=$appname['sfull_name'];
    }

    if ($request->query->has('b') && $request->query->get('b') !== '') {
        $last=$link;
        $appname=$db->MgetByShortName($request->query->get('b'));
        $link.="&b=".$appname['sshort_name'];
        $path.='->'.'<a href="'.$link.'">'.$appname['sfull_name'].'</a>';
        $pagename=$appname['sfull_name'];
    }
    return $twig->render(
        'pageheader.html.twig',
        [
            'path' => $path,
            'creator' => '<a href="mailto:'.$db->getMail($appname['id_created_by']).'">'.$db->getName($appname['id_created_by'])."</a>",
            'modifby' => '<a href="mailto:'.$db->getMail($appname['id_modif_by']).'">'.$db->getName($appname['id_modif_by'])."</a>",
            'owner' => '<a href="mailto:'.$db->getMail($appname['id_owner']).'">'.$db->getName($appname['id_owner'])."</a>",
            'cdate' => $appname['dcreation_date'],
            'mdate' => $appname['dmodif_date'],
            'pagename' => $pagename,
            'last' => $last,
            'TR_TEXT_OWNER' => TR_TEXT_OWNER,
            'TR_TEXT_CREATED_BY' => TR_TEXT_CREATED_BY,
            'TR_TEXT_CREATION_DATE' => TR_TEXT_CREATION_DATE,
            'TR_TEXT_CREATED_BY' => TR_TEXT_CREATED_BY,
            'TR_TEXT_MODIFIED_BY' => TR_TEXT_MODIFIED_BY,
        ]
    );
}
