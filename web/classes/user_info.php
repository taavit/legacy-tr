<?php
/// plik zawiera klasę 'user_info'
/** @file user_info.php */



///Klasa strony do zarządzania użytkownikami
/**
Obiekt zawiera stronę z danymi użytkownika. Pozwala także zmienić hasło użytkownika.
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-01-2008
*/

class user_info extends TPage
{
    ///szablon strony
    protected $t;
///Konstruktor ustawiający szablon
    public function __construct()
    {
        parent::__construct();
        $this->t=new TTemplate;
        $this->t->setTplFile(__DIR__.'/../../app/Resources/views/user_info.tpl');
    }
    ///Funkcja zmienia hasło uzytkownika na podstawie tablicy $_POST, komunikat wysyłany jest do zmiennej sesyjnej $_SESSION['msg']
    public function ChangePass()
    {
        if (isset($_POST['pass'])&&$_POST['pass']!=''&& ($_POST['pass']==$_POST['pass2']) && isset($_POST['id'])) {
            $w= $this->db->setPassw($_POST['id'], md5($_POST['pass']), $this->usr->getId_logged());
            if ($w==0) {
                $_SESSION['err']=TR_TEXT_CHANGE_PASS_FAILED;
            } else {
                $_SESSION['err']=TR_TEXT_CHANGE_PASS_SUCCESS;
            }
        } else {
            $_SESSION['err']=TR_TEXT_CHANGE_PASS_NOT_THE_SAME;
        }
        redirect("index2.php?appname=travreq");
    }
///Funkcja generująca kod strony
/**
@return kod strony
*/
    public function show()
    {
        if (!isset($_GET['a'])) {
            $_GET['a']='';
        }
        switch ($_GET['a']) {
        case 'chpass':$output=$this->ChangePass();break;
        default:
        $this->t->addRepVar('TR_TEXT_USER_FORENAME2', TR_TEXT_USER_FORENAME2);
        $this->t->addRepVar('TR_TEXT_USER_SURNAME2', TR_TEXT_USER_SURNAME2);
        $this->t->addRepVar('TR_TEXT_USER_MAIL', TR_TEXT_USER_MAIL);
        $this->t->addRepVar('TR_TEXT_USER_PHONE', TR_TEXT_USER_PHONE);
        $emp=$this->db->getEmployee($this->usr->getId_logged());
        $this->t->addRepVar('{forename}', $emp->getForename());
        $this->t->addRepVar('{surname}', $emp->getSurname());
        $this->t->addRepVar('{mail}', $emp->getMail());
        $this->t->addRepVar('{phone}', $emp->getPhone());
        $chpassbox=new TTemplate;
        $chpassbox->setTplFile(__DIR__.'/../../app/Resources/views/chpassbox.tpl');
        

        $chpassbut=new TButton;
        $chpassbut->setBType('submit');
        $chpassbut->setCaption(TR_TEXT_USER_CHPASS);
        $chpassbut->setWidth(150);
        $chpassbox->addRepVar('{chpass}', $chpassbut->Show());
        $chpassbox->addRepVar('{id}', $emp->getId());
        $chpassbox->addRepVar('TR_TEXT_USER_PASS2', TR_TEXT_USER_PASS2);
        $chpassbox->addRepVar('TR_TEXT_USER_PASS', TR_TEXT_USER_PASS);
        $chpassbox->prepare();
        $this->t->addRepVar('{chpassbox}', $chpassbox->getOutputText());
        $this->t->prepare();
        return $this->t->getOutputText();}
    }
}
