<?php
/// plik zawiera klasę 'acceptator'
/** @file acceptator.php */

///Klasa zawierająca stronę zarządzania delegacjami osób podlegających
/**
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 12-11-2007
*/

class acceptator extends TPage
{
    public function __construct()
    {
        parent::__construct();
        global $xajax;
        $xajax->registerFunction('X_AcceptDelegation');
        $xajax->registerFunction('X_ShowDelGrid');
    }
    public function Show()
    {
        if (!isset($_GET['a'])) {
            $_GET['a']="";
        }
        switch ($_GET['a']) {
case 'tripdet': return LoadnRunMod('', 'tripdet');
default:
        $tpl=new TTemplate();
        $tpl->setTplFile(__DIR__.'/../../app/Resources/views/accept.tpl');
        $tpl->addRepVar('{grid}', ShowDelBox(0));
        $tpl->addRepVar('{filtres}', generateFilters());
        $tpl->addRepVar('{pagesbottom}', generatePages());
        $tpl->addRepVar('{pagestop}', generatePages());
        $tpl->prepare();
        return $tpl->getOutputText();
    }
    }
}
///Metoda generująca tabelę z delegacjami
/**
@return string zawierający wygenerowaną tabelkę
@param $st id od którego zaczynamy pobierać
@param $status tablica zawierająca maskę statusów delegacji do pobrania
*/
function ShowDelBox($st=0, $status=array(0,1,1,1,1,0,0))
{
    $db=new MainPDO;
    $db->connect();
    $trips=array();
    $grid=new TDataGrid('travels');
    $trips=$db->getSomeTripByTStatusAndAccept($status, $_SESSION['user']->getId_logged(), CS_ROWS_PER_PAGE, $st);
    $buttons=array();
    $headers[]=TR_TEXT_ID;
    $headers[]=TR_TEXT_USER_EMPLOYEE;
    $headers[]=TR_TEXT_CITY_FROM;
    $headers[]=TR_TEXT_COUNTRY_FROM;
    $headers[]=TR_TEXT_CITY_TO;
    $headers[]=TR_TEXT_COUNTRY_TO;
    $headers[]=TR_TEXT_TRIP_START_DATE;
    $headers[]=TR_TEXT_TRIP_END_DATE;
    $headers[]=TR_TEXT_TRIP_STATUS;
    $headers[]=TR_TEXT_TRIP_PURPOSE;
    $data=array();
    $ids;
    $i=0;
    foreach ($trips as $trip) {
        $data[$i][]='tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT);
        $city=$db->getCity($trip->getIdCity());
        $data[$i][]=$db->getName($trip->getIdEmployee());
        $data[$i][]=$city->getName();
        $data[$i][]=$db->getCountry($city->getIdCountry())->getName();
        $city=$db->getCity($trip->getIdStartCity());
        $data[$i][]=$city->getName();
        $data[$i][]=$db->getCountry($city->getIdCountry())->getName();
        $button=new TButton;
        $button->setCaption(TR_TEXT_BUTTON_DETAILS);
        $button->setAction('window.location="index2.php?page=acceptator&a=tripdet&id='.$trip->getId().'"');
        $button->setWidth(70);
        $buttons[$i][]=$button;
        $data[$i][]=$trip->getStartDate();
        $data[$i][]=$trip->getEndDate();
        switch ($trip->getStatus()) {
                case 0:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_0;
                        break;
                case 1:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_1;
                        break;
                case 2:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_2;
                        break;
                case 3:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_3;
                        break;
                case 4:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_4;
                        break;
                case 5:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_5;
                        break;
                case 6:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_6;
                        break;
            }
        $data[$i][]=$db->getPurp($trip->getIdPurp())->getName();

        $ids[]=$trip->getId();
        $i++;
    }

    $grid->setHeaders($headers);
    $grid->setData($data);
    $grid->setButtons($buttons);
    return $grid->Show();
}
///Metoda generująca filtry
/**
@return string zawierający wygenerowany kod opcji filtrowania
*/
function generateFilters()
{
    $db=new MainPDO;
    $db->connect();
    $content='<div id="filters">';
    $content.='<input type="checkbox" name="s1" id="s1" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_1;
    $content.='<input type="checkbox" name="s2" id="s2" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_2;
    $content.='<input type="checkbox" name="s3" id="s3" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_3;
    $content.='<input type="checkbox" name="s4" id="s4" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_4;
    $content.='<input type="checkbox" name="s5" id="s5" onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_5;
    $content.='<input type="checkbox" name="s6" id="s6" onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_6;
    $content.='</div>';
    return $content;
}

///Metoda indeksująca strony z danymi
/**
@param $status maska aktywnych filtrów
@param $act aktualnie wybrana strona
@return string zawierający wygenerowany kod zawierający numery stron, oraz linki do nich
*/

function generatePages($status=array(0,1,1,1,1,0,0), $act=0)
{
    $db=new MainPDO;
    $db->connect();

    $ap=0;
    
    $act=ceil($act/CS_ROWS_PER_PAGE);
    $cpages=ceil($db->countTripByTStatusAndAccept($status, $_SESSION['user']->getId_logged())/ CS_ROWS_PER_PAGE);
    $links='<span class="pageslist" onclick="xajax_X_ShowDelGrid(0,generateFilter());">'. TR_TEXT_PAGE_FIRST .' </span>';
    $prev=$act-1;
    if ($act!=0) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.$prev*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    }
    
    if ($act<5) {
        $beg=0;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $beg=$act-4;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $links.=' ...';
    }
    for ($i=$beg;$i<$end;$i++) {
        $ap=$i+1;
        $active='';
        if ($i==$act) {
            $active='style="font-weight:bolder;text-decoration:underline;"';
        } else {
            $active='';
        }
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('. $i*CS_ROWS_PER_PAGE .',generateFilter());"'.$active.' >'. $ap .' </span>';
    }
    $next=$act+1;
    if ($act<$cpages-5) {
        $links.='... ';
    }
    if ($act!=$cpages-1) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.$next*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_NEXT .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_NEXT .' </span>';
    }
    $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.($cpages-1)*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_LAST .' </span>';
    return $links;
}

///Xajaxowa funkcja odświerzająca tabelę z delegacjami
/**
@return kod xml zawierający dane dla xajaxa
@param $st pierwsza pozycja w bazie od której zaczynamy
@param $status maska filtrów
*/
function X_ShowDelGrid($st, $status)
{
    $obj=new XajaxResponse;
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}

///Xajaxowa funkcja akceptująca delegację
/**
@return kod xml zawierający dane dla xajaxa
@param $id id delegacji do zaakceptowania
*/
function X_AcceptDelegation($id)
{
    $obj=new XajaxResponse;
    $db=new MainPDO;
    $db->connect();
    $trip=$db->getTrip($id);
    if ($_SESSION['user']->getIsAdmin() && $trip->getStatus()!=0) {
        $db->closeTrip($id, $_SESSION['user']->getId_logged());
    }
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}
