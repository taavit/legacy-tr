<?php
/// plik zawiera klasę 'permissions'
/** @file permissions.php */


///Klasa zawierająca stronę z edycją uprawnień
/**
Obiekt zawiera stronę służącą do zarządzania uprawnieniami użytkowników
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/

class permissions extends Tpage
{
    ///Konstruktor rejestrujący niezbędne funkcje xajaxowe
    public function __construct()
    {
        global $xajax;
        $xajax->registerFunction('X_AddAdm');
        $xajax->registerFunction('X_DelAdm');
        $xajax->registerFunction('X_AddAcc');
        $xajax->registerFunction('X_DelAcc');
    }
    
///Funkcja generująca stronę z uprawnieniami
/**
@return kod wygenerowanej strony zarządzania uprawnieniami
*/

public function Show()
{
    $tpl=new TTemplate();
    $tpl->setTplFile(__DIR__.'/../../../app/Resources/views/administration/permissions.tpl');
    $tpl->addRepVar('{NAUL}', ListUsersNAdmin());
    $tpl->addRepVar('{AUL}', ListAdmins());
    $tpl->addRepVar('{NCUL}', ListUsersNAccept());
    $tpl->addRepVar('{CUL}', ListAccepts());
    $tpl->addRepVar('TR_TEXT_ACCEPTATORS', TR_TEXT_ACCEPTATORS);
    $tpl->addRepVar('TR_TEXT_ADMINISTRATORS', TR_TEXT_ADMINISTRATORS);
    $addadm=new TButton;
    $deladm=new TButton;
    $addacc=new TButton;
    $delacc=new TButton;
    $delacc->setWidth(75);
    $deladm->setWidth(75);
    $addacc->setWidth(75);
    $addadm->setWidth(75);
    $addadm->setCaption('Add >>');
    $addacc->setCaption('Add >>');
    
    $deladm->setCaption('<< Remove');
    $delacc->setCaption('<< Remove');
    
    $addadm->setAction('xajax_X_AddAdm(document.getElementById("listuadm").options[document.getElementById("listuadm").selectedIndex].value)');
    $addacc->setAction('xajax_X_AddAcc(document.getElementById("listuacc").options[document.getElementById("listuacc").selectedIndex].value)');
    
    $deladm->setAction('xajax_X_DelAdm(document.getElementById("listAdm").options[document.getElementById("listAdm").selectedIndex].value)');
    $delacc->setAction('xajax_X_DelAcc(document.getElementById("listAc").options[document.getElementById("listAc").selectedIndex].value)');
    
    $tpl->addRepVar('{ADMT}', $addadm->Show().'<br />'.$deladm->Show());
    $tpl->addRepVar('{ACMT}', $addacc->Show().'<br />'.$delacc->Show());
    $tpl->addRepVar('TR_TEXT_ADM_ADMINISTRATORS', TR_TEXT_ADM_ADMINISTRATORS);
    $tpl->addRepVar('TR_TEXT_ADM_ACCEPTATORS', TR_TEXT_ADM_ACCEPTATORS);
    
    $tpl->addRepVar('TR_TEXT_NON_ADMIN_LIST', TR_TEXT_NON_ADMIN_LIST);
    $tpl->addRepVar('TR_TEXT_NON_ACC_LIST', TR_TEXT_NON_ACC_LIST);
    $tpl->addRepVar('TR_TEXT_ADMINS_LIST', TR_TEXT_ADMINS_LIST);
    $tpl->addRepVar('TR_TEXT_ACC_LIST', TR_TEXT_ACC_LIST);
    $tpl->prepare();
    return $tpl->getOutputText();
}
}

///Funkcja listująca użytkowników niebędących administratorem
/**
@return kod listy użytkowników nie będących administratorami
*/
function ListUsersNAdmin()
{
    $db=new MainPDO;
    $db->connect();
    $ena=array();
    $z;
    $emps=$db->getAllEmployees(true);
    foreach ($emps as $emp) {
        if ($db->isAdmin($emp->getId(), $z)) {
            if ($z==2) {
                $ena[]=$emp->getId();
            }
        } else {
            $ena[]=$emp->getId();
        }
    }
    $wyn='<select id="listuadm" size="10">';
    foreach ($ena as $em) {
        $wyn.='<option value='.$em.'>'.$db->getName($em).'</option>';
    }
    $wyn.="</select>";
    return $wyn;
}

///Funkcja listująca użytkowników niebędących osobą akceptującą
/**
@return kod listy użytkowników nie będących osobami akceptującymi
*/

function ListUsersNAccept()
{
    $db=new MainPDO;
    $db->connect();
    $z=0;
    $emps=$db->getAllEmployees(true);
    $ena=array();
    foreach ($emps as $emp) {
        if (!$db->isActiveAccept($emp->getId(), $z)) {
            $ena[]=$emp->getId();
        }
    }
    $wyn='<select id="listuacc"  size="10">';
    foreach ($ena as $em) {
        $wyn.='<option value='.$em.'>'.$db->getName($em).'</option>';
    }
    $wyn.="</select>";
    return $wyn;
}


///Funkcja listująca użytkowników będących osobami akceptującymi
/**
@return kod listy użytkowników będących osobami akceptującymi
*/

function ListAccepts()
{
    $db=new MainPDO;
    $db->connect();
    $wyn='<select id="listAc"  size="10">';

    foreach ($db->getAllAccept(true) as $em) {
        $wyn.='<option value='.$em->getId().'>'.$db->getName($em->getId()).'</option>';
    }
    $wyn.="</select>";
    return $wyn;
}

///Funkcja listująca użytkowników będącymi administratorami
/**
@return kod listy użytkowników będącymi administratorami
*/

function ListAdmins()
{
    $db=new MainPDO;
    $db->connect();
    $wyn='<select id="listAdm"  size="10">';
    foreach ($db->getAllAdmins(true) as $em) {
        $wyn.='<option value='.$em.'>'.$db->getName($em).'</option>';
    }
    $wyn.="</select>";
    return $wyn;
}
///Funkcja dodająca administratora
/**
@param $id id użytkownika który ma zostać administratorem
@return kod xml dla xajaxa z zmienionymi listami administratorów i nieadministratorów
*/

function X_AddAdm($id)
{
    $db=new MainPDO;
    $db->connect();
    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->addAdmin($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new XajaxResponse();
    $obj->addAssign('Adminslist', 'innerHTML', ListAdmins());
    $obj->addAssign('UserNad', 'innerHTML', ListUsersNAdmin());
    return $obj->getXML();
}

///Funkcja dodająca osobę akceptującą
/**
@param $id id użytkownika który ma zostać osobą akceptującą
@return kod xml dla xajaxa z zmienionymi listami akceptatorów i nieakceptatorów
*/

function X_AddAcc($id)
{
    $db=new MainPDO;
    $db->connect();
    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->addAccept($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new XajaxResponse();
    $obj->addAssign('Acclist', 'innerHTML', ListAccepts());
    $obj->addAssign('UserNac', 'innerHTML', ListUsersNAccept());
    return $obj->getXML();
}

///Funkcja usuwająca administratora
/**
@param $id id użytkownika który ma zostać nieadministratorem
@return kod xml dla xajaxa z zmienionymi listami administratorów i nieadministratorów
*/

function X_DelAdm($id)
{
    $db=new MainPDO;
    $db->connect();
    $blah;
    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->delAdmin($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new XajaxResponse();
    $obj->addAssign('Adminslist', 'innerHTML', ListAdmins());
    $obj->addAssign('UserNad', 'innerHTML', ListUsersNAdmin());
    return $obj->getXML();
}


///Funkcja usuwająca osobę akceptującą
/**
@param $id id użytkownika który ma przestać być osobą akceptującą
@return kod xml dla xajaxa z zmienionymi listami akceptatorów i nieakceptatorów
*/

function X_DelAcc($id)
{
    $db=new MainPDO;
    $db->connect();
    $blah;
    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->delAccept($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new XajaxResponse();
    $obj->addAssign('Acclist', 'innerHTML', ListAccepts());
    $obj->addAssign('UserNac', 'innerHTML', ListUsersNAccept());
    return $obj->getXML();
}
