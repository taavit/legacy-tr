<?php
/// plik zawiera klasę 'delegationman'
/** @file delegationman.php */



///Klasa generująca stronę zarządzania delegacjami, oraz wykonująca operacje na delegacjach
/**
Obiekt zawiera stronę do zarządzania delegacjami. Pozwala zamykanie delegacji.
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/

class delegationman extends TPage
{
    private $page;
    ///Konstruktor rejestrujący funkcje xajaxowe
    public function __construct()
    {
        parent::__construct();
        global $xajax;
        $xajax->registerFunction('X_ShowDelGrid');
        $xajax->registerFunction('X_CloseDelegation');
        isset($_GET['page']) ? $this->page=1 : $this->page=$_GET['page'];
    }
    ///funkcja generująca kod strony zarządzania delegacjiami z poziomu administratora
    /**
    @return kod strony zarządzania delegacjami
    */
    public function Show()
    {
        if (!isset($_GET['b'])) {
            $_GET['b']='';
        }
        switch ($_GET['b']) {
        case 'tripdet': return LoadnRunMod('', 'tripdet');
        default:    $tpl=new TTemplate();
                $tpl->setTplFile(__DIR__.'/../../../app/Resources/views/administration/delegation.tpl');
                $tpl->addRepVar('{grid}', ShowDelBox(0));
                $tpl->addRepVar('{filtres}', generateFilters());
                $tpl->addRepVar('{pagesbottom}', generatePages());
                $tpl->addRepVar('{pagestop}', generatePages());
                $tpl->prepare();
                return $tpl->getOutputText();
        }
    }
}
///Generowanie tabelki z delegacjami według określonego filtra
/**
@param $st id pierwszej wyświetlanej delegacji
@param $status maska filtrów wyświetlanych delegacji
*/
function ShowDelBox($st=0, $status=array(0,1,1,1,1,0,0))
{
    $db=new MainPDO;
    $db->connect();
    $trips=array();
    $grid=new TDataGrid('travels');
    $trips=$db->getSomeTripByTStatusAndUser($status, CS_ROWS_PER_PAGE, $st, null);
    $buttons=array();
    $headers[]=TR_TEXT_ID;
    $headers[]=TR_TEXT_USER_EMPLOYEE;
    $headers[]=TR_TEXT_CITY_FROM;
    $headers[]=TR_TEXT_COUNTRY_FROM;
    $headers[]=TR_TEXT_CITY_TO;
    $headers[]=TR_TEXT_COUNTRY_TO;
    $headers[]=TR_TEXT_TRIP_START_DATE;
    $headers[]=TR_TEXT_TRIP_END_DATE;
    $headers[]=TR_TEXT_TRIP_STATUS;
    $headers[]=TR_TEXT_TRIP_PURPOSE;

    $data=array();
    $ids;
    $i=0;
    foreach ($trips as $trip) {
        $data[$i][]='tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT); //id
            $data[$i][]=$db->getName($trip->getIdEmployee()); //pracownik
            $city=$db->getCity($trip->getIdCity());
        $data[$i][]=$city->getName(); //cityfrom
            $data[$i][]=$db->getCountry($city->getIdCountry())->getName(); //countryfrom
            $city=$db->getCity($trip->getIdStartCity());
        $data[$i][]=$city->getName(); //cityto
            $data[$i][]=$db->getCountry($city->getIdCountry())->getName();//countryto
            $button=new TButton;
        $button->setCaption(TR_TEXT_BUTTON_DETAILS);
        $button->setAction('window.location="index2.php?page=administration&a=delegationman&b=tripdet&id='.$trip->getId().'"');
        $button->setWidth(70);
        $buttons[$i][]=$button;
        if ($trip->getStatus()<5) {
            $button2=new TButton;
            $button2->setCaption(TR_TEXT_BUTTON_CLOSE);
            $button2->setAction('xajax_X_CloseDelegation('.$trip->getId().',generateFilter())');
            $button2->setWidth(70);
            $buttons[$i][]=$button2;
        }
        $data[$i][]=$trip->getStartDate();//startdate
            $data[$i][]=$trip->getEndDate();//enddate
            switch ($trip->getStatus()) {
                case 0:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_0;
                        break;
                case 1:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_1;
                        break;
                case 2:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_2;
                        break;
                case 3:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_3;
                        break;
                case 4:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_4;
                        break;
                case 5:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_5;
                        break;
                case 6:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_6;
                        break;
            }//status
            $data[$i][]=$db->getPurp($trip->getIdPurp())->getName();//purpose

            $ids[]=$trip->getId();
        $i++;
    }

    $grid->setHeaders($headers);
    $grid->setData($data);
    $grid->setButtons($buttons);
    return $grid->Show();
}


///Generowanie pól z filtrami dla tabelki z delegacjami
/**
@return kod filtrów
*/
function generateFilters()
{
    $db=new MainPDO;
    $db->connect();
    $content='<div id="filters">';
    $content.='<input type="checkbox" name="s1" id="s1" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_1;
    $content.='<input type="checkbox" name="s2" id="s2" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_2;
    $content.='<input type="checkbox" name="s3" id="s3" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_3;
    $content.='<input type="checkbox" name="s4" id="s4" checked onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_4;
    $content.='<input type="checkbox" name="s5" id="s5" onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_5;
    $content.='<input type="checkbox" name="s6" id="s6" onclick="xajax_X_ShowDelGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_6;
    $content.='</div>';
    return $content;
}

///Generowanie listy stron
/**
@param $status tablica filtrów
@param $act aktualna strona
@return kod listy stron, z zaznaczeniem aktualnego
*/
function generatePages($status=array(0,1,1,1,1,0,0), $act=0)
{
    $db=new MainPDO;
    $db->connect();
    $ap=0;
    $act=ceil($act/CS_ROWS_PER_PAGE);
    $cpages=ceil($db->countTripByTStatusAndUser($status)/ CS_ROWS_PER_PAGE);
    $links='<span class="pageslist" onclick="xajax_X_ShowDelGrid(0,generateFilter());">'. TR_TEXT_PAGE_FIRST .' </span>';
    $prev=$act-1;
    if ($act!=0) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.$prev*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    }
    
    if ($act<5) {
        $beg=0;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $beg=$act-4;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $links.='<span class="pageslist"> ...</span>';
    }
    for ($i=$beg;$i<$end;$i++) {
        $ap=$i+1;
        $active='';
        if ($i==$act) {
            $active='class="activePage"  style="font-weight:bolder;text-decoration:underline;"';
        } else {
            $active='class="nonActivePage"';
        }
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('. $i*CS_ROWS_PER_PAGE .',generateFilter());"'.$active.' >'. $ap .' </span>';
    }
    $next=$act+1;
    if ($act<$cpages-5) {
        $links.='<span class="pageslist"> ...</span>';
    }
    if ($act!=$cpages-1) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.$next*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_NEXT .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_NEXT .' </span>';
    }
    $links.='<span class="pageslist" onclick="xajax_X_ShowDelGrid('.($cpages-1)*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_LAST .' </span>';
    return $links;
}

///Xajaxowa funkcja generująca tabelkę delegacji
/**
@param $st pierwsza delegacja wyświetlana
@param $status filtr wyświetlanych delegacji
@return kod xml dla xajaxa z tabelką
*/
function X_ShowDelGrid($st, $status)
{
    $obj=new XajaxResponse;
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}

///Xajaxowa funkcja zamykająca delegacje i generująca tabelkę delegacji
/**
@param $id id zamykanej delegacji
@param $status filtr wyświetlanych delegacji
@return kod xml dla xajaxa z tabelką po zamknięciu
*/

function X_CloseDelegation($id, $status)
{
    $obj=new XajaxResponse;
    $db=new MainPDO;
    $db->connect();
    $trip=$db->getTrip($id);
    $st=0;
    if ($_SESSION['user']->getIsAdmin() && $trip->getStatus()!=0) {
        $db->closeTrip($id, $_SESSION['user']->getId_logged());
    }
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}
