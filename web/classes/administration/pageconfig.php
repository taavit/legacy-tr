<?php
/// plik zawiera klasę 'pageconfig'
/** @file pageconfig.php */

use Taavit\TravelRequest\Model\Hotel;
use Taavit\TravelRequest\Model\City;
use Taavit\TravelRequest\Model\Currency;
use Taavit\TravelRequest\Model\Country;
use Taavit\TravelRequest\Model\Purpose;
use Taavit\TravelRequest\Model\Transport;

///Klasa strony z ustawieniami parametrów systemu
/**
Obiekt zawiera stronę z konfiguracja systemu (miasta, państwa, hotele, waluty, cele, transport)
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-01-2008
*/
class pageconfig extends TPage
{
    ///Rejestrowanie funkcji xajaxowych
    public function __construct()
    {
        parent::__construct();
        global $xajax;
        $xajax->registerFunction("X_ShowCurrencies");
        $xajax->registerFunction("X_ShowCities");
        $xajax->registerFunction("X_ShowCountries");
        $xajax->registerFunction("X_ShowHotels");
        $xajax->registerFunction("X_ShowTransport");
        $xajax->registerFunction("X_ShowAims");
        
        $xajax->registerFunction("X_ModifyCurrency");
        $xajax->registerFunction("X_ModifyCity");
        $xajax->registerFunction("X_ModifyCountry");
        $xajax->registerFunction("X_ModifyHotel");
        $xajax->registerFunction("X_ModifyTransport");
        $xajax->registerFunction("X_ModifyAim");
        
        $xajax->registerFunction("X_DelCurrency");
        $xajax->registerFunction("X_DelCity");
        $xajax->registerFunction("X_DelCountry");
        $xajax->registerFunction("X_DelHotel");
        $xajax->registerFunction("X_DelTransport");
        $xajax->registerFunction("X_DelAim");
        
        $xajax->registerFunction("X_DDShowCities");
    }
    
    ///Generowanie strony
    /**
    @return kod strony
    */
    public function Show()
    {
        $tpl=new TTemplate;
        $currency=ShowCurrenciesBox(-1, 0);
        $aims=ShowAimsBox(-1, 0);
        $tpl->setTplFile(__DIR__.'/../../../app/Resources/views/administration/pageconfig/main.tpl');

        $tpl->addRepVar('TR_TEXT_TRIP_PURPOSES', TR_TEXT_TRIP_PURPOSES);
        $tpl->addRepVar('TR_TEXT_TRANSPORTS', TR_TEXT_TRANSPORTS);
        $tpl->addRepVar('TR_TEXT_HOTELS', TR_TEXT_HOTELS);
        $tpl->addRepVar('TR_TEXT_COUNTRIES', TR_TEXT_COUNTRIES);
        $tpl->addRepVar('TR_TEXT_CITIES', TR_TEXT_CITIES);
        $tpl->addRepVar('TR_TEXT_CURRENCIES', TR_TEXT_CURRENCIES);
        $tpl->addRepVar('{Grid}', $currency);
        $tpl->prepare();
        return $tpl->getOutputText();
    }
}

///funkcja generująca tabelkę z celami podróży
/**
@param $id id edytowanego celu
@param $addfield czy ma zostać dodany wiersz dodawania celu
@return kod tabelki z celami podróży
*/
function ShowAimsBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $aims=$db->getAllPurp(true);
    $tableob=new TDataGrid('AimsGrid', 800, 10);
    $ids=array();
    $data;
    $i=0;
    $zm;
    $headersp[]=TR_TEXT_TRIP_PURPOSE;
    $headersp[]=TR_TEXT_CREATED_BY;
    $headersp[]=TR_TEXT_CREATION_DATE;
    $headersp[]=TR_TEXT_MODIFIED_BY;
    $headersp[]=TR_TEXT_MODIFICATION_DATE;
    $buttons;
        
    $columns[]='130px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    if ($addfield==1) {
        $id++;
    }
    if ($aims!=null) {
        foreach ($aims as $purpose) {
            $modbut=new TButton;
            $delbut=new TButton;
            
            $modbut->setName('AimsGridMB'.$purpose->getId());
            
            if ($purpose->getId()==$id && $addfield==0) {
                $data[$i][]='<input type="text" id="modaim" value="'.$purpose->getName().'" onenter="xajax_X_ModifyAim('.$purpose->getId().',document.getElementById("modaim").value)" class="edit"/>';
                $modbut->setAction('xajax_X_ModifyAim('.$purpose->getId().',document.getElementById("modaim").value)');
                $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
                $delbut->setAction('xajax_X_ShowAims(-1,0)');
                $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
            } else {
                $data[$i][]=$purpose->getName();
                $modbut->setCaption(TR_TEXT_BUTTON_MOD);
                $modbut->setAction('xajax_X_ShowAims('.$purpose->getId().')');
                $delbut->setAction('xajax_X_DelAim('.$purpose->getId().')');
                $delbut->setCaption(TR_TEXT_BUTTON_DEL);
            }
            $data[$i][]=$db->getName($db->getPurpInfo($purpose->getId())->getCreatedBy());
            $data[$i][]=$db->getPurpInfo($purpose->getId())->getCreationDate();
            $data[$i][]=$db->getName($db->getPurpInfo($purpose->getId())->getModifBy());
            $data[$i][]=$db->getPurpInfo($purpose->getId())->getModifDate();
            $modbut->setWidth(50);
            $delbut->setWidth(50);
            $buttons[$i][]=$modbut;
            $buttons[$i][]=$delbut;
            $ids[]=$purpose->getId();
            $i++;
        }
    }
    if ($i==0) {
        $addfield=1;
    }
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="modaim" value="" class="edit"/>';
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('AimGridMB-2');
        $modbut->setAction('xajax_X_ModifyAim(-2,document.getElementById("modaim").value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $modbut->setWidth(50);
        $delbut=new TButton;
        $delbut->setWidth(50);
        $delbut->setName('AimGridDB-2');
        $delbut->setAction('xajax_X_ShowAims(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    if (isset($buttons)) {
        $tableob->setButtons($buttons);
    }
    if (isset($ids)) {
        $tableob->setIndexes($ids);
    }
    $tableob->setHeaders($headersp);
    if (isset($data)) {
        $tableob->setData($data);
    }
    if ($addfield==1) {
        $zm=-1;
    } else {
        $zm=$ids[count($ids)-1];
    }
    return $tableob->Show().'<p><input type="button" name="addAimB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowAims('.$zm.',1)" class="button"/>';
}
 
///funkcja generująca tabelkę z walutami
/**
@param $id id edytowanej waluty
@param $addfield czy ma zostać dodany wiersz dodawania waluty
@return kod tabelki z walutami
*/
function ShowCurrenciesBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $currencies=$db->getAllCurr(true);
    $tableob=new TDataGrid('CurrGrid', 800, 10);
    $ids=array();
    $data;
    $i=0;
    $zm;
    $headersp[]=TR_TEXT_CURRENCY;
    $headersp[]=TR_TEXT_CREATED_BY;
    $headersp[]=TR_TEXT_CREATION_DATE;
    $headersp[]=TR_TEXT_MODIFIED_BY;
    $headersp[]=TR_TEXT_MODIFICATION_DATE;
    $buttons;
        
    $columns[]='130px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    if ($addfield==1) {
        $id++;
    }
    if ($currencies!=null) {
        foreach ($currencies as $curr) {
            $modbut=new TButton;
            $delbut=new TButton;
            
            $modbut->setName('CurrGridMB'.$curr->getId());
            
            if ($curr->getId()==$id && $addfield==0) {
                $data[$i][]='<input type="text" id="modcurr" value="'.$curr->getName().'" class="edit"/>';
                $modbut->setAction('xajax_X_ModifyCurrency('.$curr->getId().',document.getElementById("modcurr").value)');
                $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
                $delbut->setAction('xajax_X_ShowCurrencies(-1,0)');
                $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
            } else {
                $data[$i][]=$curr->getName();
                $modbut->setCaption(TR_TEXT_BUTTON_MOD);
                $modbut->setAction('xajax_X_ShowCurrencies('.$curr->getId().')');
                $delbut->setAction('xajax_X_DelCurrency('.$curr->getId().')');
                $delbut->setCaption(TR_TEXT_BUTTON_DEL);
            }
            $modbut->setWidth(50);
            $delbut->setWidth(50);
            $data[$i][]=$db->getName($db->getCurrInfo($curr->getId())->getCreatedBy());
            $data[$i][]=$db->getCurrInfo($curr->getId())->getCreationDate();
            $data[$i][]=$db->getName($db->getCurrInfo($curr->getId())->getModifBy());
            $data[$i][]=$db->getCurrInfo($curr->getId())->getModifDate();
            $buttons[$i][]=$modbut;
            $buttons[$i][]=$delbut;
            $ids[]=$curr->getId();
            $i++;
        }
    }
    if ($i==0) {
        $addfield=1;
    }
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="modcurr" value="" class="edit"/>';
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('CurrGridMB-2');
        $modbut->setAction('xajax_X_ModifyCurrency(-2,document.getElementById("modcurr").value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $delbut=new TButton;
        $delbut->setName('CurrGridDB-2');
        $delbut->setAction('xajax_X_ShowCurrencies(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    if (isset($buttons)) {
        $tableob->setButtons($buttons);
    }
    if (isset($ids)) {
        $tableob->setIndexes($ids);
    }
    $tableob->setHeaders($headersp);
    if (isset($data)) {
        $tableob->setData($data);
    }
    if ($addfield==1) {
        $zm=-1;
    } else {
        $zm=$ids[count($ids)-1];
    }
    return $tableob->Show().'<p><input type="button" name="addcurrB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowCurrencies('.$zm.',1)" class="button"/>';
}


///funkcja generująca tabelkę z miastami
/**
@param $id id edytowanego miasta
@param $addfield czy ma zostać dodany wiersz dodawania miasta
@return kod tabelki z miastami
*/

function ShowCitiesBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $cities=$db->getAllCities(true);
    $tableob=new TDataGrid('CityGrid', 800, 10);
    $ids;
    $data;
    $i=0;
    $buttons;
    $headers=array();
    $headers[]=TR_TEXT_CITY;
    $headers[]=TR_TEXT_COUNTRY;
    $headers[]=TR_TEXT_CREATED_BY;
    $headers[]=TR_TEXT_CREATION_DATE;
    $headers[]=TR_TEXT_MODIFIED_BY;
    $headers[]=TR_TEXT_MODIFICATION_DATE;
        
    $columns[]='100px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    $columns[]='100px';
    $columns[]='100px';
        
    foreach ($cities as $city) {
        $modbut=new TButton;
        $delbut=new TButton;
            
        $modbut->setName('CityGridMB'.$city->getId());
        $delbut->setName('CityGridDB'.$city->getId());
            
        if ($city->getId()==$id && $addfield==0) {
            $data[$i][]='<input type="text" id="modctname" value="'.$city->getName().'" style="width:80px" class="edit"/>';
            $clist='<select id="modctcountry" class="edit">';
            foreach ($db->getAllCountries() as $country) {
                $clist.='<option value='.$country->getId().' ';
                if ($country->getId()==$city->getIdCountry()) {
                    $clist.='selected';
                }
                $clist.='>'.$country->getName().'</option>';
            }
            $clist.='</select>';
            $data[$i][]=$clist;
            $modbut->setAction('xajax_X_ModifyCity('.$city->getId().',document.getElementById("modctname").value,document.getElementById("modctcountry").options[document.getElementById("modctcountry").selectedIndex].value)');
            $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
            $delbut->setAction('xajax_X_ShowCities(-1,0)');
            $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        } else {
            $data[$i][]=$city->getName();
            $data[$i][]=$db->getCountry($city->getIdCountry())->getName();
            $modbut->setCaption(TR_TEXT_BUTTON_MOD);
            $modbut->setAction('xajax_X_ShowCities('.$city->getId().')');
            $delbut->setAction('xajax_X_DelCity('.$city->getId().')');
            $delbut->setCaption(TR_TEXT_BUTTON_DEL);
        }
            
        $ids[]=$city->getId();
        $modbut->setWidth(50);
        $delbut->setWidth(50);
        $data[$i][]=$db->getName($db->getCityInfo($city->getId())->getCreatedBy());
        $data[$i][]=$db->getCityInfo($city->getId())->getCreationDate();
        $data[$i][]=$db->getName($db->getCityInfo($city->getId())->getModifBy());
        $data[$i][]=$db->getCityInfo($city->getId())->getModifDate();
            
        $buttons[$i][]=$modbut;
        $buttons[$i][]=$delbut;
        $i++;
    }
    if ($i==0) {
        $addfield=1;
    }
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="modctname" value="" style="width:80px" class="edit"/>';
        $clist='<select id="modctcountry">';
        foreach ($db->getAllCountries() as $country) {
            $clist.='<option value='.$country->getId().' class="edit">'.$country->getName().'</option>';
        }
        $clist.='</select>';
        $data[$i][]=$clist;
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('CityGridMB-2');
        $modbut->setAction('xajax_X_ModifyCity(-2,document.getElementById("modctname").value,document.getElementById("modctcountry").value,document.getElementById("modctcountry").options[document.getElementById("modctcountry").selectedIndex].value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $delbut=new TButton;
        $delbut->setName('CityGridDB-2');
        $delbut->setAction('xajax_X_ShowCities(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    $tableob->setButtons($buttons);
    $tableob->setIndexes($ids);
    $tableob->setHeaders($headers);
    $tableob->setData($data);
    $addfield==1 ? $zm=-2:$zm=$ids[count($ids)-1];
    return $tableob->show().'<p><input type="button" name="addcityB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowCities('.$zm.',1)" class="button"/>';
}

///funkcja generująca tabelkę z państwami
/**
@param $id id edytowanego państwa
@param $addfield czy ma zostać dodany wiersz dodawania państwa
@return kod tabelki z państwami
*/

function ShowCountriesBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $countries=$db->getAllCountries(true);
    $tableob=new TDataGrid('CurrGrid', 800, 10);
    $ids;
    $data;
    $i=0;
    $headersp[]=TR_TEXT_COUNTRY;
    $headersp[]=TR_TEXT_CREATED_BY;
    $headersp[]=TR_TEXT_CREATION_DATE;
    $headersp[]=TR_TEXT_MODIFIED_BY;
    $headersp[]=TR_TEXT_MODIFICATION_DATE;
    $buttons;
        
    $columns[]='130px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    if ($addfield==1) {
        $id++;
    }
    foreach ($countries as $country) {
        $modbut=new TButton;
        $delbut=new TButton;
            
        $modbut->setName('CountryGridMB'.$country->getId());
            
        if ($country->getId()==$id && $addfield==0) {
            $data[$i][]='<input type="text" id="modcountry" value="'.$country->getName().'" />';
            $modbut->setAction('xajax_X_ModifyCountry('.$country->getId().',document.getElementById("modcountry").value)');
            $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
            $delbut->setAction('xajax_X_ShowCountries(-1,0)');
            $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        } else {
            $data[$i][]=$country->getName();
            $modbut->setCaption(TR_TEXT_BUTTON_MOD);
            $modbut->setAction('xajax_X_ShowCountries('.$country->getId().')');
            $delbut->setAction('xajax_X_DelCountry('.$country->getId().')');
            $delbut->setCaption(TR_TEXT_BUTTON_DEL);
        }
        $data[$i][]=$db->getName($db->getCountryInfo($country->getId())->getCreatedBy());
        $data[$i][]=$db->getCountryInfo($country->getId())->getCreationDate();
        $data[$i][]=$db->getName($db->getCountryInfo($country->getId())->getModifBy());
        $data[$i][]=$db->getCountryInfo($country->getId())->getModifDate();
        $modbut->setWidth(50);
        $delbut->setWidth(50);
        $buttons[$i][]=$modbut;
        $buttons[$i][]=$delbut;
        $ids[]=$country->getId();
        $i++;
    }

    if ($i==0) {
        $addfield=1;
    }
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="modcountry" value="" class="button"/>';
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('CountryGridMB-2');
        $modbut->setAction('xajax_X_ModifyCountry(-2,document.getElementById("modcountry").value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $delbut=new TButton;
        $delbut->setName('CountryGridDB-2');
        $delbut->setAction('xajax_X_ShowCountries(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
            
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    $tableob->setButtons($buttons);
    $tableob->setIndexes($ids);
    $tableob->setHeaders($headersp);
    $tableob->setData($data);
    $addfield==1 ? $zm=-2:$zm=$ids[count($ids)-1];
    return $tableob->Show().'<p><input type="button" name="addcountryB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowCountries('.$zm.',1)" class="button"/>';
}


///funkcja generująca tabelkę z środkami transportu
/**
@param $id id edytowanego środka transportu
@param $addfield czy ma zostać dodany wiersz dodawania środka transportu
@return kod tabelki z środkami transportu
*/

function ShowTransportBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $transport=$db->getAllTransp(true);
    $tableob=new TDataGrid('TransGrid', 800, 10);
    $ids=array();
    $data;
    $i=0;
    $zm;
    $headersp[]=TR_TEXT_TRANSPORT;
    $headersp[]=TR_TEXT_CREATED_BY;
    $headersp[]=TR_TEXT_CREATION_DATE;
    $headersp[]=TR_TEXT_MODIFIED_BY;
    $headersp[]=TR_TEXT_MODIFICATION_DATE;
    $buttons;
        
    $columns[]='130px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    if ($addfield==1) {
        $id++;
    }
    if ($transport!=null) {
        foreach ($transport as $trans) {
            $modbut=new TButton;
            $delbut=new TButton;
            
            $modbut->setName('TransGridMB'.$trans->getId());
            
            if ($trans->getId()==$id && $addfield==0) {
                $data[$i][]='<input type="text" id="modtrans" value="'.$trans->getName().'" class="button"/>';
                $modbut->setAction('xajax_X_ModifyTransport('.$trans->getId().',document.getElementById("modtrans").value)');
                $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
                $delbut->setAction('xajax_X_ShowTransport(-1,0)');
                $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
            } else {
                $data[$i][]=$trans->getName();
                $modbut->setCaption(TR_TEXT_BUTTON_MOD);
                $modbut->setAction('xajax_X_ShowTransport('.$trans->getId().')');
                $delbut->setAction('xajax_X_DelTransport('.$trans->getId().')');
                $delbut->setCaption(TR_TEXT_BUTTON_DEL);
            }
            $data[$i][]=$db->getName($db->getTranspInfo($trans->getId())->getCreatedBy());
            $data[$i][]=$db->getTranspInfo($trans->getId())->getCreationDate();
            $data[$i][]=$db->getName($db->getTranspInfo($trans->getId())->getModifBy());
            $data[$i][]=$db->getTranspInfo($trans->getId())->getModifDate();
            $modbut->setWidth(50);
            $delbut->setWidth(50);
            $buttons[$i][]=$modbut;
            $buttons[$i][]=$delbut;
            $ids[]=$trans->getId();
            $i++;
        }
    }
    if ($i==0) {
        $addfield=1;
    }
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="trans" value="" />';
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('TransGridMB-2');
        $modbut->setAction('xajax_X_ModifyTransport(-2,document.getElementById("trans").value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $delbut=new TButton;
        $delbut->setName('TransGridDB-2');
        $delbut->setAction('xajax_X_ShowTransport(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    if (isset($buttons)) {
        $tableob->setButtons($buttons);
    }
    if (isset($ids)) {
        $tableob->setIndexes($ids);
    }
    $tableob->setHeaders($headersp);
    if (isset($data)) {
        $tableob->setData($data);
    }
    if ($addfield==1) {
        $zm=-1;
    } else {
        $zm=$ids[count($ids)-1];
    }
    return $tableob->Show().'<p><input type="button" name="addtransB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowTransport('.$zm.',1)" class="button"/>';
}


///funkcja generująca tabelkę z hotelami
/**
@param $id id edytowanego hotelu
@param $addfield czy ma zostać dodany wiersz dodawania hotelu
@return kod tabelki z hotelami
*/
function ShowHotelsBox($id, $addfield=0)
{
    $db=new MainPDO;
    $db->connect();
    $hotels=$db->getAllHotels(true);
    $tableob=new TDataGrid('HotelGrid', 800, 10);
    $ids;
    $data;
    $i=0;
    $buttons;
    $headers[]=TR_TEXT_HOTEL;
    $headers[]=TR_TEXT_COUNTRY;
    $headers[]=TR_TEXT_CITY;
    $headers[]=TR_TEXT_CREATED_BY;
    $headers[]=TR_TEXT_CREATION_DATE;
    $headers[]=TR_TEXT_MODIFIED_BY;
    $headers[]=TR_TEXT_MODIFICATION_DATE;
        
    $columns[]='150px';
    $columns[]='150px';
    $columns[]='100px';
    $columns[]='100px';
    $columns[]='100px';
    $columns[]='100px';
    $columns[]='100px';
        
    foreach ($hotels as $hotel) {
        $modbut=new TButton;
        $delbut=new TButton;
            
        $modbut->setName('HotelGridMB'.$hotel->getId());
        $delbut->setName('HotelGridDB'.$hotel->getId());
            
        if ($hotel->getId()==$id && $addfield==0) {
            $data[$i][]='<input type="text" id="modhname" value="'.$hotel->getName().'" style="width:80px"/>';
            $clist='<select id="modhcountry">';
                
            foreach ($db->getAllCountries(true) as $country) {
                $clist.='<option value='.$country->getId().' ';
                if ($country->getId()==$db->getCountry($hotel->getIdCity())->getId()) {
                    $clist.='selected';
                }
                $clist.='>'.$country->getName().'</option>';
            }
            $clist.='</select>';
            $data[$i][]=$clist;
            $data[$i][]='<div id="CitiesDDBox">'.DDShowCities($db->getCity($hotel->getIdCity())->getIdCountry()).'</div>';
    
                
            $modbut->setAction('xajax_X_ModifyHotel('.$hotel->getId().',document.getElementById("modhname").value,document.getElementById("modhcity").options[document.getElementById("modhcity").selectedIndex].value)');
            $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
            $delbut->setAction('xajax_X_ShowHotels(-1,0)');
            $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        } else {
            $data[$i][]=$hotel->getName();
            $data[$i][]=$db->getCountry($db->getCity($hotel->getIdCity())->getIdCountry())->getName();//$db->getCountry(($db->getCity($hotel->getIdCity()))->getIdCountry())->getName();
                $data[$i][]=$db->getCity($hotel->getIdCity())->getName();
            $modbut->setCaption(TR_TEXT_BUTTON_MOD);
            $modbut->setAction('xajax_X_ShowHotels('.$hotel->getId().')');
            $delbut->setAction('xajax_X_DelHotel('.$hotel->getId().')');
            $delbut->setCaption(TR_TEXT_BUTTON_DEL);
        }
            
        $ids[]=$hotel->getId();
        $data[$i][]=$db->getName($db->getHotelInfo($hotel->getId())->getCreatedBy());
        $data[$i][]=$db->getHotelInfo($hotel->getId())->getCreationDate();
        $data[$i][]=$db->getName($db->getHotelInfo($hotel->getId())->getModifBy());
        $data[$i][]=$db->getHotelInfo($hotel->getId())->getModifDate();
        $modbut->setWidth(50);
        $delbut->setWidth(50);
        $buttons[$i][]=$modbut;
        $buttons[$i][]=$delbut;
        $i++;
    }
    if ($i==0) {
        $addfield=1;
    }
        
    if ($addfield==1) {
        $ids[]=-2;
        $data[$i][]='<input type="text" id="modhname" value="" style="width:80px"/>';
        $clist='<select id="modhcountry" onchange=xajax_X_DDShowCities(document.getElementById("modhcountry").options[document.getElementById("modhcountry").selectedIndex].value)>';
        $countries=$db->getAllCountries(true);
        foreach ($countries as $country) {
            $clist.='<option value='.$country->getId().' >'.$country->getName().'</option>';
        }
        $clist.='</select>';
        $data[$i][]=$clist;
        if (isset($countries[0])) {
            $data[$i][]='<div id="CitiesDDBox">'.DDShowCities($countries[0]->getId()).'</div>';
        }
        $id=$ids[$i];
        $modbut=new TButton;
        $modbut->setName('HotelGridMB-2');
        $modbut->setAction('xajax_X_ModifyHotel(-2,document.getElementById("modhname").value,document.getElementById("modhcity").value,document.getElementById("modhcity").options[document.getElementById("modhcity").selectedIndex].value)');
        $modbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $delbut=new TButton;
        $delbut->setName('HotelGridDB-2');
        $delbut->setAction('xajax_X_ShowHotels(-1,0)');
        $delbut->setCaption(TR_TEXT_BUTTON_CANCEL);
        $buttons[]=array($modbut,$delbut);
    }
        
    $tableob->setColumnsWidth($columns);
    $tableob->setButtons($buttons);
    $tableob->setIndexes($ids);
    $tableob->setHeaders($headers);
    $tableob->setData($data);
    $addfield==1 ? $zm=-2:$zm=$ids[count($ids)-1];
    return $tableob->show().'<p><input type="button" name="addhotelB" value="'.TR_TEXT_BUTTON_ADD.'" onclick="xajax_X_ShowHotels('.$zm.',1)" class="button"/>';
}

///Funkcja generująca listę miast danego państwa
/**
@param $id_country id państwa którego miasta mają znalezc sie na liście
@return kod listy miast
*/

function DDShowCities($id_country)
{
    $db=new MainPDO;
    $db->connect();
    $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
    $select=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/select.tpl');
    $opt='';
    $var=$db->getCitiesOfCountry($id_country, true);
    /*if(count($var)==0)
    {
        $opt.=str_replace('{option}',count($var),$option);
        $opt=str_replace('{option_value}','',$opt);
        $opt=str_replace('{selected}','',$opt);
    }
    else*/
    foreach ($var as $a) {
        $opt.=str_replace('{option}', $a->getName(), $option);
        $opt=str_replace('{option_value}', $a->getId(), $opt);
        $opt=str_replace('{selected}', '', $opt);
    }
    $select=str_replace('{select_name}', 'modhcity', $select);
    $opt=str_replace('{option}', $opt, $select);
    return $opt;
}
///Xajaxowa funkcja zwracająca do strony kod listy
/**
@param $country id państwa
@return xajax xml z listę państw
*/
function X_DDShowCities($country)
{
    $obj=new xajaxResponse();
    $obj->addAssign('CitiesDDBox', 'innerHTML', DDShowCities($country));
    $obj->addScript('document.getElementById("CitiesDDBox").style.display="block"');
    return $obj->getXML();
}



///Xajaxowa funkcja generująca tabelkę hoteli
/**
@param $id edytowanego hotelu
@param $addfield czy ma być dodany wiersz dodawania nowego hotelu
@return xajax xml z listę hoteli
*/

function X_ShowHotels($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowHotelsBox($id, $addfield));
    $obj->addScript('document.getElementById("modhname").focus()');
    return $obj->getXML();
}


///Xajaxowa funkcja generująca tabelkę walut
/**
@param $id edytowanej waluty
@param $addfield czy ma być dodany wiersz dodawania nowej waluty
@return xajax xml z listę hoteli
*/

function X_ShowCurrencies($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCurrenciesBox($id, $addfield));
    $obj->addScript('document.getElementById("modcurr").focus()');
    return $obj->getXML();
}


///Xajaxowa funkcja generująca tabelkę środków transportu
/**
@param $id edytowanego środka transportu
@param $addfield czy ma być dodany wiersz dodawania nowego środka transportu
@return xajax xml z listę środków transportu
*/

function X_ShowTransport($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowTransportBox($id, $addfield));
    $obj->addScript('document.getElementById("modtrans").focus()');
    return $obj->getXML();
}


///Xajaxowa funkcja generująca tabelkę państw
/**
@param $id edytowanego panstwa
@param $addfield czy ma być dodany wiersz dodawania nowego panstwa
@return xajax xml z listę państwa
*/

function X_ShowCountries($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCountriesBox($id, $addfield));
    $obj->addScript('document.getElementById("modcountry").focus()');
    return $obj->getXML();
}


///Xajaxowa funkcja generująca tabelkę miast
/**
@param $id edytowanego miasta
@param $addfield czy ma być dodany wiersz dodawania nowego miasta
@return xajax xml z listę miast
*/

function X_ShowCities($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCitiesBox($id, $addfield));
    $obj->addScript('document.getElementById("modctname").focus()');
    return $obj->getXML();
}

///Xajaxowa funkcja generująca tabelkę celów podróży
/**
@param $id edytowanego celu podróży
@param $addfield czy ma być dodany wiersz dodawania nowego celu podróży
@return xajax xml z listę celów podróży
*/

function X_ShowAims($id, $addfield=0)
{
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowAimsBox($id, $addfield));
    $obj->addScript('document.getElementById("modaim").focus()');
    return $obj->getXML();
}


///Xajaxowa funkcja modyfikująca miasto
/**
@param $id edytowanego miasta
@param $cityname nowa nazwa miasta
@param $idcountry id państwa pod które podlega miasto
@return xajax xml z listę miast po zmianie
*/
function X_ModifyCity($id, $cityname, $idcountry)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $city=new City();
            $city->setId($id);
            $city->setName($cityname);
            $city->setIdCountry($idcountry);
            $city->setActive(1);
            if ($cityname!='') {
                if ($id < 0) {
                    $db->addCity($city->getName(), $city->getIdCountry(), $_SESSION['user']->getId_logged());
                } else {
                    $db->setCity($city, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCitiesBox(-1, 0));
    return $obj->getXML();
}


///Xajaxowa funkcja modyfikująca hotel
/**
@param $id edytowanego hotelu
@param $hotelname nowa nazwa hotelu
@param $idcity id miasta pod które podlega hotel
@return xajax xml z listę hoteli po zmianie
*/

function X_ModifyHotel($id, $hotelname, $idcity)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $hotel=new Hotel();
            $hotel->setId($id);
            $hotel->setName($hotelname);
            $hotel->setIdCity($idcity);
            $hotel->setActive(1);
            if ($hotelname!='') {
                if ($id < 0) {
                    $db->addHotel($hotel, $_SESSION['user']->getId_logged());
                } else {
                    $db->setHotel($hotel, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowHotelsBox(-1, 0));
    return $obj->getXML();
}

///Xajaxowa funkcja modyfikująca walutę
/**
@param $id edytowanej waluty
@param $currname nowa nazwa waluty
@return xajax xml z listę walut po zmianie
*/
function X_ModifyCurrency($id, $currname)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $cur=new Currency();
            $cur->setId($id);
            $cur->setName($currname);
            $cur->setActive(1);
            if ($currname!='') {
                if ($id < 0) {
                    $db->addCurr($cur->getName(), $_SESSION['user']->getId_logged());
                } else {
                    $db->setCurr($cur, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCurrenciesBox(-1, 0));
    return $obj->getXML();
}


///Xajaxowa funkcja modyfikująca środek transportu
/**
@param $id edytowanego środka transportu
@param $name nowa nazwa środka transportu
@return xajax xml z listę środków transportu po zmianie
*/

function X_ModifyTransport($id, $name)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $trans=new Transport();
            $trans->setId($id);
            $trans->setName($name);
            $trans->setActive(1);
            if ($name!='') {
                if ($id < 0) {
                    $db->addTransp($trans->getName(), $_SESSION['user']->getId_logged());
                } else {
                    $db->setTransp($trans, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowTransportBox(-1, 0));
    return $obj->getXML();
}


///Xajaxowa funkcja modyfikująca cel podróży
/**
@param $id edytowanego celu
@param $name nowa nazwa celu
@return xajax xml z listę celów po zmianie
*/

function X_ModifyAim($id, $name)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $aim=new Purpose();
            $aim->setId($id);
            $aim->setName($name);
            $aim->setActive(1);
            if ($name!='') {
                if ($id<0) {
                    $db->addPurp($aim->getName(), $_SESSION['user']->getId_logged());
                } else {
                    $t=$db->setPurp($aim, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowAimsBox(-1, 0));
    return $obj->getXML();
}



///Xajaxowa funkcja modyfikująca panstwo
/**
@param $id edytowanego państwa
@param $countryname nowa nazwa państwa
@return xajax xml z listę państw po zmianie
*/


function X_ModifyCountry($id, $countryname)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $country=new Country();
            $country->setId($id);
            $country->setName($countryname);
            $country->setActive(1);
            if ($countryname!='') {
                if ($id < 0) {
                    $db->addCountry($country->getName(), $_SESSION['user']->getId_logged());
                } else {
                    $db->setCountry($country, $_SESSION['user']->getId_logged());
                }
            }
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCountriesBox(-1, 0));
    return $obj->getXML();
}


///Xajaxowa funkcja kasująca walutę
/**
@param $id usuwanej waluty
@return xajax xml z listę walut po skasowaniu
*/

function X_DelCurrency($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->delCurr($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCurrenciesBox(-1, 0));
    return $obj->getXML();
}
///Xajaxowa funkcja kasująca miasto
/**
@param $id usuwanego miasta
@return xajax xml z listę miast po skasowaniu
*/


function X_DelCity($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $d=$db->delCity($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCitiesBox(-1, 0));
    return $obj->getXML();
}

///Xajaxowa funkcja kasująca środek transportu
/**
@param $id usuwanego środka transportu
@return xajax xml z listę środków transportów po skasowaniu
*/

function X_DelTransport($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $db->delTransp($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowTransportBox(-1, 0));
    return $obj->getXML();
}

///Xajaxowa funkcja kasująca państwo
/**
@param $id usuwanego państwa
@return xajax xml z listę państw po skasowaniu
*/

function X_DelCountry($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $res=$db->delCountry($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowCountriesBox(-1, 0));
    return $obj->getXML();
}

///Xajaxowa funkcja kasująca hotel
/**
@param $id usuwanego hotelu
@return xajax xml z listę hoteli po skasowaniu
*/

function X_DelHotel($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $res=$db->delHotel($id, $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowHotelsBox(-1, 0));
    return $obj->getXML();
}

///Xajaxowa funkcja kasująca cel podróży
/**
@param $id usuwanego celu podróży
@return xajax xml z listę celów podróży po skasowaniu
*/

function X_DelAim($id)
{
    $db=new MainPDO;
    $db->connect();

    if ($db->isAdmin($_SESSION['user']->getId_logged(), $blah)) {
        if ($blah==1) {
            $res=$db->delPurp($id, $_SESSION['user']->getId_logged(), $_SESSION['user']->getId_logged());
        }
    }
    $obj=new xajaxResponse();
    $obj->addAssign('Grid', 'innerHTML', ShowAimsBox(-1, 0));
    return $obj->getXML();
}
