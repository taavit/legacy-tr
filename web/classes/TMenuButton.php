<?php
/// plik zawiera klasę 'TMenuButton'
/** @file TMenuButton.php */


///Klasa do obsługi strony
        /**Klasa generująca przycsik menu
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
        @date 17-11-2007
        */

class TMenuButton
{
    ///Tytuł przycisku
    protected $caption;
    ///Link do strony wywoływanej po kliknięciu
    protected $link;

    /**
     * @param $caption tytuł
     * @param $link link do strony
     */
    public function __construct($caption, $link)
    {
        $this->caption = $caption;
        $this->link = $link;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }

    public function getLink(): string
    {
        return $this->link;
    }
}
