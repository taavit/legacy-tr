<?php
/// plik zawiera klasę kontrolki 'TDataGrid'
/** @file TDataGrid.php */


///Kontrolka do obsługi tabel
        /**Klasa wyświetla wprowadzone dane w postaci tabelki
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavicjusz@gmail.com)
        @date 17-02-2008
*/
class TDataGrid
{
    ///Nazwa tabelki
    protected $name="";
    ///Dwuwymiarowa tablica wartości do wyświetlenia
    protected $data=array();
     ///Tablica indeksów dla kolejnych rekordów
    protected $ids;
    ///szerokość
    protected $width;
    ///wysokość
    protected $height;
    ///tablica nagłówków dla kolumn
    protected $headers;
    ///tablica przycisków modyfikujących
    protected $buttons;
    ///tablica mask na wiersze przy których mają się batony pokazywać
    protected $mask;
    ///tablica zawierająca szerokości kolumn
    protected $columnswidth=array();
///Konstruktor
/**
@param $name nazwa tabelki
@param $width szerokość tabelki
@param $height wysokość tabelki
*/
    public function __construct($name, $width='840', $height='10')
    {
        $this->width=$width;
        $this->height=$height;
    }
///Metoda ustawiająca szerokości kolumn
/**
@param $col szerokości kolumn
*/
    public function setColumnsWidth($col)
    {
        $this->columnswidth=$col;
    }
///metoda ustawiająca przyciski
/**
@param $buttons tablica przycisków
*/
    public function setButtons($buttons)
    {
        $this->buttons=$buttons;
    }
///metoda ustawiająca nagłówki
/**
@param $h tablica nagłówków
*/
    
    public function setHeaders($h)
    {
        $this->headers=$h;
    }
    ///metoda ustawiająca indeksy wierszy
/**
@param $ids tablica indeksów
*/
    public function setIndexes($ids)
    {
        $this->ids=$ids;
    }
///metoda ustawiająca dane w obiekcie
/**
@param $array dwuwymiarowa tablcia danych
*/
    
    public function setData($array)
    {
        $this->data=$array;
    }
    
///metoda generująca tabelkę
/**
@return kod wygenerowanej tabelki
*/
    public function Show()
    {
        $tpl=new TTemplate;
        $tpl->setTplFile(__DIR__.'/../../app/Resources/views/DataGrid/main.tpl');
        $tpl->addRepVar('{width}', $this->width);
        $tpl->addRepVar('{height}', $this->height);
        $i=0;
        $tplhead=new TTemplate;
        $tplhead->setTplFile(__DIR__.'/../../app/Resources/views/DataGrid/datarow.tpl');
        $headers='';
        $rows='';
        $fieldtext='';
        $this->headers[]=TR_TEXT_GRID_BUTTONS;
        foreach ($this->headers as $head) {
            $tplfield= new TTemplate;
            $tplfield->setTplFile(__DIR__.'/../../app/Resources/views/DataGrid/datafield.tpl');
            $tplfield->addRepVar('{field}', $this->headers[$i]);
            $tplfield->addRepVar('{class}', 'DataGridHeader');
            if (!isset($this->columnswidth[$i])) {
                $this->columnswidth[$i]='100px';
            }
            $tplfield->addRepVar('{width}', $this->columnswidth[$i]);
            $tplfield->prepare();
            $fieldtext.=$tplfield->getOutputText();
            $i++;
        }
        $tplhead->addRepVar('{buttons}', '');
        $tplhead->addRepVar('{row}', $fieldtext);
        $tplhead->addRepVar('{class}', 'DataGridHeader');
        $tplhead->prepare();
        $rows=$tplhead->getOutputText();
        $i=0;
        if (isset($this->data) && count($this->data)>0) {
            foreach ($this->data as $row) {
                $fieldtext='';
                $tplrow=new TTemplate;
                $tplrow->setTplFile(__DIR__.'/../../app/Resources/views/DataGrid/datarow.tpl');
                $k=0;
                foreach ($row as $field) {
                    $tplfield= new TTemplate;
                    $tplfield->setTplFile(__DIR__.'/../../app/Resources/views/DataGrid/datafield.tpl');
                    $tplfield->addRepVar('{field}', $field);
                    $tplfield->addRepVar('{class}', 'DataGridCell');
                    $tplfield->addRepVar('{width}', $this->columnswidth[$k]);
                    $tplfield->prepare();
                    $fieldtext.=$tplfield->getOutputText();
                    $k++;
                }
                $buttons='<td class="DataGridCell">';
                if (isset($this->buttons)) {
                    foreach ($this->buttons[$i] as $but) {
                        $buttons.=$but->Show();
                    }
                }
            
                $buttons.='</td>';
                $tplrow->addRepVar('{buttons}', $buttons);
                $tplrow->addRepVar('{id}', $this->ids[$i]);
                $tplrow->addRepVar('{row}', $fieldtext);
                $tplrow->addRepVar('{class}', 'DataGridRow');
                $tplrow->prepare();
                $rows.=$tplrow->getOutputText();
                $i++;
            }
        }
        $tpl->addRepVar('{content}', $rows);
        $tpl->prepare();
        return $tpl->getOutputText();
    }
}
