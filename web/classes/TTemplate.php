<?php
/// plik zawiera klasę kontrolki 'TTemplate'
/** @file TTemplate.php */


///Klasa obsługująca szablony
/**
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 12-11-2007
*/

class TTemplate
{
    ///Wygenerowany kod strony
    private $outputtext="";
    ///Plik z szablonem
    private $tplfile="";
    ///Tablica zamian
    private $replacetable=array();
    ///Funkcja ustawiająca plik z szablonem

    public function __construct(string $path = '')
    {
        $this->tplfile = $path;
    }

    /**
    @param $name nazwa pliku
    */
    public function setTplFile($name)
    {
        $this->tplfile=$name;
    }
    ///Funkcja zwracająca wygenerowany kod
    /**
    @return kod strony
    */
    public function getOutputText()
    {
        return $this->outputtext;
    }
    ///Funkcja dodająca słowa do podmiany
    /**
    @param $a co ma zamienić
    @param $b na co ma zamienić
    */
    public function addRepVar($a, $b)
    {
        $this->replacetable[$a]=$b;
    }
    ///Funkcja generująca kod
    public function prepare()
    {
        $text=file_get_contents($this->tplfile);
        foreach ($this->replacetable as $klucz=>$wartosc) {
            $text=str_replace($klucz, $wartosc, $text);
        }
        $this->outputtext=$text;
    }
}
