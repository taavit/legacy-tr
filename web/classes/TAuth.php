<?php
/// plik zawiera klasę 'LoggedUser' oraz 'TAuth' odpowiedzialne za autoryzacje użytkowników
/** @file TAuth.php */


///Klasa zawierająca dane zalogowanego użytkownika
class LoggedUser
{
    ///Id zalogowanego użytkownika
    protected $id_logged=0;
    ///Zmienna mówiąca czy zalogowany użytkownik jest adminem
    protected $isadmin=false;
    ///Czy jest akceptatorem
    protected $isaccept=false;
    ///Godzina zalogowania
    protected $time_logged;
    
    /** @return id zalogowanego użytkownika*/
    public function getId_logged()
    {
        return $this->id_logged;
    }
    /** @return czy jest administratorem*/
    public function getIsAdmin()
    {
        return $this->isadmin;
    }
    
    /** @return czy jest akceptatorem*/
    public function getIsAccept()
    {
        return $this->isaccept;
    }
    /** @return czas zalogowania*/
    public function getTime_logged()
    {
        return $this->time_logged;
    }

    /** Metoda przypisuje id zalogowanego użytkownika
    @param $id identyfikator użytkownika
    @return void */

    public function setId_logged($id)
    {
        $this->id_logged=$id;
    }

    /** Metoda przypisuje czy użytkownik jest administratorem
    @param $isadmin wartość logiczna czy zalogowany jest administratorem
    @return void */
    
    public function setIsAdmin($isadmin)
    {
        $this->isadmin=$isadmin;
    }
    /** Metoda przypisuje czy użytkownik jest osobą akceptującą jakąkolwiek delegację
    @param $isaccept wartość logiczna czy zalogowany jest osobą akceptującą jakąkolwiek delegację
    @return void */
    public function setIsAccept($isaccept)
    {
        $this->isaccept=$isaccept;
    }
    /** Metoda przypisuje czas zalogowania
    @param $time godzina i data zalogowania
    @return void */
    public function setTime_logged($time)
    {
        $this->time_logged=$time;
    }
}

///Klasa odpowiedzialna za autoryzajcę użytkownika

class TAuth extends LoggedUser
{
    
    ///Funkcja zwracająca pola logowania
    /**
    @return sting zawierający pola logowania
    */
    public function ShowLoginForm($url, $message_p='')
    {
        $tpl=new TTemplate;
        $tpl->setTplFile(__DIR__.'/../../app/Resources/views/auth/login.tpl');
        $tpl->addRepVar('{LoginC}', TR_TEXT_LOGIN_USERNAME);
        $tpl->addRepVar('{PassC}', TR_TEXT_LOGIN_PASSWORD);
        $tpl->addRepVar('{url}', $url);
        $tpl->addRepVar('{message}', $message_p);
        $tpl->prepare();
        return $tpl->getOutputText();
    }
    ///Funkcja logująca użytkownika na podstawie tablicy $_POST. Funkcja sprawdza także czy system nie jest zablokowany przez administratora
    /**
    @return true gdy użyutkownik został zalogowany poprawnie / false gdy niepoprawnie
    */
    public function LoginUser()
    {
        $loginok=false;
        $allow=1;
        $usr=new LoggedUser;
        $db=new MainPDO;
        $db->connect();
        $emp=$db->getActiveEmployeeByLogin($_POST['login']);
        if ($emp) {
            $pass=$db->getPassw($emp->getId());
            if ($pass==md5($_POST['pass'])) {
                $loginok=true;
                $usr->SetId_logged($emp->getId());
                if ($db->isAdmin($emp->getId(), $baza_d)) {
                    if ($baza_d==1) {
                        $usr->setIsAdmin(true);
                    }
                }
                if ($db->isAccept($emp->getId(), $baza_d)) {
                    if ($baza_d==1) {
                        $usr->setIsAccept(true);
                    }
                }
                if ($_SESSION['blocked']&&!$db->isAdmin($emp->getId(), $baza_d)) {
                    if ($baza_d==1) {
                        $loginok=false;
                    }
                }
            }
            if ($loginok) {
                $_SESSION['user']=$usr;
            }
            return $loginok;
        }
    }
    
    ///Funkcja niszcząca zmienne sesyjne, tym samym wylogowywują użytkownika
    public function LogoutUser()
    {
        unset($_SESSION['user']);
        unset($_SESSION['msg']);
        unset($_SESSION['err']);
        unset($_SESSION['ERR']);
    }
}
