<?php
/// plik zawiera klasę 'administration'
/** @file administration.php */


///Klasa panelu administracyjnego
/**
obiekt zawiera stronę panelu administracyjnego. Sprawdza czy osoba zalogowana ma dostęp do opcji administracyjnych, i wczytuje zadany moduł panelu
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/

class administration extends TPage
{
    ///Funkcja wyświetlająca stronę
/** @return Zawartość strony panelu administracyjnego, lub komunikat o błędzie*/
    public function Show()
    {
        if (!isset($_GET['a'])) {
            $_GET['a']='adm_menu';
        }
        if ($this->usr->getIsAdmin()) {
            if ($_GET['a']=='block') {
                if (!file_exists('.lockfile')) {
                    $file=fopen(".lockfile", "w");
                }
                redirect("index2.php?page=administration");
            }
            if ($_GET['a']=='unblock') {
                if (file_exists('.lockfile')) {
                    unlink(".lockfile");
                }
                redirect("index2.php?page=administration");
            }

            if ($_GET['a']=='clean') {
                $this->db->clean();
                $_SESSION['ERR']="Baza wyczyszczona";
                redirect("index2.php?page=administration");
            }

            $_GET['a']=='tripdet' ? $directory='':$directory='administration';
            $content=LoadnRunMod($directory, $_GET['a']);
        } else {
            $content=TR_TEXT_ERROR_PAGE_NOT_ALLOWED;
        }
        return $content;
    }
}
