<?php
/// plik zawiera klasę kontrolki 'TButton'
/** @file TButton.php */


///Kontrolka do obsługi przycisków
        /**Klasa wyświetla wprowadzone dane w postaci tabelki
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavicjusz@gmail.com)
        @date 17-02-2008
*/

class TButton
{
    ///Tytuł przycisku
    protected $caption;
    ///Przejście do strony
    protected $link='';
    ///Akcja javascriptowa wykonywana po zdarzeniu onclick()
    protected $action='';//akcja
    ///Nazwa przycisku
    protected $name;
    ///Szerokość przycisku
    protected $width;
    ///Znacznik czy przejść do kolejnej lini
    protected $endline=false;
    ///Typ przycisku
    protected $type="button";
    
    
///Metoda ustawiająca tytuł przycisku
/**
@param $caption tytuł przycisku
*/
    public function setCaption($caption)
    {
        $this->caption=$caption;
    }

///Metoda ustawiająca szerokość przycisku
/**
@param $w szerokość przycisku
*/

    public function setWidth($w)
    {
        $this->width=$w;
    }


///Metoda ustawiająca łącze przycisku
/**
@param $link łącze przycisku
*/
    public function setLink($link)
    {
        $this->link=$link;
    }

///Metoda ustawiająca nazwę przycisku
/**
@param $name nazwa przycisku
*/

    public function setName($name)
    {
        $this->name=$name;
    }


///Metoda ustawiająca przejście do kolejnej lini
/**
@param $wa czy przejść do następnej lini
*/

    public function setEndline($wa)
    {
        $this->endline=$wa;
    }

///Metoda ustawiająca akcję przycisku
/**
@param $action akcja przycisku
*/
    public function setAction($action)
    {
        $this->action=$action;
    }

///Metoda ustawiająca typ przycisku
/**
@param $sub typ przycisku, domyślnie button
*/

    public function setBType($sub="button")
    {
        $this->type=$sub;
    }
///Metoda generująca przycisk
/**
@return wygenerowany kod przycisku
*/
    public function Show()
    {
        $this->action=str_replace('{link}', $this->link, $this->action);
        $wyn;
        isset($this->width) ? $wyn='style="width:'.$this->width.'px;"':$wyn='';
        $onclick='';
        if ($this->action!='') {
            $onclick='onclick=\''.$this->action.'\'';
        }
        $text= '<input type="'.$this->type.'" id="'.$this->name.'" name="'.$this->name.'" value="'.$this->caption.'" '.$onclick.$wyn.' class="button"/>';
        if ($this->endline) {
            $text.='<br />';
        }
        return $text;
    }
}
