<?php
require_once('classes/commonfunctions.php');
require_once('config.php');

require_once(__DIR__.'/../vendor/autoload.php');

session_start();

$authorize=new TAuth;
$_SESSION['blocked']=false;

//e-blad logowania
if (!isset($_GET['e'])) {
    $_GET['e']='';
}

if (!isset($_GET['a'])) {
    $_GET['a']='';
}
if (!isset($_GET['url'])) {
    $_GET['url']='index2.php';
}
//echo $_GET['url'];
if ($_GET['a']=='login') {
    if ($authorize->LoginUser()) {
        $url=$_GET['url'];

        $url=str_replace("__", "=", $url);

        $url=str_replace("~", "&", $url);

        redirect($url);
    } else {
        redirect('index.php?e=1&url='.$_GET['url']);
    }
}


if ($_GET['a']=='logout') {
    $authorize->LogoutUser();
    redirect('index.php');
}
$message='';
if ($_SESSION['blocked']) {
    $message=TR_TEXT_PAGE_BLOCKED;
} elseif ($_GET['e']=='1') {
    $message=TR_TEXT_LOGIN_FAILED;
}
//echo $_GET['url'];
echo $authorize->ShowLoginForm($_GET['url'], $message);
exit;
