<?php
/// plik zawiera angielskie tłumaczenie dla systemu
/** @file en.php */

///modyfiakcja userów
define('TR_TEXT_USER_FORENAME', 'Name');
define('TR_TEXT_USER_FORENAME2', 'First name');
define('TR_TEXT_USER_SURNAME', 'Surname');
define('TR_TEXT_USER_SURNAME2', 'Last name');
define('TR_TEXT_USER_MAIL', 'E-mail');
define('TR_TEXT_USER_LOGIN', 'Login');
define('TR_TEXT_USER_PHONE', 'Mobile');
define('TR_TEXT_USER_PASS', 'New password');
define('TR_TEXT_USER_PASS2', 'Retype password');
define('TR_TEXT_USER_CHPASS', 'Change password');
define('TR_TEXT_USER_EMPLOYEE', 'Employee');


///user add
define('TR_TEXT_USER_ALL_FIELDS', 'Wszystkie pola muszą zostać wypełnione');
define('TR_TEXT_USER_LOGIN_EXISTS', 'Istnieje już taki login');
define('TR_TEXT_CHANGE_PASS_FAILED', 'zmiana hasła nie powiodła się');
define('TR_TEXT_CHANGE_PASS_SUCCESS', 'zmiana hasła powiodła się');
define('TR_TEXT_CHANGE_PASS_NOT_THE_SAME', 'hasła nie mogą być puste i muszą być takie same');
define('TR_TEXT_USER_WRONG_EMAIL', 'Niepoprawny adres email');
define('TR_TEXT_ADD_USER', 'Dodawanie uzytkownika');

define('TR_TEXT_BUTTON_CANCEL', 'Cancel');
define('TR_TEXT_LOGIN_BUTTON', 'Zaloguj');
define('TR_TEXT_LOGIN_FAILED', 'Błąd logowania');
define('TR_TEXT_PAGE_BLOCKED', 'System zablokowany');
define('TR_TEXT_MENU_ADMIN_POS', 'Panel Admina');
define('TR_TEXT_LOGIN_USERNAME', 'Login');
define('TR_TEXT_LOGIN_PASSWORD', 'Hasło');
define('TR_TEXT_ERROR_PAGE_NOT_FOUND', 'Strony nie znaleziono');
define('TR_TEXT_ERROR_PAGE_NOT_ALLOWED', 'Nie masz uprawnień żeby podejrzeć tą stronę');
define('TR_TEXT_MENU_NEW_DELEGATION', 'Nowa delegacja');
define('TR_TEXT_MENU_DELEGATION_HISTORY', 'Historia delegacji');
define('TR_TEXT_MENU_USER_INFO', 'Dane użytkownika');
define('TR_TEXT_LOGGED_AS', 'Zalogowany jako');
define('TR_TEXT_USER_CHANGED_SUCCSESFUL', 'Użytkownik zmieniony pomyślnie');
define('TR_TEXT_MENU_ADMINISTRATOR_PANEL', 'Panel administratora');
define('TR_TEXT_USER_MANAGMENT', 'Zarządzanie użytkownikami');
//define('TR_TEXT_USERMAN_USER_ALREADY_REGISTERED','Użytkownik jest już zarejestrowany');
define('TR_TEXT_ADM_USER_MANAGMENT', 'Użytkownicy');
define('TR_TEXT_ADM_PAGE_CONF', 'Konfiguracja');
define('TR_TEXT_MENU_ACCEPTATOR_PANEL', 'Zarządzanie podrózami');
define('TR_TEXT_ADM_DELEGATION_MANAGMENT', 'Delegacje');
define('TR_TEXT_ADM_PERMISSION_MANAGMENT', 'Uprawnienia');
define('TR_TEXT_ADM_SYS_CLEAN', 'Czyść bazę');
define('TR_TEXT_ADM_SYS_BLOCK', 'Zablokuj użytkowników');
define('TR_TEXT_ADM_SYS_UNBLOCK', 'Odblokuj użytkowników');

define('TR_TEXT_GRID_BUTTONS', 'Edit');
define('TR_TEXT_BUTTON_MOD', 'Edit');
define('TR_TEXT_BUTTON_DEL', 'Delete');
define('TR_TEXT_BUTTON_ADD', 'Add');
define('TR_TEXT_BUTTON_SAVE', 'Save');
define('TR_TEXT_BUTTON_NEW_USER', 'New user');
define('TR_TEXT_BUTTON_BACK', 'Back');
define('TR_TEXT_BUTTON_DETAILS', 'Details');
define('TR_TEXT_BUTTON_CLOSE', 'Close');
define('TR_TEXT_BUTTON_ACCEPT', 'Akceptuj');
define('TR_TEXT_BUTTON_REJECT', 'Odrzuć');
define('TR_TEXT_CURRENCY', 'Currency');
define('TR_TEXT_CURRENCIES', 'Currencies');
define('TR_TEXT_TRANSPORT', 'Transport');
define('TR_TEXT_TRANSPORTS', 'Transports');
define('TR_TEXT_HOTELS', 'Hotels');
define('TR_TEXT_HOTEL', 'Hotel');
define('TR_TEXT_CITY', 'City');
define('TR_TEXT_CITIES', 'Cities');
define('TR_TEXT_COUNTRY', 'Country');
define('TR_TEXT_COUNTRIES', 'Countries');
define('TR_TEXT_CREATED_BY', 'Created by');
define('TR_TEXT_MODIFIED_BY', 'Changed by');
define('TR_TEXT_CREATION_DATE', 'Creation date');
define('TR_TEXT_MODIFICATION_DATE', 'Change date');
define('TR_TEXT_ACCEPTATORS', 'Osoby zatwierdzające');
define('TR_TEXT_ADMINISTRATORS', 'Administratorzy');
define('TR_TEXT_TRIP_PURPOSE', 'Reason');
define('TR_TEXT_TRIP_PURPOSES', 'Reasons');
define('TR_TEXT_ADM_CREATED_BY', 'Created by');
define('TR_TEXT_ID', 'Id');
define('TR_TEXT_ACCEPTATOR', 'Accepting person');
define('TR_TEXT_ADVANCE', 'Cash advance');
define('TR_TEXT_TRIP_START_DATE', 'Begin date');
define('TR_TEXT_TRIP_END_DATE', 'End date');
define('TR_TEXT_IS_HOTEL_TRUE', 'yes');
define('TR_TEXT_IS_HOTEL_FALSE', 'no');
define('TR_TEXT_IS_ADVANCE_TRUE', 'yes');
define('TR_TEXT_IS_ADVANCE_FALSE', 'no');
define('TR_TEXT_CITY_TO', 'To city');
define('TR_TEXT_COUNTRY_TO', 'To country');
define('TR_TEXT_CITY_FROM', 'From city');
define('TR_TEXT_COUNTRY_FROM', 'From country');

define('TR_TEXT_TRIP_STATUS', 'Status');
define('TR_TEXT_TRIP_STATUS_0', 'Kopia robocza');
define('TR_TEXT_TRIP_STATUS_1', 'Oczekuje');
define('TR_TEXT_TRIP_STATUS_2', 'Zaakceptowana');
define('TR_TEXT_TRIP_STATUS_3', 'Odrzucona');
define('TR_TEXT_TRIP_STATUS_4', 'Rozliczona');
define('TR_TEXT_TRIP_STATUS_5', 'Zamknięta');
define('TR_TEXT_TRIP_STATUS_6', 'Zamknięte przez program');

define('TR_TEXT_TRIP_ALL_STATUS', 'Wszystkie');
define('TR_TEXT_TRIP_STATUS_0s', 'Kopie robocze');
define('TR_TEXT_TRIP_STATUS_1s', 'Oczekujące');
define('TR_TEXT_TRIP_STATUS_2s', 'Zaakceptowane');
define('TR_TEXT_TRIP_STATUS_3s', 'Odrzucone');
define('TR_TEXT_TRIP_STATUS_4s', 'Rozliczone');
define('TR_TEXT_TRIP_STATUS_5s', 'Zamknięte');
define('TR_TEXT_TRIP_STATUS_6s', 'Zamknięte przez program');

define('TR_TEXT_STATEDEL', 'Status');
define('TR_TEXT_ACCCEPTATOR', 'Osoba akceptująca');
define('TR_TEXT_YES', 'Tak');
define('TR_TEXT_NO', 'Nie');

//trip form
define('TR_ADD_TRIP', 'Zamów delegację');
define('TR_FORM_TRIP_ACCEPT_PERSON', 'Accepting person');
define('TR_FORM_TRIP_START_DATE', 'Begin date');
define('TR_FORM_TRIP_END_DATE', 'End date');
define('TR_FORM_TRIP_AIM', 'Reason');

define('TR_FORM_TRIP_TRANSPORT', 'Transportation type');
define('TR_FORM_TRIP_IS_HOTEL', 'Hotel');
define('TR_FORM_TRIP_IS_ADVANCE', 'Cash advance');
define('TR_FORM_TRIP_COMMENT', 'Additional comments (reason description, project name, special travel requirements or other details)');
define('TR_FORM_TRIP_COMMENT2', 'Accepting person`s comment');

define('TR_TRIP_HOTEL_NAME', 'Hotel name');
define('TR_FORM_TRIP_HOTEL', 'Hotel');
define('TR_FORM_TRIP_HOTEL_START_DATE', 'Hotel from');
define('TR_FORM_TRIP_HOTEL_END_DATE', 'Hotel until');
define('TR_FORM_TRIP_HOTEL_DAYS', 'Number of nights');

define('TR_FORM_TRIP_ADVANCE_VALUE', 'Amount');
define('TR_FORM_TRIP_ADVANCE_MONEY', 'Money');
define('TR_FORM_TRIP_ADVANCE_NO_MONEY', 'Money transfer');
define('TR_FORM_TRIP_ADVANCE_HOW_TO_PAY', 'Paid by');
define('TR_FORM_TRIP_SEND_FORM', 'Send');
define('TR_FORM_TRIP_SAVE_FORM', 'Save as draft');
define('TR_FORM_TRIP_CLEAN_FORM', 'Clear form');

define('TR_FORM_TRIP_NO_ID_ACCEPT', 'Brak identyfkatora osoby akceptującej delegację');
define('TR_FORM_TRIP_WRONG_START_DATA_TYPE', 'Nieprawidłowy format daty wyjazdu na delegację');
define('TR_FORM_TRIP_WRONG_END_DATA_TYPE', 'Nieprawidłowy format daty powrotu z delegacji');
define('TR_FORM_TRIP_WRONG_ORDER_DATES', 'Nieprawidłowa kolejność dat');
define('TR_FORM_TRIP_NO_ADVANCE_VALUE', 'Nieprawidłowa wartość zaliczki');
define('TR_FORM_TRIP_NO_CURRENCY_ID', 'Nie wybrano waluty zaliczki');
define('TR_FORM_TRIP_NO_INFO_MONEY', 'Nie wybrano fromy uzyskania zaliczki');
define('TR_FORM_TRIP_NO_HOTEL_ID', 'Nie wybrano hotelu');
define('TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE', 'Nieprawidłowy format daty zameldowania w hotelu');
define('TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE', 'Nieprawidłowy fromat wymeldowania z hotelu');
define('TR_FORM_TRIP_WRONG_HOTEL_ORDER_DATES', 'Nieprawidłowa kolejność dat w zamawianiu hotelu');
define('TR_FORM_TRIP_WRONG_HOTEL_TRIP_ORDER_START_DATES', 'Data zameldowania w hotelu jest wcześniejsza niż rozpoczęcia delegacji');
define('TR_FORM_TRIP_DAYS_MUST_BE_NUMERIC', 'Liczba dób musi być liczbą naturalną>0');
define('TR_FORM_TRIP_ADVANCE_VALUE_MUST_BE_NUMERIC', 'Wartość zaliczki musi być liczbą całkowitą');
define('TR_FORM_TRIP_NO_HOTEL_DAYS', 'Nie podano ilości dób hotelowych');
define('TR_FORM_TRIP_NO_TRIP_ID', 'Nie wybrano środka transportu');
define('TR_FORM_TRIP_NO_MATCH_CITY_COUNTRY', 'Dane misto nie jest przyporządkowane danemu państwu');
define('TR_FORM_TRIP_NO_CITY_OR_COUNTRY', 'Nie podano miasta lub państwa');
define('TR_TRIP_FORM_DELEGATION_REQUEST_SEND', 'Wysłano prośbę o delegację');
define('TR_TRIP_FORM_DELEGATION_REQUEST_SAVED', 'Zapisano delegację');
define('TR_TRIP_FORM_DELEGATION_REQUEST_ALREADY_SENT_TODAY', 'Wysłano/zapisano już dzisiaj taką prośbę o delegację.');
define('TR_TRIP_FORM_DELEGATION_REQUEST_FAILED', 'Próba wysłania delegacji nie powiodła się');
define('TR_FORM_TRIP_CITY_FROM_CITY_TO_ARE_THE_SAME', 'Miasto zaczęcia delegacji i miasto docelowe jest tym samym miastem');
define('TR_FORM_TRIP_NO_HOTEL_IN_THIS_CTIY', 'W tym mieście nie ma zdefiniowanych hoteli');
define('TR_FORM_TRIP_BAD_HOTEL_DAYS', 'Ilość dób hotelowych za bardzo odbiega od wyliczonych');
define('TR_FORM_TRIP_OLD_SETTINGS', 'Niestety od czasu zapisania tej kopii roboczej niektóre z osób akceptujących delegację, miast, środków transportu, hoteli, walut, celi podrózy są niedostępne.');


//datails of trip
define('TR_TRIP_DETAIL_EMPLOYEE', 'Ordering person');



//pages
define('TR_TEXT_PAGE_FIRST', '<img src="graphics/btn_nav_first.jpg" />');
define('TR_TEXT_PAGE_LAST', '<img src="graphics/btn_nav_last.jpg" />');
define('TR_TEXT_PAGE_PREVIOUS', '<img src="graphics/btn_nav_left.jpg" />');
define('TR_TEXT_PAGE_NEXT', '<img src="graphics/btn_nav_right.jpg" />');
    

//CALENDER
define('TR_TRIP_COUNT_DAYS', 'Calculate');
define('TR_CALENDAR_LINK', 'Choose');

define('TR_CALENDER_MOUNTH_1', 'January');
define('TR_CALENDER_MOUNTH_2', 'February');
define('TR_CALENDER_MOUNTH_3', 'March');
define('TR_CALENDER_MOUNTH_4', 'April');
define('TR_CALENDER_MOUNTH_5', 'May');
define('TR_CALENDER_MOUNTH_6', 'June');
define('TR_CALENDER_MOUNTH_7', 'July');
define('TR_CALENDER_MOUNTH_8', 'August');
define('TR_CALENDER_MOUNTH_9', 'September');
define('TR_CALENDER_MOUNTH_10', 'October');
define('TR_CALENDER_MOUNTH_11', 'November');
define('TR_CALENDER_MOUNTH_12', 'December');

define('TR_CALENDER_TODAY', 'Today');
define('TR_CALENDER_LETTER_OF_DAY_1', 'M');
define('TR_CALENDER_LETTER_OF_DAY_2', 'Tu');
define('TR_CALENDER_LETTER_OF_DAY_3', 'W');
define('TR_CALENDER_LETTER_OF_DAY_4', 'Th');
define('TR_CALENDER_LETTER_OF_DAY_5', 'F');
define('TR_CALENDER_LETTER_OF_DAY_6', 'Sa');
define('TR_CALENDER_LETTER_OF_DAY_7', 'Su');

define('TR_TEXT_OWNER', 'Właściciel');
define('TR_TEXT_ADM_ADMINISTRATORS', 'Administratorzy');
define('TR_TEXT_ADM_ACCEPTATORS', 'Osoby akceptujące');
define('TR_TEXT_NON_ADMIN_LIST', 'Employees without permission');
define('TR_TEXT_NON_ACC_LIST', 'Employees without permission');
define('TR_TEXT_ADMINS_LIST', 'Employees with permissions');
define('TR_TEXT_ACC_LIST', 'Employees with permissions');
