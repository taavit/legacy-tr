<?php
require_once('smtp.php');
  /* Uncomment when using SASL authentication mechanisms */
            /**/
require_once("sasl.php");

class SmtpMail
{
    private $main_mail;
    protected $smtp;
    protected $from;
    protected $to;
    protected $sub;
    protected $content;
    protected $fromText=TR_TEXT_MAIL_FROM_TEXT;
    public function __construct()
    {
        $this->smtp=new smtp_class;
        $this->smtp->host_name=MAIL_HOST_NAME;       /* Change this variable to the address of the SMTP server to relay, like "smtp.myisp.com" */
        $this->smtp->host_port=MAIL_PORT;                /* Change this variable to the port of the SMTP server to use, like 465 */
        $this->smtp->ssl=0;                       /* Change this variable if the SMTP server requires an secure connection using SSL */
        $this->smtp->localhost="";       /* Your computer address */
        $this->smtp->direct_delivery=0;           /* Set to 1 to deliver directly to the recepient SMTP server */
        $this->smtp->timeout=10;                  /* Set to the number of seconds wait for a successful connection to the SMTP server */
        $this->smtp->data_timeout=0;              /* Set to the number seconds wait for sending or retrieving data from the SMTP server.
                                                                                    Set to 0 to use the same defined in the timeout variable */
        $this->smtp->debug=0;                     /* Set to 1 to output the communication with the SMTP server */
        $this->smtp->html_debug=0;                /* Set to 1 to format the debug output as HTML */
        $this->smtp->pop3_auth_host="";           /* Set to the POP3 authentication host if your SMTP server requires prior POP3 authentication */
        $this->smtp->user=MAIL_USER;                     /* Set to the user name if the server requires authetication */
        $this->smtp->realm="";                    /* Set to the authetication realm, usually the authentication user e-mail domain */
        $this->smtp->password=MAIL_PASSWORD;                 /* Set to the authetication password */
        $this->smtp->workstation="";              /* Workstation name for NTLM authentication */
        $this->smtp->authentication_mechanism=""; /* Specify a SASL authentication method like LOGIN, PLAIN, CRAM-MD5, NTLM, etc..
                                                                                    Leave it empty to make the class negotiate if necessary */

        /*
        * If you need to use the direct delivery mode and this is running under
        * Windows or any other platform that does not have enabled the MX
        * resolution function GetMXRR() , you need to include code that emulates
        * that function so the class knows which SMTP server it should connect
        * to deliver the message directly to the recipient SMTP server.
        */
        if ($this->smtp->direct_delivery) {
            if (!function_exists("GetMXRR")) {
                /*
                * If possible specify in this array the address of at least on local
                * DNS that may be queried from your network.
                */
                $_NAMESERVERS=array();
                include("getmxrr.php");
            }
            /*
            * If GetMXRR function is available but it is not functional, to use
            * the direct delivery mode, you may use a replacement function.
            */
            /*
            else
            {
                $_NAMESERVERS=array();
                if(count($_NAMESERVERS)==0)
                    Unset($_NAMESERVERS);
                include("rrcompat.php");
                $this->smtp->getmxrr="_getmxrr";
            }
            */
        }
        $this->main_mail=MAIL_MAIN_MAIL;
    }

    public function set($from, $to, $sub, $content)
    {
        $this->from=$from;
        $this->to=explode(',', $to);
        $this->sub=$sub;
        $this->content=$content;
    }

    public function setFromText($text)
    {
        $this->fromText=$text;
    }
    
    public function send()
    {
        $to=$this->to[0];
        for ($i=1;$i<count($this->to);$i++) {
            $to.=", ".$this->to;
        }
        if ($this->smtp->SendMessage($this->main_mail, $this->to, array("From:".$this->fromText." <".$this->from.">","To: ".$to,"Subject: ".$this->sub,"Date: ".strftime("%a, %d %b %Y %H:%M:%S %Z")
        ), $this->content)) {
            return true;
        } else {
            return false;
        }
    }
}
