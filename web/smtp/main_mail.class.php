<?php
require_once('smtp/SmtpMail.class.php');

class main_mail
{
    private $mail;
    private $db;
    public function main_mail()
    {
        $this->db=new MainPDO;
        $this->mail=new SmtpMail;
    }

    public function init()
    {
        $this->db->connect();
    }

    //false- blad
    public function sendToEmployee($id_trip)
    {
        $trip=$this->db->getTrip($id_trip);
        $m=file_get_contents(__DIR__.'/../../app/Resources/views/mail/to_employee.tpl');
        $m=str_replace("{id}", 'tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT), $m);
        $m=str_replace("{link}", WWW_HOST_NAME."/index2.php?page=tripdet&id=".$trip->getId(), $m);
        if ($trip->getStatus()==2) {
            $m=str_replace("{accept}", TR_TEXT_MAIL_ACCEPTED_TRIP, $m);
        } elseif ($trip->getStatus()==3) {
            $m=str_replace("{accept}", TR_TEXT_MAIL_REJECTED_TRIP, $m);
        } else {
            return false;
        }
        $m=str_replace("{name_accept}", $this->db->getName($trip->getIdAccept()), $m);
        $m=explode("\n\n\n", $m);
        $sub=$m[0];
        $content=$m[1];
        $this->mail->setFromText(TR_TEXT_MAIL_FROM_TEXT);
        $this->mail->set($this->db->getMail($trip->getIdAccept()), $this->db->getMail($trip->getIdEmployee()), $sub, $content);
        if ($this->mail->send()) {
            return "wyslano";
        } else {
            "failed";
        }
    }

    ///1-ok; 0-failed
    public function sendToSupervisor($id_trip)
    {
        $trip=$this->db->getTrip($id_trip);
        if ($trip->getStatus()!=1) {
            return false;
        }
        $m=file_get_contents(__DIR__.'/../../app/Resources/views/mail/to_supervisor.tpl');
        $m=str_replace("{name_employee}", $this->db->getName($trip->getIdEmployee()), $m);
        $m=str_replace("{link}", WWW_HOST_NAME."/index2.php?page=tripdet&id=".$trip->getId(), $m);
        $m=explode("\n\n\n", $m);
        $sub=$m[0];
        $content=$m[1];
        $this->mail->setFromText(TR_TEXT_MAIL_FROM_TEXT);
        $this->mail->set($this->db->getMail($trip->getIdEmployee()), $this->db->getMail($trip->getIdAccept()), $sub, $content);
        if ($this->mail->send()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function sendToTravelComp($id_trip)
    {
        $trip=$this->db->getTrip($id_trip);
        if ($trip->getStatus()!=2) {
            return false;
        }
        $m=file_get_contents(__DIR__.'/../../app/Resources/views/mail/to_travel_comp.tpl');
        $m=str_replace("{id}", 'tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT), $m);
        $m=str_replace("{transp}", $this->db->getTransp($trip->getIdTransp())->getName(), $m);
        $citys=$this->db->getCity($trip->getIdStartCity());
        $m=str_replace("{start_city}", $citys->getName(), $m);
        $city=$this->db->getCity($trip->getIdCity());
        $m=str_replace("{end_city}", $this->db->getCity($trip->getIdCity())->getName(), $m);
        $m=str_replace("{start_country}", $this->db->getCountry($citys->getIdCountry())->getName(), $m);
        $m=str_replace("{end_country}", $this->db->getCountry($city->getIdCountry())->getName(), $m);
        $m=str_replace("{start_date}", $trip->getStartDate(), $m);
        $m=str_replace("{end_date}", $trip->getEndDate(), $m);
        $m=str_replace("{comment}", $trip->getDescript(), $m);
        $emp=$this->db->getEmployee($trip->getIdEmployee());
        $m=str_replace("{name_employee}", $emp->getForename()." ".$emp->getSurname(), $m);
        $m=str_replace("{email_employee}", $emp->getMail(), $m);
        $m=str_replace("{tel_employee}", $emp->getPhone(), $m);
        $m=str_replace("{name_accept}", $this->db->getName($trip->getIdAccept()), $m);
        
        if ($trip->getIfHotel()) {
            $hotel=file_get_contents(__DIR__.'/../../app/Resources/views/mail/to_travel_comp_hotel.tpl');
            $hotel=str_replace("{start_date}", $trip->getTripHotel()->getStartDate(), $hotel);
            $hotel=str_replace("{end_date}", $trip->getTripHotel()->getEndDate(), $hotel);
            $hotel=str_replace("{days}", $trip->getTripHotel()->getDays(), $hotel);
            $h=$this->db->getHotel($trip->getTripHotel()->getIdHotel());
            $hotel=str_replace("{city}", $this->db->getCity($h->getIdCity())->getName(), $hotel);
            $hotel=str_replace("{hotel_name}", $h->getName(), $hotel);
            $m=str_replace("{hotel}", $hotel, $m);
        } else {
            $m=str_replace("{hotel}", '', $m);
        }
        $m=explode("\n\n\n", $m);
        $sub=$m[0];
        $content=$m[1];
        $this->mail->setFromText($this->db->getName($trip->getIdEmployee()));
        $this->mail->set($emp->getMail(), TR_TEXT_MAIL_TO_TRAVEL_COMPANY, $sub, $content);
        if ($this->mail->send()) {
            return "wyslano";
        } else {
            "failed";
        }
    }

    public function sendToFinance($id_trip)
    {
        $trip=$this->db->getTrip($id_trip);
        if ($trip->getStatus()!=2) {
            return false;
        }
        if ($trip->getIfAdvance()==false) {
            return false;
        }
        $m=file_get_contents(__DIR__.'/../../app/Resources/views/mail/to_finance.tpl');
        $emp=$this->db->getEmployee($trip->getIdEmployee());
        $m=str_replace("{name_employee}", $emp->getForename()." ".$emp->getSurname(), $m);
        $m=str_replace("{email_employee}", $emp->getMail(), $m);
        $m=str_replace("{tel_employee}", $emp->getPhone(), $m);
        $m=str_replace("{start_date}", $trip->getStartDate(), $m);
        $m=str_replace("{end_date}", $trip->getEndDate(), $m);
        $c=$this->db->getCity($trip->getIdCity());
        $m=str_replace("{country}", $this->db->getCountry($c->getIdCountry())->getName(), $m);
        $m=str_replace("{currency}", $this->db->getCurr($trip->getAdvance()->getIdCurr())->getName(), $m);
        $m=str_replace("{value}", $trip->getAdvance()->getValue(), $m);
        if ($trip->getAdvance()->getIfMoney()) {
            $m=str_replace("{cash}", TR_FORM_TRIP_ADVANCE_MONEY, $m);
        } else {
            $m=str_replace("{cash}", TR_FORM_TRIP_ADVANCE_NO_MONEY, $m);
        }
        $m=explode("\n\n\n", $m);
        $sub=$m[0];
        $content=$m[1];
        $this->mail->setFromText(TR_TEXT_MAIL_FROM_TEXT);
        $this->mail->set($emp->getMail(), TR_TEXT_MAIL_TO_FINANCE, $sub, $content);
        if ($this->mail->send()) {
            return "wyslano";
        } else {
            "failed";
        }
    }
}
