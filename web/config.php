<?php
define('CS_ROWS_PER_PAGE', 5); //Ilość wierszy pobieranych z bazy na raz
define('TR_DAYS_AFTER_CLOSE_REJECTED', 30);//minimalna ilosc dni po ilu odrzucone delegacje zostana zarchiwizowane

/*************************************
        mail configuration
*************************************/
define('MAIL_HOST_NAME', "#############");
define('MAIL_PORT', 25);
define('MAIL_USER', '########');
define('MAIL_PASSWORD', '#########');
define('MAIL_MAIN_MAIL', '#############');



define('TR_TEXT_MAIL_ACCEPTED_TRIP', 'accepted');
define('TR_TEXT_MAIL_REJECTED_TRIP', 'rejected');
define('TR_TEXT_MAIL_FROM_TEXT', 'Travel Team');
define('TR_TEXT_MAIL_TO_FINANCE', '###########');
define('TR_TEXT_MAIL_TO_TRAVEL_COMPANY', '###########');


/*************************************
        www configuration
 *************************************/
    define('WWW_HOST_NAME', 'http://localhost:8000');
