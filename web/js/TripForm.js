function isNumDay()
{
// 	alert (document.tripform.hotel_days.value.charAt('a'));
	if(isNaN(document.tripform.hotel_days.value) || (document.tripform.hotel_days.value.lastIndexOf('.')!=-1) || (document.tripform.hotel_days.value.lastIndexOf(',')!=-1))
	{
		alert('Liczba dób musi być liczbą naturalną>0');
		document.tripform.hotel_days.value='';
	}
}

function isNumValue()
{
	if(isNaN(document.tripform.advance_value.value)|| (document.tripform.advance_value.value.lastIndexOf('.')!=-1) || (document.tripform.advance_value.value.lastIndexOf(',')!=-1))
	{
		alert('Wartość zaliczki musi być liczbą całkowitą');
		document.tripform.advance_value.value='';
	}
}

function getNumDays()
{
	start=document.tripform.hotel_start_date.value;
	end=document.tripform.hotel_end_date.value;
	if(!isDate(start,'y-M-d'))
	{
		alert('Nieprawidłowy format daty zameldowania w hotelu');
		return;
	}
	if(!isDate(end,'y-M-d'))
	{
		alert('Nieprawidłowy fromat wymeldowania z hotelu');
		return;
	}
	time1=getDateFromFormat(start,'y-M-d');
	time2=getDateFromFormat(end,'y-M-d');
	days=(time2-time1)/(1000*60*60*24);
	if(days<=0)
	{
		alert('Nieprawidłowa kolejność dat w zamawianiu hotelu');
		return false;
	}
	document.tripform.hotel_days.value=days;
}


function isHotel()
{
	if(document.tripform.is_hotel.checked==true)
	{
		xajax_X_Hotel(document.tripform.city.value,document.tripform.start_date.value,document.tripform.end_date.value);
	}
	else document.getElementById('hotel').innerHTML ='';
}

function isAdvance()
{
	if(document.tripform.is_advance.checked==true)
	{
		xajax_X_Advance();
	}
	else document.getElementById('advance').innerHTML ='';
}

function changeCity()
{
	if(document.tripform.is_hotel.checked==true)
	{
		xajax_X_Hotel(document.tripform.city.value,document.tripform.start_date.value,document.tripform.end_date.value);
	}

}

function changeCountry()
{
	xajax_X_Country(document.tripform.country.value,document.tripform.is_hotel.checked);
}

function changeCountry2()
{
	xajax_X_Country2(document.tripform.country_from.value);
}
