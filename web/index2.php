<?php
require_once('classes/commonfunctions.php');
session_start();
require_once('config.php');

require_once(__DIR__.'/../vendor/autoload.php');

use Symfony\Component\Debug\Debug;
Debug::enable();

$_SESSION['blocked']=file_exists('.lockfile');

if (!isset($_GET['app'])) {
    $_GET['app']='travreq';
}
if (!isset($_SESSION['user'])) {
    $url='index2.php?';
    foreach ($_GET as $klucz => $wartosc) {
        $url.=$klucz.'__'.$wartosc.'~';
    }

    redirect('index.php?url='.$url);
}

$xajax = new xajax;


$twigLoader = new \Twig_Loader_Filesystem(__DIR__.'/../app/Resources/views/');
$twig = new \Twig_Environment($twigLoader);

$db=new MainPDO;
$db->connect();

// $tpl=new TTemplate;
// $tpl->setTplFile(__DIR__.'/../app/Resources/views/main.tpl');
$content='';
$usr=&$_SESSION['user'];

if ($_SESSION['blocked']) {
    if ($db->isAdmin($usr->getId_logged(), $a)) {
        if ($a!=1) {
            redirect("index.php?a=logout");
        }
    } else {
        redirect('index.php?a=logout');
    }
}

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

$params['page_header'] = generateHeader($request, $twig);

$content = LoadnRunMod('', $request->query->get('page', 'MainMenu'));
$params['content'] = $content;
$params['TR_TEXT_LOGGED_AS'] = TR_TEXT_LOGGED_AS;
$params['logged'] = $db->getName($usr->getId_logged());
if (isset($_SESSION['ERR']) /*&& $_SESSION['ERR']!=null*/) {
    $params['error'] = $_SESSION['ERR'];
    unset($_SESSION['ERR']);
} else {
    $params['error'] = '';
}

$xajax->processRequests();
$head=$xajax->getJavascript();
if ($request->query->get('page') == 'TripForm') {
    $head.="<SCRIPT LANGUAGE=\"JavaScript\" SRC=\"js/CalendarPopup.js\"></SCRIPT>
            <SCRIPT LANGUAGE=\"JavaScript\" SRC=\"js/date.js\"></SCRIPT>
            <SCRIPT LANGUAGE=\"JavaScript\">document.write(getCalendarStyles());</SCRIPT>";
        //http://www.mattkruse.com/javascript/calendarpopup/
    $js=file_get_contents("js/TripForm_schem.js");
    $js=str_replace('TR_FORM_TRIP_DAYS_MUST_BE_NUMERIC', TR_FORM_TRIP_DAYS_MUST_BE_NUMERIC, $js);
    $js=str_replace('TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE', TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE, $js);
    $js=str_replace('TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE', TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE, $js);
    $js=str_replace('TR_FORM_TRIP_WRONG_HOTEL_ORDER_DATES', TR_FORM_TRIP_WRONG_HOTEL_ORDER_DATES, $js);
    $js=str_replace('TR_FORM_TRIP_ADVANCE_VALUE_MUST_BE_NUMERIC', TR_FORM_TRIP_ADVANCE_VALUE_MUST_BE_NUMERIC, $js);
    file_put_contents("js/TripForm.js", $js);
}

$params['ajaxhead'] = $head;
$jsfile='js/'.$request->query->get('page', 'MainMenu').'.js';
$javascript='';
if (!isset($_GET['a'])) {
    $_GET['a']='';
}
if (file_exists($jsfile)) {
    $javascript='<script type="text/javascript" src="'.$jsfile.'"></script>';
}
$params['javascript'] = $javascript;

echo $twig->render('main.html.twig', $params);
