<?php
/// Plik klasy służąca do komunikacji z bazą danych 'main'
/** @file mainPDO.class.php */

// require_once('archPDO.class.php');
require_once('config.php');

use Taavit\TravelRequest\Model\City;
use Taavit\TravelRequest\Model\Country;
use Taavit\TravelRequest\Model\Currency;
use Taavit\TravelRequest\Model\Purpose;
use Taavit\TravelRequest\Model\Transport;

///Klasa komunikująca się z bazą danych
    /**Klasa pobiera dane w bazy, oraz zapisuje je. Dziedzieczy i jednocześnie zawiera ArchPDO.
    @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavicjusz@gmail.com)
    @date 17-11-2007
    */
class MainPDO
{
    ///Obiekt słuzacy do archiwizacji- łączy sie z baza arch.
    // protected $arch;
    protected $conn;

    /**metoda tworząca obiekt PDO z ustawieniami specyficznymi dla bazy. Wywłoanie jej jest konieczne do poprawnego dzialania klasy. Jeśli operacja ta się nie powiedzie, wyświetlany jest błąd.
    @return void
    */
    public function connect()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'url' => 'pgsql://pracownik:tremployee@localhost/travelrequest',
        );
        $this->conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        try {
            $this->db = new PDO('pgsql:host=localhost; port=5432;dbname=travelrequest', 'pracownik', 'tremployee');
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //====================employee
    //---------------set
            /** \defgroup Employee_settery Edycja danych pracownika *//** @{ */
    /// metoda pobiera iles wierszy  z bazy op pracownikach zdefiniowanych w parametrach od danego miejsca, sortujac je w zdefiniowany parametrami sposob
    /** @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $filtr po czym sortujemy
    @param $kolejnosc rosnaco czy malejaco
    @return tablicę obiektów typu Employee z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeActiveEmployee($ilosc = 10, $poczatek = 0, $filtr = 'dmodif_date', $kolejnosc = 'desc')
    {
        $w=array();
        $res = $this->db->prepare("select *  from temployee where id not in (select id_emp  from tactive) order by ? ? limit ? offset ?");
        $stmt= "select *  from temployee where id not in (select id_emp  from tactive) order by $filtr $kolejnosc limit $ilosc offset $poczatek";
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Employee");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    /// metoda podaje liczbę aktywnych pracowników
    /**
    @return liczbę aktywnych pracowników
    */
    public function countActiveEmployee()
    {
        $w=array();
        $stmt="select count(*)  from temployee where id not in (select id_emp  from tactive)";
        $res = $this->db->query($stmt);
        $o=$res->fetch();
        return $o['count'];
    }

    /// Metoda pozwala na edycję danych pracownika. Dane są wprowadzane do bazy aktualnej (main) oraz archiwalnej (arch)
    /**
    Wyświetla błąd w razie problemów z transakcją
    @return logiczne zero gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $emp obiekt typu Employee, zawierający nowe dane pracownika
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function setEmployee($emp, $id_admin)
    {
        $this->db->beginTransaction();
        $stmt="update temployee set slogin='".$emp->getLogin()."', sforename='".$emp->getForename()."', ssurname='".$emp->getSurname();
        $stmt.="', smail='".$emp->getMail()."', sphone='".$emp->getPhone()."' , dmodif_date=current_date, id_modif_by='".$id_admin."' where id=".$emp->getId();
        $ret=$this->db->exec($stmt);
        if ($ret) {
            ;
            $emp=$this->getActiveEmployeeByLogin($emp->getLogin());
            $ret=$this->arch->archEmployee_($emp, $id_admin);
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }


    ///metoda dodaje nowego użytkownika
    /**Metoda nie sprawdza czy jest już taki login. Metoda nie wpisuje id pracownika do bazy.
            Generuje go sama.
    @return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
            @param $emp obiekt typu Employee, zawierający nowe dane pracownika
            @param $id_admin identyfikator osoby modyfikującej
            @param $pass zaszyfrowane hasło pracownika, jeśli ktoś nie poda hasła, nie zaloguje się.
    */
    public function addEmployee($emp, $id_admin, $pass = '')
    {
        $this->db->beginTransaction();
        $stmt="insert into temployee(slogin, spass,sforename, ssurname,smail, sphone, id_created_by, dcreation_date, dmodif_date, id_modif_by) values('";
        $stmt.=$emp->getLogin()."','".$pass."','".$emp->getForename()."', '".$emp->getSurname()."','";
        $stmt.=$emp->getMail()."','".$emp->getPhone()."','".$id_admin."',current_date, current_date,'".$id_admin."' )";
        $ret=$this->db->exec($stmt);
        if ($ret) {
            $emp=$this->getActiveEmployeeByLogin($emp->getLogin());
            $ret=$this->arch->archEmployee_($emp, $id_admin, null, null, $pass);
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda rozpoczyna procedurę usuwania użytkownika. Jeśli dany pracownik nie może zostać skasowany, unieaktywnia się go.
    /**	@return logiczne zero, gdy operacja się nie powiodła, prawdę w przeciwnym wypadku
    @param $id identyfikator pracownika, którego usuwamy
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function delEmployee($id, $id_admin)
    {
        $noacitve=false;
        $res = $this->db->prepare("select count(*) from ttrip where id_emp=? or id_accept=?");
        $res->execute(array($id,$id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        if ($obj['count']>0) {
            $noacitve=true;
        } else {
            $res=$this->db->prepare("select count(*) from tmape where id_created_by=? or id_owner=?");
            $res->execute(array($id,$id));
            $res->setFetchMode(PDO::FETCH_ASSOC);
            $obj = $res->fetch();
            if ($obj['count']>0) {
                $noacitve=true;
            } elseif ($this->isAdmin($id, $db)) {
                $tab=array("taccept","tcity", "tcountry", "tcurrency", "temployee","temployee_accept", "thotel", "ttransp");
                foreach ($tab as $t) {
                    $stmt="select count(*) from $t where id_created_by='".$id."' and id_modif_by='".$id."' ";
                    $res=$this->db->query($stmt);
                    $w=$res->fetch();
                    if ($w['count']>0) {
                        $noacitve=true;
                        break;
                    }
                }
                if (!$noactive) {
                    //tactive jako amdin
                    $res = $this->db->prepare("select count(*) from tactive where id_admin_time=? or id_admin_deleg=?");
                    $res->execute(array($id,$id));
                    $res->setFetchMode(PDO::FETCH_ASSOC);
                    $obj = $res->fetch();
                    if ($obj['count']>0) {
                        $noacitve=true;
                    }
                }
            }
        }
        if ($noacitve) {
            $res = $this->db->prepare("select count(*) from tactive where id_emp=?");
            $res->execute(array($id));
            $res->setFetchMode(PDO::FETCH_ASSOC);
            $obj = $res->fetch();
            if ($obj['count']!=0) {
                $stmt="update tactive set bdeleg='false',id_admin_deleg='".$id_admin."'  where id_emp='".$id."' ";
                $ret=$this->db->exec($stmt);
            } else {
                $stmt="insert into tactive (id_emp, bdeleg, id_admin_deleg) values ('".$id."','false','".$id_admin."')";
                $ret=$this->db->exec($stmt);
                if ($ret) {
                    $this->changedEmployee($id, $id_admin);
                    $this->arch->changedEmployee($id, $id_admin);
                }
            }
        } else {
            $res = $this->db->prepare("select count(*) from tactive where id_emp=?");
            $res->execute(array($id));
            $res->setFetchMode(PDO::FETCH_ASSOC);
            $obj = $res->fetch();
            //jest juz taki wpis
            if ($obj['count']!=0) {
                $res = $this->db->prepare("select btime from tactive where id_emp=?");
                $res->execute(array($id));
                $res->setFetchMode(PDO::FETCH_ASSOC);
                $obj = $res->fetch();
                //time moze juz skasowac
                if ($obj['btime']) {
                    $ret=$this->delEmployee_($id);
                } else {
                    $stmt="update tactive set bdeleg='true',id_admin_deleg='".$id_admin."'  where id_emp='".$id."' ";
                    $ret=$this->db->exec($stmt);
                }
            } else {
                $stmt="insert into tactive (id_emp, bdeleg, id_admin_deleg) values ('".$id."','true','".$id_admin."')";
                $ret=$this->db->exec($stmt);
                if ($ret) {
                    $stmt="update temployee set dmodif_date=current_date ,id_modif_by='".$id_admin."'  where id='".$id."' ";
                    $ret=$this->db->exec($stmt);
                }
            }
        }
        if ($ret) {
            return true;
        } else {
            return false;
        }
    }

        ///metoda usuwa użytkownika
    /**	@return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id identyfikator pracownika, którego usuwamy
    */
    protected function delEmployee_($id)
    {
        $stmt ="delete from temployee where id ='".$id."'";
        ;
        return $this->db->exec($stmt);
    }

    ///metoda sprawdza, czy pracownik o danym id istnieje już w tabeli tactive.
    /**jeśli tak, to znaczy, że powinien być nieaktywny
    @return ilość wierszy w tabeli z takim id
    @param $id identyfikator pracownika
    */
    public function checkIdActive($id)
    {
        $res = $this->db->prepare("select count(*) from tactive where id_emp=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return $obj['count'];
    }

    ///metoda sprawdza, czy pracownik o danym imieniu i nazwisku jest już w bazie
    /**
    @return false, jeśli nie ma użytkownika, true, jeśli jest użytkownik
    @param &$employee obiekt typu Employee, zawiera dane sprawdzanego pracownika. Dodatkowo, jeśli pracownik o takim imieniu i nazwisku zostanie znaleziony, to w polu id obiektu wpisywany jest znaleziony identyfikator
    @param &$db pole, w którym, jeśli znaleziono danego pracownika pojawi się identyfikator bazy, w której go znaleziono: 1-baza main; 2- baza arch
    */
    public function isEmployeeIN(&$employee, &$db)
    {
        $stmt="select id from temployee where sforename='".$employee->getForename()."' and ssurname='".$employee->getSurname()."' ";
        $res=$this->db->query($stmt);
        $w=$res->fetch();
        if ($w) {
            $db=1;
            $employee->setId($w['id']);
            return true;
        } else {
            $res=$this->arch->db->query($stmt);
            $w=$res->fetch();
            if ($w) {
                $db=2;
                $employee->setId($w['id']);
                return true;
            } else {
                return false;
            }
        }
    }

    ///metoda przywraca pracownika do aktualnej bazy
    /**
    @return logiczne zero, jeśli operacja się nie powiodła,  w przeciwnym razie ilość zmodyfikowanych wierszy
    @param $id znaleziony identyfikator pracownika
    @param $db identyfikator bazy: 1-main; 2-arch
    @param $id_admin identyfikator administratora wprowadzającego zmiany
    */
    public function restoreEmployee($id, $db, $id_admin)
    {
        if ($db==1) {
            $stmt="select id_admin_time from tactive where id_emp='".$id."' ";
            $res=$this->db->query($stmt);
            $obj=$res->fetch();
            if ($obj['id_admin_time']===null) {
                $this->db->beginTransaction();
                $stmt="delete from tactive where id_emp='".$id."' ";
                $ret=$this->db->exec($stmt);
                if ($ret) {
                    $this->changedEmployee($id, $id_admin);
                }
                if ($ret) {
                    $this->db->commit();
                } else {
                    $this->db->rollBack();
                }
            }
        } elseif ($db==2) {
            $this->db->beginTransaction();
            $employee=$this->arch->getEmployee($id);
            $info=$this->arch->getEmployeeInfo($id);
            $ret= $this->archEmployee_($employee, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            if ($ret) {
                $ret=$this->arch->archEmployee_($employee, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            }
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
            return $ret;
        }
    }

    
        /// Metoda pobiera z bazy dane wszystkich pracowników
    /**
    @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywnych (nie oczekujących na skasowanie) pracowników; false-wszystkich, true-tylko aktywnych
    @return tablicę obiektów typu Employee zawierającą dane pracowników
    */
    
    public function getAllEmployees($active=false)
    {
        $res = $this->db->query("select id,slogin,sforename, ssurname, smail, sphone from temployee order by sforename");
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Employee");
        $i=0;
        while ($obj = $res->fetch()) {
            if ($active) {
                if ($this->checkIdActive($obj->getId())) {
                    $i++;
                    continue;
                }
            }
            $w[$i]=$obj;
            $w[$i]->setIdAccept($this->getIdAccept($w[$i]->getId()));
            $i++;
        }
        return $w;
    }

    ///metoda dodaje istniejącego pracownika do listy osób akceptujących delegacje
    /**	@return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id_emp identyfikator pracownika
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function addAccept($id_emp, $id_admin)
    {
        $this->db->beginTransaction();
        $stmt="insert into taccept values('".$id_emp."','1', '".$id_admin."', current_date, current_date, '".$id_admin."' )";
        $ret=$this->db->exec($stmt);
        if ($ret) {
            $ret=$this->arch->archAccept($id_emp, $id_admin);
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }


    ///metoda dodaje pracownikowi osobę akceptującą delegacje
    /**	Metoda nie pozwala dodać wpisu, gdzie identyfikator pracownika oraz osoby akceptującej mu delegacje będzie taki sam.
    @return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id_emp identyfikator pracownika
    @param $id_accept identyfikator osoby akceptującej
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function addEmpAccept($id_emp, $id_accept, $id_admin)
    {
        if ($id_emp!=$id_accept) {
            $this->db->beginTransaction();
            $stmt="insert into temployee_accept values('".$id_emp."','".$id_accept."','".$id_admin."', current_date, current_date, '".$id_admin."' )";
            $ret=$this->db->exec($stmt);
            if ($ret) {
                $ret=$this->arch->archEmpAccept($id_emp, $id_accept, $id_admin);
            }
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
            return $ret;
        } else {
            return false;
        }
    }

    ///metoda kasuje przyporządkowanie pracownikowi danej osoby akceptującej delegacje
    /**	@return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id_emp identyfikator pracownika
    @param $id_accept identyfikator osoby akceptującej
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function delEmpAccept($id_emp, $id_accept, $id_admin)
    {
        $this->db->beginTransaction();
        $ret=$this->arch->archEmpAccept($id_emp, $id_accept, $id_admin);
        if ($ret) {
            $stmt ="delete from temployee_accept where id ='".$id_emp."' and id_accept='".$id_accept."' ";
            $ret= $this->db->exec($stmt);
            if ($ret) {
                $stmt="update temployee_accept set dmodif_date=current_date ,id_modif_by='".$id_admin."'  where id='".$id_emp."' ";
                $ret=$this->db->exec($stmt);
            }
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda usuwa pracownika z listy osób akceptujących delegacje.
    /**Jeśli są jakieś nie zamknięte delegacje, które dany pracownik zaakceptował, staje się on nieaktywny jako osoba akceptująca
    @return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id identyfikator pracownika
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function delAccept($id, $id_admin)
    {
        $res=$this->db->prepare("select count(*) from ttrip where id_accept=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        if ($obj['count']!=0) {
            $this->db->beginTransaction();
            $stmt="update taccept set bactive='false',  dmodif_date=current_date ,id_modif_by='".$id_admin."' where id_emp='".$id."'";
            $ret=$this->db->exec($stmt);
            if ($ret) {
                $ret=$this->arch->archAccept($id, $id_admin);
            }
            if ($ret) {
                $this->db->commit();
                return 1;
            } else {
                $this->db->rollBack();
                return 0;
            }
        } else {
            $this->db->beginTransaction();
            $this->arch->archAccept($id, $id_admin);
            $ret=$this->delAccept_($id);
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
        }
        if ($ret==0) {
            return 0;
        }
        return 2;
    }

    ///metoda usuwa danego pracownika z listy osób akceptujących delegacje
    /**	@return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id identyfikator pracownika
    */
    protected function delAccept_($id)
    {
        $stmt ="delete from taccept where id_emp ='".$id."' ";
        $ret= $this->db->exec($stmt);
        return $ret;
    }


    ///metoda sprawdza, czy osoba o danym identyfikatorze kiedykolwiek mogła akceptować delegacje
    /** @return false, gdy operacja się nie powiodła, w przeciwnym wypadku true
    @param $id identyfikator pracownika
    @param &$db pole, w którym, jeśli znaleziono danego pracownika pojawi się identyfikator bazy, w której go znaleziono: 1-baza main; 2- baza arch
    */
    public function isAccept($id, &$db)
    {
        $res=$this->checkIdAccept($id);
        if ($res) {
            $db=1;
            return true;
        } else {
            $res=$this->arch->checkIdAccept($id);
            if ($res) {
                $db=2;
                return true;
            } else {
                return false;
            }
        }
    }

    ///metoda sprawdza, czy dana osoba akceptująca jest aktywna
    /** @return true, jeśli jest aktywna, false w przeciwnym wypadku
    @param $id identyfikator osoby akceptującej
    */
    public function isActiveAccept($id)
    {
        $stmt ="select bactive from taccept where id_emp='".$id."' ";
        $res=$this->db->query($stmt);
        $w=$res->fetch();
        return $w['bactive'];
    }

    ///metoda przywraca prawo akceptowania delegacji danej osobie
    /**
    @return logiczne zero, jeśli operacja się nie powiodła, w przeciwnym wypadku prawdę.
    @param $id znaleziony identyfikator pracownika
    @param $db identyfikator bazy: 1-main; 2-arch
    @param $id_admin identyfikator administratora
    */
    public function restoreAccept($id, $db, $id_admin)
    {
        if ($db==1) {
            if (!$this->isActiveAccept($id)) {
                $this->db->beginTransaction();
                $stmt="update taccept set bactive='true', dmodif_date=current_date, id_modif_by='".$id_admin."' where id_emp='".$id."' ";
                $ret=$this->db->exec($stmt);
                if ($ret) {
                    $info=$this->arch->getAcceptInfo($id);
                    $ret=$this->arch->archAccept($id, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
                }
                if ($ret) {
                    $this->db->commit();
                } else {
                    $this->db->rollBack();
                }
                return $ret;
            }
        } elseif ($db==2) {
            $this->db->beginTransaction();
            $info=$this->arch->getAcceptInfo($id);
            $ret= $this->archAccept($id, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            if ($ret) {
                $ret=$this->arch->archAccept($id, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            }
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
            return $ret;
        }
    }

        /// Metoda pobiera z bazy identyfikatory wszystkich osób mogących acceptować delegacje
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywnych (nie oczekujących na skasowanie) osób mogących akceptować delegacje; false-wszystkich,
     true-tylko aktywnych
    @return tablicę obiektów typu Employee- wszystkich osób mogących acceptować delegacje
    */
    public function getAllAccept($active=false)
    {
        $stmt="select id,slogin,sforename, ssurname, smail, sphone from temployee where id in (select id_emp from taccept ";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $stmt.=")";
        if ($active) {
            $stmt.=" and id not in (select id_emp from tactive)";
        } else {
            $stmt.=" where id not in (select id_emp from tactive)";
        }
        $res = $this->db->query($stmt);
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Employee");
        while ($obj = $res->fetch()) {
            $obj->setIdAccept($this->getIdAccept($obj->getId()));
            $w[]=$obj;
        }
        return $w;
    }
    
    /// Metoda pobiera z bazy identyfikatory wszystkich osób mogących acceptować delegacje
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywnych (nie oczekujących na skasowanie) osób mogących akceptować delegacje; false-wszystkich,
    true-tylko aktywnych
    @return tablicę tablic asocjacyjnych- wszystkich osób mogących acceptować delegacje
    */
    public function getAllAcceptT($active=false)
    {
        if ($active) {
            $stmt="select id,slogin,sforename, ssurname, smail, sphone from temployee where id in (select id_emp from taccept where bactive='true') ";
        } else {
            $stmt="select id,slogin,sforename, ssurname, smail, sphone from temployee where id in (select id_emp from taccept) ";
        }
        $res = $this->db->query($stmt);
        $w=array();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($obj = $res->fetch()) {
            if (!$this->checkIdActive($obj['id'])) {
                $obj['id_accept']=array();
                $obj['id_accept']=$this->getIdAccept($obj['id']);
                $w[]=$obj;
            }
        }
        return $w;
    }

    ///metoda sprawdza, czy osoba o danym identyfikatorze kiedykolwiek mogła akceptować delegacje danemu pracownikowi
    /** @return false, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id_emp identyfikator pracownika
    @param $id_accept identyfikator osoby akceptującej delegacje
    @param &$db pole, w którym, jeśli znaleziono danego pracownika pojawi się identyfikator bazy, w której go znaleziono: 1-baza main; 2- baza arch
    */
    public function isEmpAccept($id_emp, $id_accept, &$db)
    {
        $stmt="select count(*) from temployee_accept where id='".$id_emp."' and id_accept='".$id_accept."' ";
        $res=$this->db->query($stmt);
        $w=$res->fetch();
        if ($w['count']!=0) {
            $db=1;
            return true;
        } else {
            $res=$this->arch->db->query($stmt);
            $w=$res->fetch();
            if ($w['count']!=0) {
                $db=2;
                return true;
            } else {
                return false;
            }
        }
    }

    ///metoda przywraca prawo akceptowania danemu pracownikowi delegacji przez daną osobę
    /**
    @return false, jeśli operacja się nie powiodła, w przeciwnym wypadku true.
    @param $id_emp identyfikator pracownika
    @param $id_accept identyfikator osoby akceptującej delegacje
    @param $db identyfikator bazy: 1-main; 2-arch
    @param $id_admin identyfikator osoby modyfikującej
    */
    public function restoreEmpAccept($id_emp, $id_accept, $db, $id_admin)
    {
        if ($db==1) {
            return 1;
        } elseif ($db==2) {
            $this->db->beginTransaction();
            $info=$this->arch->getEmployeeAcceptInfo($id_emp, $id_accept);
            $ret= $this->archEmpAccept($id_emp, $id_accept, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            if ($ret) {
                $ret=$this->arch->archEmpAccept($id_emp, $id_accept, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            }
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
            return $ret;
        }
    }

    /** @} */

//======================TRIP
        /** \defgroup trip_ Delegacje*/
        /**  @{ */

    /// metoda pobiera z bazy podaną w parametrze liczbę wierszy zawierające dane o delegacjach danego pracownika
        /** pobiera tylko delegacje z bazy main, czyli ich status musi być mniejszy od 5
        @param $tstatus tablica pięcio elementowa zawierająca informację o statusach, np. 10000, mówi, że pobieramy tlyko delegację o statusie 0, czyli kopie robocze
        @param $ilosc ilość wierszy do pobrania
        @param $poczatek numer wiersza od którego zaczynamy pobierać
        @param $id_user identyfikator użytkownika
        @param $filtr nazwa kolumy po której sortujemy wyniki
        @param $kolejnosc kolejność sorotowania-rosnaco czy malejaco
        @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
        */
    protected function getSomeTripByTStatusTo5AndUser($tstatus, $ilosc=10, $poczatek=0, $id_user=null, $filtr='dmodif', $kolejnosc='desc')
    {
        //pierwszy war bez and
        $w=array();
        $bool=false;
        $filtr_='';
        for ($i=0;$i<5;$i++) {
            if (!$bool && $tstatus[$i]) {
                $filtr_.=' where istatus in ('.$i;
                $bool=true;
            } elseif ($tstatus[$i]) {
                $filtr_.=", $i ";
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        if ($id_user!==null) {
            $filtr_.=" and id_emp='".$id_user."' ";
        }
        $stmt="select * from ttrip $filtr_ order by $filtr $kolejnosc, id desc limit $ilosc offset $poczatek";
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }
        return $w;
    }

    /// metoda pobiera z bazy podaną w parametrze liczbę wierszy zawierające dane o delegacjach danego pracownika
    /**
    @param $tstatus tablica 7 elementowa statusów, czyli np 1000000 - bierze tylko te delegacje, w ktorym status =0
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @param $filtr kolumna po której sortujemy
    @param $kolejnosc  kolejność sorotowania -rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTripByTStatusAndUser($tstatus, $ilosc=10, $poczatek=0, $id_user=null, $filtr='dmodif', $kolejnosc='desc')
    {
        if ($poczatek==0) {
            $pocztek_archa=0;
        }
        $w=array();
        $filtr_='';
        $il_main=$this->countTripByTStatusTo5AndUser($tstatus, $id_user);
        if ($poczatek+$ilosc<=$il_main) {
            $w=$this->getSomeTripByTStatusTo5AndUser($tstatus, $ilosc, $poczatek, $id_user, $filtr, $kolejnosc);
        } elseif ($poczatek<$il_main) {
            $w=$this->getSomeTripByTStatusTo5AndUser($tstatus, $ilosc, $poczatek, $id_user, $filtr, $kolejnosc);
            if ($tstatus[5] || $tstatus[6]) {
                $t=$this->arch->getSomeTripByTStatusClosedAndUser($tstatus, $ilosc-count($w), 0, $id_user, $filtr, $kolejnosc);
                $w=array_merge($w, $t);
            }
        } elseif ($tstatus[5] || $tstatus[6]) {
            $w=$this->arch->getSomeTripByTStatusClosedAndUser($tstatus, $ilosc, $poczatek-$il_main, $id_user, $filtr, $kolejnosc);
        }
        return $w;
    }

    ///  metoda pobiera z bazy podaną w parametrze liczbę wierszy zawierające dane o delegacjach danej osoby akceptującej
    /** pobiera tylko delegacje z bazy main, czyli ich status musi być mniejszy od 5
    @param $tstatus tablica 5 elementowa statusów, czyli np 10000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_accept id osoby akceptujacej, dla ktorej pobiera sie delegacje
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $filtr kolumna po której sortujemy
    @param $kolejnosc  kolejność sorotowania -rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTripByTStatusTo5AndAccept($tstatus, $id_accept, $ilosc=10, $poczatek=0, $filtr='dmodif', $kolejnosc='desc')
    {
        $w=array();
        $filtr_='';
        //pierwszy war bez and
                $bool=false;
        for ($i=0;$i<5;$i++) {
            if (!$bool && $tstatus[$i]) {
                $filtr_.=' where istatus in ('.$i;
                $bool=true;
            } elseif ($tstatus[$i]) {
                $filtr_.=", $i ";
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        $filtr_.=" and id_accept='".$id_accept."' ";
        $stmt="select * from ttrip $filtr_ order by $filtr $kolejnosc, id desc limit $ilosc offset $poczatek";
// 		echo $stmt;
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }
        return $w;
    }


    /// metoda pobiera z bazy podaną w parametrze liczbę wierszy zawierające dane o delegacjach danej osoby akceptującej
    /**
    @param $tstatus tablica 7 elementowa statusów, czyli np 1000000 - bierze tylko te delegacje, w ktorym status =0
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $accept id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @param $filtr kolumna po której sortujemy
    @param $kolejnosc  kolejność sorotowania -rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTripByTStatusAndAccept($tstatus, $accept, $ilosc=10, $poczatek=0, $filtr='dmodif', $kolejnosc='desc')
    {
        if ($poczatek==0) {
            $pocztek_archa=0;
        }
        $w=array();
        $filtr_='';
        $il_main=$this->countTripByTStatusTo5AndAccept($tstatus, $accept);
        if ($poczatek+$ilosc<=$il_main) {
            $w=$this->getSomeTripByTStatusTo5AndAccept($tstatus, $accept, $ilosc, $poczatek, $filtr, $kolejnosc);
        } elseif ($poczatek<$il_main) {
            $w=$this->getSomeTripByTStatusTo5AndAccept($tstatus, $accept, $ilosc, $poczatek, $filtr, $kolejnosc);
            if ($tstatus[5] || $tstatus[6]) {
                $t=$this->arch->getSomeTripByTStatusClosedAndAccept($tstatus, $accept, $ilosc- count($w), $poczatek, $filtr, $kolejnosc);
                $w=array_merge($w, $t);
            }
        } elseif ($tstatus[5] || $tstatus[6]) {
            $w=$this->arch->getSomeTripByTStatusClosedAndAccept($tstatus, $accept, $ilosc, $poczatek-$il_main+1, $filtr, $kolejnosc);
        }
        return $w;
    }

    
    /// metoda zwraca policzoną ilość wierszy zawierające dane o delegacjach danego pracownika
    /**
    @param $tstatus tablica 7 elementowa statusów, czyli np 1000000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @return ilość wierszy zawierające dane o delegacjach danego pracownika
    */
    public function countTripByTStatusAndUser($tstatus, $id_user=null)
    {
        $w=$this->countTripByTStatusTo5AndUser($tstatus, $id_user);
        if ($tstatus[5] || $tstatus[6]) {
            $w=$w+$this->arch->countTripByTStatusClosedAndUser($tstatus, $id_user);
        }
        return $w;
    }

    ///metoda zwraca policzoną ilość wierszy zawierające dane o delegacjach danej osoby akceptującej
    /**
    @param $tstatus tablica 7 elementowa statusów, czyli np 1000000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_accept id akceptatora, którego pobiera się delegację, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @return ilość wierszy zawierające dane o delegacjach danej osoby akceptującej
    */
    public function countTripByTStatusAndAccept($tstatus, $id_accept)
    {
        $w=$this->countTripByTStatusTo5AndAccept($tstatus, $id_accept);
        if ($tstatus[5] || $tstatus[6]) {
            $w=$w+$this->arch->countTripByTStatusClosedAndAccept($tstatus, $id_accept);
        }
        return $w;
    }


    ///metoda modyfikuje dana delegację
    /** @param $trip obiekt typu Trip z wypełnionymi polami z datami w formacie YYYY-MM-DD
    @return logiczne zero jeśli operacja się nie powiodła, w przeciwnym wypadku ilość zmodyfikowanych wierszy
    */
    public function setTrip($trip)
    {
        $old_trip=$this->getTrip($trip->getId());
        if ($trip->getIfAdvance()) {
            $ia='true';
        } else {
            $ia='false';
        }
        if ($trip->getIfHotel()) {
            $ih='true';
        } else {
            $ih='false';
        }
        $this->db->beginTransaction();
        $stmt="update ttrip set  id_emp= '".$trip->getIdEmployee()."', id_accept='".$trip->getIdAccept();
        $stmt.="',id_purp='".$trip->getIdPurp()."', dstart_date='".$trip->getStartDate()."',";
        $stmt.=" dend_date='".$trip->getEndDate()."',dmodif=current_date,";
        $stmt.="badvance='".$ia."', bhotel='".$ih."',id_transp= '".$trip->getIdTransp()."',id_city='".$trip->getIdCity()."',id_start_city='";
        $stmt.=$trip->getIdStartCity()."',istatus='".$trip->getStatus()."',";
        $stmt.="sdescript='".$trip->getDescript()."' ,sdescript2='".$trip->getDescript2()."' where id='".$trip->getId()."'";
        $ret=$this->db->exec($stmt);
        if ($ret) {
            if ($trip->getIfHotel()) {
                if ($old_trip->getIfHotel()) {
                    $stmt="delete from thotel_trip where id_trip='".$trip->getId()."'";
                    $ret=$this->db->exec($stmt);
                }
                if ($ret) {
                    $h=$trip->getTripHotel();
                    $stmt="insert into thotel_trip values('".$h->getIdTrip()."','".$h->getIdHotel()."','".$h->getStartDate()."','".$h->getEndDate()."','".$h->getDays()."')";
                    $ret=$this->db->exec($stmt);
                }
            } else {
                if ($old_trip->getIfHotel()) {
                    $stmt="delete from thotel_trip where id_trip='".$trip->getId()."'";
                    $ret=$this->db->exec($stmt);
                }
            }
        }
        if ($ret) {
            if ($trip->getIfAdvance()) {
                if ($old_trip->getIfAdvance()) {
                    $stmt="delete from tadvance where id_trip='".$trip->getId()."'";
                    $ret=$this->db->exec($stmt);
                }
                if ($ret) {
                    $a=$trip->getAdvance();
                    if ($a->getIfMoney()) {
                        $m='true';
                    } else {
                        $m='false';
                    }
                    $stmt="insert into tadvance values('".$a->getIdTrip()."','".$a->getValue()."','".$a->getIdCurr()."','".$m."')";
                    $ret=$this->db->exec($stmt);
                }
            } else {
                if ($old_trip->getIfAdvance()) {
                    $stmt="delete from tadvance where id_trip='".$trip->getId()."'";
                    $ret=$this->db->exec($stmt);
                }
            }
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda zamyka poprawnie delegację
    /**Zamyka delegację i perznosi ją do bazy archiwalnej
    @param $id identyfikator delegacji do zamknicia
    @return logiczną prawdę, jeśli operacja się powiedzie w przeciwnym wypadku logiczne zero
    */
    public function closeTrip($id)
    {
        $this->setTripStatus($id, 5);
        $t=$this->getTrip($id);
    }

    ///Kasuje delegacje o danym id
    /**metoda przenosi delegację z bazy main do bazy arch
    @param $id identyfikator delegacji do zamknicia
    @return ilość zmodyfikowanych wierszy, jeśli operacja si powiedzie w przeciwnym wypadku logiczne zero
    */
    public function delTrip($id)
    {
        {
            $this->db->beginTransaction();
            $ret=$this->delTrip_($id);
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
        }
        return $ret;
    }

    ///metoda kasuje delegację z bazy main
    /**
    @param $id identyfikator delegacji do zamknicia
    @return ilość zmodyfikowanych wierszy, jeśli operacja się powiedzie w przeciwnym wypadku logiczne zero
    */
    protected function delTrip_($id)
    {
        $stmt="delete from ttrip where id='".$id."'";
        return $this->db->exec($stmt);
    }

    ///metoda dodaje nową delegacje
    /** Pozawala tylko raz dziennie dodać taką samą delegację, tzn przez jedną osobę, akceptowaną przez tego samego akceptatora,itd. Nawet jeśli zostaną zmienione szczegółowe dane dot. pobytu w hotelu, czy zaliczki, dodanie delegacji nie będzie możliwe
     @param $trip obirkt typu Trip. Identyfikator nie będzie brany pod uwagę
    @return -1. jeśli pracownik zamawiał już taką samą delegację tego dnia. Logiczne zero, jeśli operacja się nie powiedzie z innych względów, w przeciwnym przypadku ilość zmodyfikowanych wierszy
    */
    public function addTrip($trip)
    {
        if ($trip->getIfAdvance()) {
            $ia='true';
        } else {
            $ia='false';
        }
        if ($trip->getIfHotel()) {
            $ih='true';
        } else {
            $ih='false';
        }
        $stmt="select count(*) from ttrip where id_emp= '".$trip->getIdEmployee()."' and id_accept= '".$trip->getIdAccept()."' and id_purp='".$trip->getIdPurp()."'";
        if ($trip->getStartDate()!='') {
            $stmt.=" and dstart_date='".$trip->getStartDate()."'";
        } elseif ($trip->getStatus()!=0) {
            return false;
        } else {
            $stmt.=" and dstart_date is null";
        }
        if ($trip->getEndDate()!='') {
            $stmt.=" and dend_date='".$trip->getEndDate()."'";
        } elseif ($trip->getStatus()!=0) {
            return false;
        } else {
            $stmt.=" and dend_date is null";
        }
        $stmt.=" and dmodif=current_date and badvance='".$ia."' and bhotel='".$ih;
        $stmt.="' and id_transp='".$trip->getIdTransp()."' and id_city= '".$trip->getIdCity()."' and id_start_city= '".$trip->getIdStartCity();
        $stmt.="' and istatus= '".$trip->getStatus()."' and sdescript= '".$trip->getDescript()."' and sdescript2= '".$trip->getDescript2()."'";
        $res=$this->db->query($stmt);
        $ret=$res->fetch();
        if ($ret['count']>0) {
            return -1;
        }
        $ret;
        $this->db->beginTransaction();
        $stmt="insert into ttrip(id_emp,id_accept,id_purp,";
        if ($trip->getStartDate()!='') {
            $stmt.="dstart_date,";
        }
        if ($trip->getEndDate()!='') {
            $stmt.="dend_date,";
        }
        $stmt.="dmodif,badvance,bhotel,id_transp,id_city,id_start_city,istatus,sdescript,sdescript2) values('";
        $stmt.=$trip->getIdEmployee()."','".$trip->getIdAccept()."','".$trip->getIdPurp()."',";
        if ($trip->getStartDate()!='') {
            $stmt.="'".$trip->getStartDate()."',";
        }
        if ($trip->getEndDate()!='') {
            $stmt.="'".$trip->getEndDate()."',";
        }
        $stmt.="current_date,'".$ia."','".$ih."','".$trip->getIdTransp()."','".$trip->getIdCity()."','".$trip->getIdStartCity();
        $stmt.="','".$trip->getStatus()."','".$trip->getDescript()."','".$trip->getDescript2()."')";
        $ret=$this->db->exec($stmt);
        if ($ret && ($trip->getIfHotel() || $trip->getIfAdvance())) {
            //pobierz id trip
                    $stmt="select id from ttrip where id_emp= '".$trip->getIdEmployee()."' and id_accept= '".$trip->getIdAccept()."' and id_purp='".$trip->getIdPurp()."'";
            if ($trip->getStartDate()!='') {
                $stmt.=" and dstart_date='".$trip->getStartDate()."'";
            } elseif ($trip->getStatus()!=0) {
                return false;
            } else {
                $stmt.=" and dstart_date is null ";
            }
            if ($trip->getEndDate()!='') {
                $stmt.=" and dend_date='".$trip->getEndDate()."'";
            } elseif ($trip->getStatus()!=0) {
                return false;
            } else {
                $stmt.=" and dend_date is null ";
            }
            $stmt.="and dmodif=current_date and badvance='".$ia."' and bhotel='".$ih."' and id_transp='".$trip->getIdTransp()."'";
            $stmt.=" and id_city= '".$trip->getIdCity()."' and id_start_city= '".$trip->getIdStartCity();
            $stmt.="' and istatus= '".$trip->getStatus()."' and sdescript= '".$trip->getDescript()."' and sdescript2= '".$trip->getDescript2()."'";
            $res=$this->db->query($stmt);
            $ret=$res->fetch();
            $id=$ret['id'];
            if ($trip->getIfHotel()) {
                $h=$trip->getTripHotel();
                $stmt="insert into thotel_trip values('".$id."',";
                if ($h->getIdHotel()!='') {
                    $stmt.="'".$h->getIdHotel()."',";
                } elseif ($trip->getStatus()!=0) {
                    return false;
                } else {
                    $stmt.=" null, ";
                }
                if ($h->getStartDate()!='') {
                    $stmt.="'".$h->getStartDate()->format('Y-m-d H:i:s')."',";
                } elseif ($trip->getStatus()!=0) {
                    return false;
                } else {
                    $stmt.=" null, ";
                }
                if ($h->getEndDate()!='') {
                    $stmt.="'".$h->getEndDate()->format('Y-m-d H:i:s')."',";
                } elseif ($trip->getStatus()!=0) {
                    return false;
                } else {
                    $stmt.=" null, ";
                }
                if ($h->getDays()!='') {
                    $stmt.="'".$h->getDays()."'";
                } elseif ($trip->getStatus()!=0) {
                    return false;
                } else {
                    $stmt.=" null ";
                }
                $stmt.=")";
                $ret=$this->db->exec($stmt);
            }
            if ($ret) {
                if ($trip->getIfAdvance()) {
                    $a=$trip->getAdvance();
                    if ($a->getIfMoney()) {
                        $money='true';
                    } else {
                        $money='false';
                    }
                    $stmt="insert into tadvance values('".$id."','".$a->getValue()."','".$a->getIdCurr()."','".$money."')";
                    $ret=$this->db->exec($stmt);
                }
            }
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda ustawia status konkretnej delegacji
    /**  @param $id identyfikator delegacji
        @param $status status podróży. Możliwe statusy:
    0-kopia robocza
    1- oczekuje na zaakceptowanie
    2- zaakceptowana
    3- niezaakceptowana
    4- rozliczona
    5- zamknięta poprawnie
    6- zamknięta przez program
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function setTripStatus($id, $status)
    {
        $this->conn->update(
            'ttrip',
            ['istatus' => $status, 'dmodif_date' => (new \DateTime())->format(\DateTime::DATE_ISO8601)],
            ['id' => $id]
        );
    }

    ///metoda ustawia komentarz akceptującego delegację
    /**
    @param $id identyfikator delegacji
    @param $descript komentarz akceptującego
    @param $id_admin identyfikator osoby akceptującej
    @return logiczny fałsz w razie niepowodzenia, w przeciwnym wypadku ilość zmodyfikowanych wierszy
    */
    public function setDescript2($id, $descript, $id_admin)
    {
        $this->conn->update(
            'ttrip',
            [
                'sdescript2' => $descript,
                'dmodif_date' => (new \DateTime())->format(\DateTime::DATE_ISO8601),
            ],
            [
                'id' => $id
            ]
        );
    }

    /** @} */
//=====================HOTEL
        /** \defgroup hotel_ Edycja danych hoteli*/
        /**  @{ */

    ///metoda dodaje Hotel
        /** @param $hotel obiekt typu Hotel z wypełnionymi danymi hotelu. Identyfikator nie będzie brany pod uwagę
        @param $id_admin identyfikator osoby dodającej hotel
        @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    public function addHotel($hotel, $id_admin)
    {
        return $this->conn->insert(
            'thotel',
            [
                'id_city' => $hotel->getIdCity(),
                'sname' => $hotel->getName(),
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'integer',
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

    ///metoda edytuje dany Hotel. Używa transakcji
        /** @param $hotel obiekt typu Hotel z wypełnionymi danymi hotelu.
    @param $id_admin identyfikator osoby dodającej hotel
    @return logiczne zero w przypadku niepowodzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    public function setHotel($hotel, $id_admin)
    {
        $this->db->beginTransaction();
        $ret=$this->setHotel_($hotel, $id_admin);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

        ///metoda edytuje dany Hotel. Nie używa transakcji
        /** @param $hotel obiekt typu Hotel z wypełnionymi danymi hotelu.
    @param $id_admin identyfikator osoby dodającej hotel
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    protected function setHotel_($hotel, $id_admin)
    {
        if ($hotel->getActive()) {
            $a='true';
        } else {
            $a='false';
        }
        $stmt="update thotel set sname='".$hotel->getName()."', id_city= '".$hotel->getIdCity()."', dmodif_date=current_date, id_modif_by='".$id_admin."', bactive='".$a."' where id='".$hotel->getId()."'";
        $ret=$this->db->exec($stmt);
        if ($ret) {
            $ret=$this->arch->archHotel_($hotel, $id_admin);
        }
        return $ret;
    }
    
    ///metoda kasuje hotel, lub gdy jest on używany w delegacji, ustawia go na nieaktywny
    /** @param $id identyfikator hotelu
    @param $id_admin identyfikator osoby kasującej hotel
    @return 0- wystąpił błąd, 1-oczekuje na skasowanie, 2-skasowano
    */
    public function delHotel($id, $id_admin)
    {
        $this->conn->delete('thotel', ['id_hotel' => $id]);
    }

    ///metoda sprawdza, czy dany hotel jest aktywny
    /** @return true, jeśli jest aktywny, false w przeciwnym wypadku
    @param $id identyfikator hotelu
    */
    public function isActiveHotel($id)
    {
        return $this->conn->fetchColumn(
            'select bactive from thotel where id=:id',
            [ 'id' => $id ]
        );
    }

    ///metoda pobiera z bazy dane wszystkich hoteli
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywny (nie oczekujące na skasowanie) hotele; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Hotel.
    */
    public function getAllHotels($active = false)
    {
        $w=array();
        $stmt="select id, id_city,sname, bactive from thotel";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Hotel");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    ///metoda pobiera z bazy dane wszystkich hoteli
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) hotele; false-wszystkie, true-tylko aktywne
    @return tablicę tablic asocjacyjnych, zawierającą dane wszystkich hoteli
    */
    public function getAllHotelsT($active=false)
    {
        $w=array();
        $stmt="select id, id_city,sname, bactive from thotel";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

        ///metoda pobiera z bazy dane hoteli na podstawie identyfikatora miasta, w którym się znajdują
    /** @param $cityid identyfikator miasta
    @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywny (nie oczekujące na skasowanie) hotele; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Hotel.
    */
    public function getHotelByCityId($cityid, $active=true)
    {
        $w=array();
        $stmt="select id, id_city,sname, bactive from thotel where id_city=?";
        if ($active) {
            $stmt.=" and bactive='true'";
        }
        $res = $this->db->prepare($stmt);
        $res->execute(array( $cityid));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Hotel");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    /** @} */
//====================CURRENCY
        /** \defgroup curr_ Edycja danych waluty*/
        /**  @{ */

    ///metoda dodaje walutę do bazy
        /** @param $name_curr nazwa waluty
        @param $id_admin identyfikator administratora
        @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    public function addCurr($name_curr, $id_admin)
    {
        return $this->conn->insert(
            'tcurrency',
            [
                'sname' => $name_curr,
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

    ///metoda modyfikuje walutę.  Nie opiera sie na transakcji
    /** @param $name obiekt typu Name zawieający dane waluty
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function setCurr($currency, $id_admin)
    {
        return $this->conn->update(
            'tcurrency',
            [
                'sname' => $currency->getName(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin,
                'bactive' => $currency->getActive()
            ],
            [
                'id' => $currency->getId()
            ]
        ) ;
    }
    
    ///metoda usuwa walutę, lub jeśli jest ona używana, ustawia ją na nieaktywną
    /** @param $id identyfikator waluty
    @param $id_admin identyfikator administratora
    @return 0- błąd; 1- oczekuje na usunięcie; 2-usunięto
    */
    public function delCurr($id, $id_admin)
    {
        $this->conn->delete('tcurrency', ['id' => $id]);
    }

    ///metoda sprawdza czy jest dana waluta w bazie
    /** @param &$name obiekt typu Name z wypełnioną nazwą waluty. Identyfikator jest nieistotny
    @param  &$db pole, w którym, jeśli znaleziono daną walutę pojawi się identyfikator bazy, w której go znaleziono: 1-baza main; 2- baza arch
    @return false, gdy operacja się nie powiodła, w przeciwnym wypadku true, a w polu id obiektu Name będzie identyfikator pod jakim znaleziono walutę.
    */
    public function isCurr(&$name, &$db)
    {
        $stmt="select id from tcurrency where sname='".$name->getName()."'";
        $res=$this->db->query($stmt);
        $w=$res->fetch();
        if ($w) {
            $db=1;
            $name->setId($w['id']);
            return true;
        } else {
            $res=$this->arch->db->query($stmt);
            $w=$res->fetch();
            if ($w) {
                $db=2;
                $name->setId($w['id']);
                return true;
            } else {
                return false;
            }
        }
    }

        ///metoda sprawdza, czy dana waluta jest aktywna
    /** @return true, jeśli jest aktywna, false w przeciwnym wypadku
    @param $id identyfikator waluty
    */
    public function isActiveCurr($id)
    {
        return (bool) $this->conn->fetchColumn(
            "select bactive from tcurrency where id=:id",
            [
                'id' => $id
            ]
        );
    }

        ///metoda pobiera z bazy nazwy i identyfikatory wszystkich walut
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) waluty; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Name.
    */
    public function getAllCurr($active=false)
    {
        $w=array();
        $stmt="select id, sname, bactive from tcurrency";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Currency");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    ///metoda pobiera z bazy nazwy i identyfikatory wszystkich walut
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) waluty; false-wszystkie, true-tylko aktywne
    @return tablicę tablic asjocacyjnych
    */
    public function getAllCurrT($active = false)
    {
        $w=array();
        $stmt="select id, sname, bactive from tcurrency";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    /** @} */
//====================TRANSPORT
        /** \defgroup transp_ Edycja środków transportu*/
        /**  @{ */
    //------------another

    ///metoda dodaje środek transportu
    /** @param $nazwa_transp nazwa transportu
        @param $id_admin identyfikator administratora
        @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    public function addTransp($nazwa_transp, $id_admin)
    {
        return $this->conn->insert(
            'ttransp',
            [
                'sname' => $nazwa_transp,
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

    ///metoda edytuje konkretny transport
    /** @param $name obiekt typu Name z wypełnionymi danymi
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function setTransp($name, $id_admin)
    {
        $this->db->beginTransaction();
        $ret=$this->setTransp_($name, $id_admin);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

        ///metoda edytuje konkretny transport. Nie uzywa transakcji
    /** @param $name obiekt typu Name z wypełnionymi danymi
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function setTransp_($name, $id_admin)
    {
        if ($name->getActive()) {
            $a='true';
        } else {
            $a='false';
        }
        $stmt="update ttransp set sname='".$name->getName()."', dmodif_date=current_date, id_modif_by='".$id_admin."', bactive='".$a."' where id= '".$name->getId()."'";
        
        $ret=$this->db->exec($stmt);
        if ($ret) {
            $ret=$this->arch->archTransp_($name, $id_admin);
        }
        return $ret;
    }

    ///metoda usuwa konkretny transport
    /** @param $id identyfikator transportu
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function delTransp($id)
    {
        $this->conn->delete('ttransp', ['id' => $id]);
    }

        ///metoda sprawdza, czy dany transport jest aktywny
    /** @return true, jeśli jest aktywny, false w przeciwnym wypadku
    @param $id identyfikator transportu
    */
    public function isActiveTransp($id): bool
    {
        $active = $this->conn->fetchColumn('select bactive from ttransp where id = :id', ['id' => $id]);
        return (bool) $active;
    }

        ///metoda pobiera z bazy nazwy i identyfikatory wszystkich środków transportu
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) środki transportu; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Name.
    */
    public function getAllTransp($active = false)
    {
        $w=array();
        $stmt="select id, sname, bactive from ttransp";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Transport");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

        ///metoda pobiera z bazy nazwy i identyfikatory wszystkich środków transportu
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) środki transportu; false-wszystkie, true-tylko aktywne
    @return tablicę tablic asjocacyjnych
    */
    public function getAllTranspT($active=false)
    {
        $w=array();
        $stmt="select id, sname, bactive from ttransp";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }


    /** @} */
//====================CITY
        /** \defgroup city_ Edycja miast*/
        /**  @{ */
    ///metoda dodaje miasto
    /** @param $nazwa_miasta nazwa miasta
        @param $id_panstwa identyfikator państwa
        @param $id_admin identyfikator administratora
        @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function addCity($nazwa_miasta, $id_panstwa, $id_admin)
    {
        return $this->conn->insert(
            'tcity',
            [
                'id_country' => $id_panstwa,
                'sname' => $nazwa_miasta,
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'integer',
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

        ///metoda edytuje konkretne miasto
    /** @param $city obiekt typu City z wypełnionymi danymi
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function setCity($city, $id_admin)
    {
        $this->conn->update(
            'tcity',
            [
                'sname' => $city->getName(),
                'id_country' => $city->getIdCountry(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin,
                'bactive' => $city->getActive()
            ],
            ['id' => $city->getId()]
        );
    }

    ///metoda usuwa konkretne miasto z bazy
    /** @param $id identyfikator miasta
    @return  logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function delCity($id)
    {
        $this->conn->delete('tcity', ['id' => $id]);
    }

    ///metoda pobiera z bazy dane wszystkich miast
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) miasta; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu City.
    */
    public function getAllCities($active=false)
    {
        $w=array();
        $stmt="select id,id_country, sname, bactive from tcity";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\City");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    ///metoda pobiera z bazy dane wszystkich miast
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) miasta; false-wszystkie, true-tylko aktywne
    @return tablicę tablic asjocacyjnych
    */
    public function getAllCitiesT($active=false)
    {
        $w=array();
        $stmt="select id,id_country, sname, bactive from tcity";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

        ///metoda pobiera z bazy identyfikatory miast, które leżą w jednym państwie
    /** @param $idcountry identyfikator pańswta
    @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) miasta; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu City
    */
    public function getCitiesOfCountry($idcountry, $active=true)
    {
        $w=array();
        $stmt="select id,id_country, sname, bactive from tcity where id_country=?";
        if ($active) {
            $stmt.=" and bactive='true'";
        }
        $res = $this->db->prepare($stmt);
        $res->execute(array( $idcountry));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\City");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }


        /** @} */
//==================COUNTRY
        /** \defgroup country_ Edycja państw*/
        /**  @{ */
        ///metoda dodaje państwo
    /** @param $nazwa_panstwa nazwa państwa
        @param $id_admin identyfikator administratora
        @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function addCountry($nazwa_panstwa, $id_admin)
    {
        return $this->conn->insert(
            'tcountry',
            [
                'sname' => $nazwa_panstwa,
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

    ///metoda edytuje konkretne państwo
    /** @param $name obiekt typu Name z wypełnionymi danymi
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function setCountry($name, $id_admin)
    {
        $this->db->beginTransaction();
        if ($name->getActive()) {
            $a='true';
        } else {
            $a='false';
        }
        $stmt="update tcountry set sname='".$name->getName()."', dmodif_date=current_date, id_modif_by='".$id_admin."', bactive='".$a."' where id= '".$name->getId()."'";
        $ret=$this->db->exec($stmt);
        if ($ret) {
            $ret=$this->arch->archCountry_($name, $id_admin);
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }


    ///metoda usuwa konkretne państwo, lub jeśli to niemożliwe robi je nieaktywnym, oraz miasta, które są podporządkowane danemu państwu też stają się nieaktywne, bądź są kasowane.Hotele również
    /** @param $id identyfikator państwa
    @param $id_admin identyfikator administratora
    @return 0-błąd, 1-oczekuje na skasowanie; 2-skasowanie
    */
    public function delCountry($id)
    {
        $this->conn->delete('tcountry', ['id' => $id]);
    }

    ///metoda sprawdza, czy dane państwo jest aktywne
    /** @return true, jeśli jest aktywne, false w przeciwnym wypadku
    @param $id identyfikator państwa
    */
    public function isActiveCountry($id)
    {
        return (bool) $this->conn->fetchColumn("select bactive from tcountry where id=:id", ['id'=>$id]);
    }

    ///metoda pobiera z bazy dane wszystkich państw
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) państwa; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Name.
    */
    public function getAllCountries($active = false)
    {
        $w=array();
        $stmt="select id, sname, bactive from tcountry";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Country");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

//====================ADMIN
        /** @} */
        /** \defgroup admin_ Edycja administratorów*/
        /**  @{ */

            ///metoda pobiera z bazy identyfikatory wszystkich administratorów
    /**
        @return tablicę obiektów typu Name.
    */
    public function getAllAdmins()
    {
        $w=array();
        $res = $this->db->query("select id_emp from tadmin where id_emp not in (select id_emp from tactive);");
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o['id_emp'];
        }
        return $w;
    }

        ///metoda dodaje do aplikacji nowego administratora.
        /** @return false, jeśli operacja się nie powiedzie, w przeciwnym przypadku ilość zmodyfikowanych wierszy
        @param $id identyfikator pracownika, który ma zostać administratorem
        @param $id_admin identyfikator administratora
        */
    public function addAdmin($id, $id_admin)
    {
        $this->conn->beginTransaction();
        $ret = $this->conn->insert(
            'tadmin',
            [$id, $id_admin, new \DateTime(), $id_admin]
        );
        if ($ret) {
            $ret=$this->arch->archAdmin($id, $id_admin);
        }
        if ($ret) {
            $this->conn->commit();
        } else {
            $this->conn->rollBack();
        }
        return $ret;
    }

    ///metoda usuwa z aplikacji administratora.
    /** @return false, jeśli operacja się nie powiedzie, w przeciwnym przypadku ilość zmodyfikowanych wierszy
    @param $id identyfikator administratora, którego usuwamy
    @param $id_admin identyfikator administratora
    */
    public function delAdmin($id, $id_admin)
    {
        $this->conn->beginTransaction();
        $ret = $this->conn->delete('tadmin', ['id_emp' => $id]);
        if ($ret) {
            $ret=$this->arch->archAdmin($id, $id_admin);
        }
        if ($ret) {
            $this->conn->commit();
        } else {
            $this->conn->rollBack();
        }
        if ($ret==0) {
            return 0;
        }
        return 2;
    }

        ///metoda sprawdza, czy pracownik o danym identyfikatorze był kiedykolwiek administatorem
    /** @return false, gdy operacja się nie powiodła, w przeciwnym wypadku true
    @param $id identyfikator pracownika
    @param &$db jeśli znaleziono dany identykator pojawi się tutaj identyfikator bazy, w której je znaleziono: 1-baza main; 2- baza arch
    */
    public function isAdmin($id, &$db)
    {
        $res=$this->checkIdAdmin($id);
        if ($res) {
            $db=1;
            return true;
        }
        return false;
    }

    ///metoda przywraca administratora
    /**
    @return logiczne zero, jeśli operacja się nie powiodła, w przeciwnym wypadku ilość zmodyfikowanych wieszy.
    @param $id identyfikator przywracanego administratora
    @param $db identyfikator bazy: 1-main; 2-arch
    @param $id_admin identyfikator administratora, który dokonuje operacji
    */
    public function restoreAdmin($id, $db, $id_admin)
    {
        if ($db==1) {
            return true;
        } elseif ($db==2) {
            $this->db->beginTransaction();
            $info=$this->arch->getAdminInfo($id);
            $ret= $this->archAdmin($id, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            if ($ret) {
                $ret=$this->arch->archAdmin($id, $id_admin, $info->getCreationDate(), $info->getCreatedBy());
            }
            if ($ret) {
                $this->db->commit();
            } else {
                $this->db->rollBack();
            }
            return $ret;
        }
    }

    //---------------CELE
    /** @} */
    /** \defgroup purp_ Edycja danych celów delegacji*/
    /**  @{ */

    ///metoda dodaje cel do bazy
    /** @param $name_purp nazwa celu
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
        */
    public function addPurp($name_purp, $id_admin)
    {
        return $this->conn->insert(
            'ttrip_purp',
            [
                'sname' => $name_purp,
                'id_created_by' => $id_admin,
                'dcreation_date' => new \DateTime(),
                'dmodif_date' => new \DateTime(),
                'id_modif_by' => $id_admin
            ],
            [
                'string',
                'integer',
                'datetime',
                'datetime',
                'integer'
            ]
        );
    }

    ///metoda modyfikuje cel delegacji. Nie Uzywa transakcji
    /** @param $name obiekt typu Name zawieający dane celu
    @param $id_admin identyfikator administratora
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function setPurp($name, $id_admin)
    {
        $this->conn->update(
            'ttrip_purp',
            [
                'sname'=>$name->getName(),
                'dmodif_date' => (new \DateTime())->format('Y-m-d H:i:s'),
                'active' => $name->getActive(),
                'id_modif_by' => $id_admin
            ],
            [
                'id' => $name->getId()
            ]
        );
    }

    ///metoda usuwa cel delegacji z bazy
    /** @param $id identyfikator celu
    @return logiczne zero w przypadku niepowidzenia, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function delPurp($id)
    {
        return $this->conn->delete('ttrip_purp', ['id' => $id]);
    }

        ///metoda pobiera z bazy nazwy i identyfikatory wszystkich celów
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) cele; false-wszystkie, true-tylko aktywne
    @return tablicę obiektów typu Name.
    */
    public function getAllPurp($active = false)
    {
        $w=array();
        $stmt="select id, sname, bactive from ttrip_purp";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Purpose");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    ///metoda pobiera z bazy nazwy i identyfikatory wszystkich celów
    /** @param $active zmienna typu boolean, mówi o tym czy pobierać tylko aktywne (nie oczekujące na skasowanie) cele; false-wszystkie, true-tylko aktywne
    @return tablicę tablic asjocacyjnych
    */
    public function getAllPurpT($active=false)
    {
        $w=array();
        $stmt="select id, sname, bactive from ttrip_purp";
        if ($active) {
            $stmt.=" where bactive='true'";
        }
        $res = $this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    /** @} */

        ///metoda pobiera wiersz o danym identyfikatorze
    /** @param $id identyfikator wiersza
    @return obiekt typu Mape, lub false w przypadku niepowodzenia
    */
    public function Mget($id)
    {
        $res=$this->db->prepare("select *  from tmape where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Mape");
        return $res->fetch();
    }

        ///metoda pobiera wszystkie wiersze zawierające inforacje o header-ach strony
    /**
    @return tablic obiektów obiekt typu Mape, lub false w przypadku niepowodzenia
    */
    public function MgetAll()
    {
        $res=$this->db->query("select * from tmape");
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS, "Mape");
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }

    ///metoda pobiera dane strony
    /** @param $name skrócona nazwa strony
    @return obiekt typu Mape, lub false w przypadku niepowodzenia
    */
    // public function MgetByShortName($name)
    // {
    //     $stmt="select * from tmape where sshort_name='".$name."'";
    //     $res=$this->db->query($stmt);
    //     $res->setFetchMode(PDO::FETCH_CLASS, "Mape");
    //     $ret=$res->fetch();
    //     var_dump($ret);
    //     return $ret;
    // }

    ///metoda pobiera dane strony
    /** @param $name skrócona nazwa strony
    @return obiekt typu Mape, lub false w przypadku niepowodzenia
    */
    public function MgetByShortName($name)
    {
        return $this->conn->fetchAssoc(
            "select * from tmape where sshort_name= :name",
            [
                'name' => $name
            ]
        );
    }

    ///metoda edytuje dane strony
    /**
    @param $mape zmieniony obiekt typu Mape
    @param $id_admin identyfikator administratora
    @return logiczną fałsz, jeśli operacja się nie powiedzie. W przeciwnym wypadku ilość zmodyfikowanych wierszy
    */
    public function Mset($mape, $id_admin)
    {
        $ret = $this->conn->update(
            'tmape',
            [
                'sshort_name' => $mape->getShortName(),
                'sfull_name' => $mape->getFullName(),
                'id_owner' => $mape->getIdOwner(),
                'dmodif_date' => (new DateTime())->format('Y-m-d H:i:s'),
                'id_created_by' => $mape->getIdCreatedBy(),
                'id_modif_by' => $id_admin,
                'dcreation_date' => $mape->getCreationDate()
            ],
            [
                'id' => $mape->getId()
            ]
        );

        if ($ret) {
            $ret=$this->arch->archCurr_($name, $id_admin);
        }
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///maetoda czyści bazę z nieaktywnych i nieużywanych wpisów.
    /**
    @return nic nie zwraca
    */
    public function clean()
    {
        $this->cleanAccept();
        $this->cleanHotels();
        $this->cleanCities();
        $this->cleanCountries();
        $this->cleanCurr();
        $this->cleanTransp();
        $this->cleanEmployee();
    }

    ///metoda usuwa nieaktywne wpisy o możliwości akceptowania delegacji.
    /**
    @return nic nie zwraca
    */
    protected function cleanAccept()
    {
        $stmt="select id_emp from taccept where id_emp not in (select id_accept from ttrip) and bactive=false; ";
        $res=$this->db->query($stmt);
        $w=array();
        while ($o=$res->fetch()) {
            $this->delAccept_($o['id_emp']);
        }
    }
    
    ///metoda usuwa nieaktywne hotele
    /**
    @return nic nie zwraca
    */
    protected function cleanHotels()
    {
        $stmt=" select id from thotel where id not in (select id_hotel from thotel_trip) and bactive=false";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delHotel($o['id']);
        }
    }

    ///metoda usuwa nieaktywne miasta
    /**
    @return nic nie zwraca
    */
    protected function cleanCities()
    {
        $stmt="select id from tcity where id not in (select id_city from ttrip union select id_start_city from ttrip union select id_city from thotel where thotel.id in(select id_hotel from thotel_trip))  and bactive=false ";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delCity($o['id']);
        }
    }

    ///metoda usuwa nieaktywne państwa
    /**
    @return nic nie zwraca
    */
    protected function cleanCountries()
    {
        $stmt="select id from tcountry where id in ( select id_country from tcity where tcity.id not in(select id_city from ttrip union select id_start_city from ttrip union select id_city from thotel where thotel.id in(select id_hotel from thotel_trip)))  and bactive=false";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delCountry($o['id']);
        }
    }

    ///metoda usuwa nieaktywne waluty
    /**
    @return nic nie zwraca
    */
    protected function cleanCurr()
    {
        $stmt=" select id from tcurrency where id not in (select distinct id_currency from tadvance ) and bactive = false";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delCurr($o['id']);
        }
    }

    ///metoda usuwa nieaktywne cele delegacji
    /**
    @return nic nie zwraca
    */
    protected function cleanPurp()
    {
        $stmt="select id from ttrip_purp where id not in (select id_purp from ttrip) and bactive = false";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delPurp($o['id']);
        }
    }

    ///metoda usuwa nieaktywne środki transportu
    /**
    @return nic nie zwraca
    */
    protected function cleanTransp()
    {
        $stmt="select id from ttransp where id not in (select id_transp from ttrip) and bactive = false";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delTransp($o['id']);
        }
    }

    ///metoda usuwa nieaktywnych pracowników
    /**
    @return nic nie zwraca
    */
    protected function cleanEmployee()
    {
        //obaj admini zaznaczyli na kasowanie
        $stmt="select id_emp from tactive where bdeleg=true and btime=true";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $this->delEmployee_($o['id_emp']);
        }

        //spraqwdzamy te, ktore inny admin chce skasowac
        $stmt="select id from temployee where id not in (select id_emp from ttrip union select id_accept from ttrip) and id in (select id_emp from tactive where bdeleg is not true and btime=true)";
        $res=$this->db->query($stmt);
        while ($o=$res->fetch()) {
            $id=$o['id'];
            $count = $this->conn->fetchColumn(
                "select count(*) from temployee where id in
						(
						select id_modif_by from ttransp   union select id_created_by from ttransp
						union select id_modif_by from temployee where id_modif_by is not null union select id_created_by from temployee where id_created_by is not null
						union select id_modif_by from ttrip_purp   union select id_created_by from ttrip_purp
						union select id_modif_by from tcity   union select id_created_by from tcity
						union select id_modif_by from tcountry   union select id_created_by from tcountry
						union select id_modif_by from taccept   union select id_created_by from taccept
						union select id_modif_by from tcurrency   union select id_created_by from tcurrency
						union select id_modif_by from thotel   union select id_created_by from thotel
						union select id_modif_by from tmape   union select id_created_by from tmape union select id_owner from tmape
						)
					and id=:id", ['id' => $id]);
            if ($count == 0) {
                $this->delEmployee_($id);
            }
        }
    }
    /// Metoda pobiera z bazy dane pracownika, którego login wchodzi przez parametr
    /**
    @return obiekt typu Employee zawierający dane pracownika, w przypadku błędu zwraca false
    @param $login login pracownika
    */
    public function getActiveEmployeeByLogin($login)
    {
        $res = $this->db->prepare("select id,slogin,sforename, ssurname, smail, sphone from temployee where slogin=? and id not in (select id_emp  from tactive)");
        $res->execute([$login]);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Employee");
        $w=$res->fetch();
        if ($w!==false) {
            $w->setIdAccept($this->getIdAccept($w->getId()));
        }
        return $w;
    }

        /** @return tablicę identyfikatorów osób mogących zaacceptować delegację danej osobie
    @param $id identyfikator osoby dla której zwraca się osoby mogące zaacceptować jej delegację
    @param $active zmienna mówiąca, czy zwrócić wszystkie osoby akceptujące danemu pracownikowi delegacje, czy tylko aktywne; 0-wszystkie; 1-aktywne
    */
    public function getIdAccept($id, $active=true)
    {
        $stmt="select id_accept from temployee_accept where id=?";
        if ($active) {
            $stmt.=" and id_accept in (select id_emp from taccept where bactive='true')";
        }
        $res = $this->db->prepare($stmt);
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $w=array();
        $i=0;
        while ($obj = $res->fetch()) {
            $w[$i]=$obj;
            $w[$i]=$w[$i]['id_accept'];
            $i++;
        }
        return $w;
    }

        ///metoda pobiera z bazy hasło pracownika
    /** @param $id identyfikator pracownika, jeśli nie ma osoby o takim identyfikatorze, zwraca false
    @return void
    */
    public function getPassw($id)
    {
        $res = $this->db->prepare("select spass from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $row = $res->fetch();
        if ($row !==false) {
            return $row['spass'];
        } else {
            return false;
        }
    }


        ///metoda sprawdza czy jest juz w bazie administrator o takim identyfikatorze
    /** @param $id identyfikator administratora
    @return liczbe wpisów z takim id
    */
    public function checkIdAdmin($id)
    {
        $res = $this->db->prepare("select count(*) from tadmin where id_emp=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return $obj['count'];
    }

    //============Employee
    /** \defgroup Employee Odczytywanie danych pracowników*/
    /**  @{ */
    /// Metoda pobiera z bazy dane pracownika, którego identyfikator wchodzi przez parametr
    /**
    @return obiekt typu Employee zawierający dane pracownika, jesli nie ma takiego id, to zwraca logiczny fałsz
    @param $id identyfikator osoby
    */

    public function getEmployee($id)
    {
        $res = $this->db->prepare("select id,slogin,sforename, ssurname, smail, sphone from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Employee");
        $w=$res->fetch();
        if ($w!==false) {
            $w->setIdAccept($this->getIdAccept($w->getId()));
        }
        return $w;
    }

    /// Metoda pobiera z bazy dane wszystkich pracowników
    /**
    @return tablicę obiektów typu Employee zawierającą dane pracowników
    */
    /*
    public function getAllEmployees()
    {
        $res = $this->db->query("select id,slogin,sforename, ssurname, smail, sphone from temployee order by sforename");
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Employee");
        $i=0;
        while ($obj = $res->fetch())
        {
            $w[$i]=$obj;
            $w[$i]->setIdAccept($this->getIdAccept($w[$i]->getId()));
            $i++;
        }
        return $w;
    }
*/
    
        /// Metoda pobiera z bazy dane wszystkich pracowników
    /**
    @return tablicę tablic asocjacyjnych zawierającą dane pracowników
    */
            /*
    public function getAllEmployeesT()
    {
        $res = $this->db->query("select id,slogin,sforename, ssurname, smail, sphone from temployee order by sforename");
        $w=array();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $i=0;
        while ($obj = $res->fetch())
        {
            $w[$i]=$obj;
            $w[$i]['id_accept']=array();
            $w[$i]['id_accept']=$this->getIdAccept($obj['id']);
            $i++;
        }
        return $w;
    }
*/
    
    /// Metoda pobiera z bazy imię i nazwisko pracownika, którego identyfikator wchodzi przez parametr
    /**
    @return ciąg znaków postaci "Imię Nazwisko", jeśli nie ma osoby o takim identyfikatorze, zwraca false
    @param $id identyfikator osoby
    */
    public function getName($id)
    {
        $res = $this->db->prepare("select sforename,ssurname from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $row = $res->fetch();
        if ($row !==false) {
            return $row['sforename']." ".$row['ssurname'];
        } else {
            return false;
        }
    }

    /// Metoda pobiera z bazy mail pracownika, którego identyfikator wchodzi przez parametr
    /**
    @return mail pracownika, jeśli nie ma osoby o takim identyfikatorze, zwraca false
    @param $id identyfikator osoby
    */
    public function getMail($id)
    {
        $res = $this->db->prepare("select smail from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $row = $res->fetch();
        if ($row !==false) {
            return $row['smail'];
        } else {
            return false;
        }
    }

    ///metoda pobiera identyfikator pracownika mając dany jego login
    /** @return identyfikator pracownika, jeśli nie ma osoby o takim identyfikatorze, zwraca false
    @param $login login pracownika
    */
    public function getIdByLogin($login)
    {
        $res = $this->db->prepare("select id from temployee where slogin=?");
        $res->execute(array($login));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $w=$res->fetch();
        if ($w!==false) {
            return $w['id'];
        }
        return false;
    }
    
    ///metoda sprawdza czy istnieje już dany login w bazie
    /**	@return ilość wierszy zawierających ten login.
    @param $login login pracownika
    */
    public function checkLogin($login)
    {
        $res = $this->db->prepare("select count(*) from temployee where slogin=?");
        $res->execute(array( $login));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return $obj['count'];
    }

    ///metoda zmienia date, oraz osobe, ktora ostatnio dokonala modyfikacji danego pracownika
    /** @param $id id osoby, ktorej dane ulegly zmianie
    @param $id_admin id osoby dokonujacej zmiany
    @return ilosc zmodyfikowanych wierszy
    */
    public function changedEmployee($id, $id_admin)
    {
        return $this->conn->executeUpdate(
            "update temployee set dmodif_date=current_date, id_modif_by=:id_admin where id = :id",
            [
                'id_admin' => $id_admin,
                'id' => $id
            ]
        );
    }

    ///metoda liczy wszystkich pracowników
    /**
    @return ilość wszystkich pracowników
    */
    /*
    function countEmployees()
    {
        $res = $this->db->query("select count(*) from temployee");
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return $obj['count'];
    }
    */

    ///metoda zwraca informacje administracyjne o użytkowniku - kiedy i przez kogo został dodany, kto i kiedy ostatnio modyfikował jego dane
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator użytkownika
    */
    public function getEmployeeInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }

    
    /// Metoda pobiera z bazy danych identyfikatory osób, którym osoba o identyfikatorze wchodzącym przez parametr może acceptować delegacje
    /**
    @return tablicę identyfikatorów osób, którym osoba o identyfikatorze wchodzącym przez parametr może acceptować delegacje
    @param $id_accept identyfikator osoby dla której zwraca się osoby, którym może ona acceptować delegacje
    */
    public function getIdEmployeeOfAccept($id_accept)
    {
        $res = $this->db->prepare("select id from temployee_accept where id_accept=?");
        $res->execute(array($id_accept));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $w=array();
        while ($obj = $res->fetch()) {
            $w[]=$obj['id'];
        }
        return $w;
    }

    /// Metoda pobiera z bazy identyfikatory wszystkich osób mogących acceptować delegacje
    /**
    @return tablicę identyfikatorów wszystkich osób mogących acceptować delegacje
    */
    /*
    public function getAllAccept()
    {
        $res = $this->db->query("select id,slogin,sforename, ssurname, smail, sphone from temployee where id in (select id_emp from taccept )");
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Employee");
        while ($obj = $res->fetch())
        {
            $obj->setIdAccept($this->getIdAccept($obj->getId()));
            $w[]=$obj;
        }
        return $w;
    }
*/
        /// Metoda pobiera z bazy identyfikatory wszystkich osób mogących acceptować delegacje
    /**
    @return tablicę tablic asocjacyjnych, zawierającą wszystkie osoby mogące acceptować delegacje
    */
    /*
    public function getAllAcceptT()
    {
        $res = $this->db->query("select id,slogin,sforename, ssurname, smail, sphone from temployee where id in (select id_emp from taccept )");
        $w=array();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while ($obj = $res->fetch())
        {
            $obj['id_accept']=array();
            $obj['id_accept']=$this->getIdAccept($obj['id']);
            $w[]=$obj;
        }
        return $w;
    }
    */
    ///metoda zwraca informacje administracyjne o osobach akceptujących delegacje - kiedy i przez kogo zostały dodane, kto i kiedy je ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator osoby akceptującej delegacje
    */
    public function getAcceptInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from taccept where id_emp=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    
    ///metoda zwraca informacje administracyjne o powiązaniach pracowników z osobami akceptującymi delegacje - kiedy i przez kogo zostały dodane, kto i kiedy je ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator pracownika
    @param $id_accept identyfikator osoby akceptującej delegacje
    */
    public function getEmployeeAcceptInfo($id, $id_accept)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from temployee_accept where id=? and id_accept=?");
        $res->execute(array($id,$id_accept));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }

    /** @} */

    //===============================check

    /** \defgroup check Sprawdzanie identyfikatorów*/
    /**  @{ */
    
    ///metoda sprawdza czy jest juz w bazie pracownik o takim identyfikatorze
    /** @param $id identyfikator pracownika
    @return liczbe wpisów z takim id
    */
    public function checkId($id)
    {
        $res = $this->db->prepare("select count(*) from temployee where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie środek transportu o takim identyfikatorze
    /** @param $id identyfikator środka transportu
    @return liczbe wpisów z takim id
    */
    public function checkIdTransp($id)
    {
        $res = $this->db->prepare("select count(*) from ttransp where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie hotel o takim identyfikatorze
    /** @param $id identyfikator hotelu
    @return liczbe wpisów z takim id
    */
    public function checkIdHotel($id)
    {
        $res = $this->db->prepare("select count(*) from thotel where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie miasto o takim identyfikatorze
    /** @param $id identyfikator miasta
    @return liczbe wpisów z takim id
    */
    public function checkIdCity($id)
    {
        $res = $this->db->prepare("select count(*) from tcity where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie państwo o takim identyfikatorze
    /** @param $id identyfikator państwa
    @return liczbe wpisów z takim id
    */
    public function checkIdCountry($id)
    {
        $res = $this->db->prepare("select count(*) from tcountry where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie delegacja o takim identyfikatorze
    /** @param $id identyfikator delegacji
    @return liczbe wpisów z takim id
    */
    public function checkIdTrip($id)
    {
        $res = $this->db->prepare("select count(*) from ttrip where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie pracownik o takim identyfikatorze, który może zaacceptowac delegacje
    /** @param $id identyfikator pracownika acceptujacego delegacje
    @return liczbe wpisów z takim id
    */
    public function checkIdAccept($id)
    {
        $res = $this->db->prepare("select count(*) from taccept where id_emp=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie waluta o takim identyfikatorze
    /** @param $id identyfikator waluty
    @return liczbe wpisów z takim id
    */
    public function checkIdCurr($id)
    {
        $res = $this->db->prepare("select count(*) from tcurrency where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie relacja pracownika z osobą, ktora może mu acceptowac delegacje
    /** @param $id_emp identyfikator pracownika
     @param $id_accept identyfikator pracownika, acceptującego delegacje
    @return liczbe wpisów z takimi id
    */
    public function checkIdEmpAccept($id_emp, $id_accept)
    {
        $res = $this->db->prepare("select count(*) from temployee_accept where id=? and id_accept=?");
        $res->execute(array($id_emp, $id_accept));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }
    
    ///metoda sprawdza czy jest juz w bazie relacja hotelu z delegacja
    /** @param $id_trip identyfikator delegacji
     @param $id_hotel identyfikator hotelu
    @return liczbe wpisów z takimi id
    */
    public function checkIdTripHotel($id_trip, $id_hotel)
    {
        $res = $this->db->prepare("select count(*) from thotel_trip where id_trip=? and id_hotel=?");
        $res->execute(array($id_trip, $id_hotel));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    ///metoda sprawdza czy jest juz w bazie cel podrózy o takim identyfikatrze
    /** @param $id identyfikator celu podrózy
    @return liczbe wpisów z takimi id
    */
    public function checkIdPurp($id)
    {
        $res = $this->db->prepare("select count(*) from ttrip_purp where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $obj = $res->fetch();
        return  $obj['count'];
    }

    /** @} */
    
    ///metoda pobiera z bazy dane celu delegacji, którego identyfikator wchodzi jako parametr
    /** @param $id identyfikator celu podróży
    @return obiekt typu Name, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getPurp($id)
    {
        $res = $this->db->prepare("select id, sname, bactive from ttrip_purp where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Purpose");
        return $res->fetch();
    }

    ///metoda zwraca informacje administracyjne o danym celu delegacji - kiedy i przez kogo został dodany, kto i kiedy go ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator celu delegacji
    */
    public function getPurpInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from ttrip_purp where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }

    
    /** @} */
    //===============================trip

    /** \defgroup trip Odczytywanie danych delegacji*/
    /**  @{ */
    /// metoda pobiera dane delegacji o danym identyfikatorze
    /** @param $id identyfikator podróży
            @return obiekt typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getTrip($id)
    {
        $res = $this->db->prepare("select * from ttrip where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        $w=$res->fetch();
        if ($w!==false) {
            if ($w->getIfAdvance()) {
                $w->setAdvance($this->getAdvance($id));
            }
            if ($w->getIfHotel()) {
                $w->setTripHotel($this->getTripHotel($id));
            }
        }
        return $w;
    }

    /// metoda pobiera iles wierszy zdefiniowanych w parametrach od danego miejsca, sortujac je w zdefiniowany parametrami sposob
    /** @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $filtr po czym sortujemy
    @param $kolejnosc rosnaco czy malejaco (asc,desc)
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTrip($ilosc=10, $poczatek=0, $filtr='dmodif', $kolejnosc='desc')
    {
        $w=array();
        $res = $this->db->prepare("select * from ttrip order by ? ? limit ? offset ?");
        $res->execute(array($filtr, $kolejnosc,$ilosc,$poczatek));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }
        return $w;
    }

//te 2 fun byly wczesniej zakomentowane
    
        // metoda pobiera iles wierszy zdefiniowanych w parametrach od danego miejsca, sortujac je w zdefiniowany parametrami sposob
    /*
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $filtr po czym sortujemy
    @param $kolejnosc rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
/*
    public function getSomeTripByUser( $id_user,$ilosc=10,$poczatek=0, $filtr='dmodif',$kolejnosc='desc')
    {
        $w=array();
        $stmt="select * from ttrip where id_emp='".$id_user."' order by $filtr $kolejnosc, id desc limit $ilosc offset $poczatek";
        echo "<br>$stmt";
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Trip");
        while($o=$res->fetch())
        {
            if($o->getIfAdvance()) $o->setAdvance($this->getAdvance($o->getId()));
            if($o->getIfHotel())	$o->setTripHotel($this->getTripHotel($o->getId()) );
            $w[]=$o;
        }
        return $w;
    }
*/
//zmienna okreslajaca od kt miejsca nalezy pobierac getsometrip by user and t status
// protected $byl_arch=false;
    /// metoda pobiera iles wierszy zdefiniowanych w parametrach od danego miejsca, sortujac je w zdefiniowany parametrami sposob
    /**
    @param $tstatus tablica statusow, czyli np 100000 - bierze tylko te delegacje, w ktorym status =0
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkie delegacje wszystkich userow
    @param $filtr po której kolumnie sortujemy
    @param $kolejnosc rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTripByTStatusClosedAndUser($tstatus, $ilosc=10, $poczatek=0, $id_user=null, $filtr='dmodif', $kolejnosc='desc')
    {
        $w=array();
        $filtr_='';
        //pierwszy war bez and
        $bool=false;
        for ($i=5;$i<7;$i++) {
            if (!$bool && $tstatus[$i]) {
                $filtr_.=' where istatus in ('.$i;
                $bool=true;
            } elseif ($tstatus[$i]) {
                $filtr_.=", $i ";
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        if ($id_user!==null) {
            $filtr_.=" and id_emp='".$id_user."' ";
        }
        $stmt="select * from ttrip $filtr_ order by $filtr $kolejnosc, id desc limit $ilosc offset $poczatek";
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }

        return $w;
    }
    /// metoda pobiera iles wierszy zdefiniowanych w parametrach od danego miejsca, sortujac je w zdefiniowany parametrami sposob, pobierajac dane dla danej osoby akceptującej
    /**
    @param $tstatus tablica statusow, czyli np 100000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_accept id osoby akceptujacej, dla ktorej pobiera sie delegacje
    @param $ilosc  ilosc wierszy do pobrania za jednym razem
    @param $poczatek od ktorego wiersza zaczynamy pobierac
    @param $filtr nazwa kolumny po której sortujemy
    @param $kolejnosc rosnaco czy malejaco
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function getSomeTripByTStatusClosedAndAccept($tstatus, $id_accept, $ilosc=10, $poczatek=0, $filtr='dmodif', $kolejnosc='desc')
    {
        $w=array();
        $filtr_='';
        //pierwszy war bez and
                $bool=false;
        for ($i=5;$i<7;$i++) {
            if (!$bool && $tstatus[$i]) {
                $filtr_.=' where istatus in ('.$i;
                $bool=true;
            } elseif ($tstatus[$i]) {
                $filtr_.=", $i ";
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        $filtr_.=" and id_accept='".$id_accept."' ";
        $stmt="select * from ttrip $filtr_ order by $filtr $kolejnosc, id desc limit $ilosc offset $poczatek";
        $res=$this->db->query($stmt);
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }
        return $w;
    }
/*
    ///metoda pobiera z bazy dane wszystkich delegacji
    /** @return tablicę obiektów typu Trip  z wypełnionymi danymi
    */
    /*
    public function getAllTrips()
    {
        $w=array();
        $res = $this->db->query("select * from ttrip order by dmodif desc,id desc");
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Trip");
        while($o=$res->fetch())
        {
            if($o->getIfAdvance()) $o->setAdvance($this->getAdvance($o->getId()));
            if($o->getIfHotel())	$o->setTripHotel($this->getTripHotel($o->getId()) );
            $w[]=$o;
        }
        return $w;
    }
*/
/*		///metoda pobiera z bazy dane wszystkich delegacji
    /** @return tablicę tablic asocjacyjnych zawierającą wszystkie dane o delegacjach w danej bazie
    */
            /*
    public function getAllTripsT()
    {
        $w=array();
        $res = $this->db->query("select * from ttrip order by dmodif desc,id desc");
        $res->setFetchMode(PDO::FETCH_ASSOC);
        while($o=$res->fetch())
        {
            if($o['badvance']) $o['advance']=$this->getAdvance($o['id']);
            if($o['hotel'])	$o['hotel']=$this->getTripHotel($o['id'] );
            $w[]=$o;
        }
        return $w;
    }
    */
/*	///metoda pobiera z bazy wszystkie delegacje pracownika o danym identyfikatorze
    /** @param $id identyfikator pracownika
    @return tablicę obiektów typu Trip
    */
            /*
    public function getTripByUserId($id)
    {
        $res = $this->db->prepare("select * from ttrip where id_emp=? order by dmodif desc,id desc");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Trip");
        $w=array();
        while($o=$res->fetch())
        {
            if($o->getIfAdvance()) $o->setAdvance($this->getAdvance($o->getId()));
            if($o->getIfHotel())	$o->setTripHotel($this->getTripHotel($o->getId()) );
            $w[]=$o;
        }
        return $w;
    }
*/
/*	///metoda pobiera z bazy wszystkie delegacje pracownika o danym identyfikatorze i danym statusie delegacji
    /** wartosci statusu delegacji:
    0-kopia robocza
    1- oczekuje na zaacceptowanie
    2- zaacceptowana
    3- niezaacceptowana
    4- rozliczona
    5- zamknięta poprawnie
    6- zamknięta przez program

    @param $id identyfikator pracownika
    @param $status delegacji
    @return tablicę obiektów typu Trip
    */
            /*
    public function getTripByUserIdAndStatus($id, $status)
    {
        $res = $this->db->prepare("select * from ttrip where id_emp=? and istatus=? order by dmodif desc,id desc");
        $res->execute(array($id, $status));
        $w=array();
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Trip");
        while($o=$res->fetch())
        {
            if($o->getIfAdvance()) $o->setAdvance($this->getAdvance($o->getId()));
            if($o->getIfHotel())	$o->setTripHotel($this->getTripHotel($o->getId()) );
            $w[]=$o;
        }
        return $w;
    }
    */
    /*///metoda pobiera z bazy wszystkie delegacje danym statusie
    /** wartosci statusu delegacji:
    0-kopia robocza
    1- oczekuje na zaacceptowanie
    2- zaacceptowana
    3- niezaacceptowana
    4- rozliczona
    5- zamknięta poprawnie
    6- zamknięta przez program
    @param $status delegacji
    @return tablicę obiektów typu Trip
    */
            /*
    public function getTripByStatus($status)
    {
        $res = $this->db->prepare("select * from ttrip where istatus=? order by dmodif desc,id desc");
        $res->execute(array($status));
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\Trip");
        $w=array();
        while($o=$res->fetch())
        {
            if($o->getIfAdvance()) $o->setAdvance($this->getAdvance($o->getId()));
            if($o->getIfHotel())	$o->setTripHotel($this->getTripHotel($o->getId()) );
            $w[]=$o;
        }
        return $w;
    }
            */

    ///metoda pobiera z bazy wszystkie podroze, ktore nie sa kopiami roboczymi i ktorych osoba akceptujaca byla osoba podana w parametrze
    /**
    @param $id_accept id osoby akceptujacej delegacje
    @return tablice obiekow typu Trip
    */
    public function getTripsByAccept($id_accept)
    {
        $res = $this->db->prepare("select * from ttrip where id_accept=? and istatus!=0 order by dmodif desc,id desc");
        $res->execute(array($id_accept));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Trip");
        $w=array();
        while ($o=$res->fetch()) {
            if ($o->getIfAdvance()) {
                $o->setAdvance($this->getAdvance($o->getId()));
            }
            if ($o->getIfHotel()) {
                $o->setTripHotel($this->getTripHotel($o->getId()));
            }
            $w[]=$o;
        }
        return $w;
    }

    ///pobiera id podrozy
    /** @param $trip obiekt typu Trip, z ktorego nie sa brane pod uwage zaliczka i hotel, a jedynie czy byla o nie prosba.
    @return tablice obiektow o podany parametrach
    */
    public function getIdOfTrip($trip)
    {
        if ($trip->getIfAdvance()) {
            $ia='true';
        } else {
            $ia='false';
        }
        if ($trip->getIfHotel()) {
            $ih='true';
        } else {
            $ih='false';
        }
        $stmt="select id from ttrip where id_emp= '".$trip->getIdEmployee()."' and id_accept= '".$trip->getIdAccept()."' and id_purp='".$trip->getIdPurp()."'";
        if ($trip->getStartDate()!='') {
            $stmt.=" and dstart_date='".$trip->getStartDate()."'";
        } elseif ($trip->getStatus()!=0) {
            return false;
        } else {
            $stmt.=" and dstart_date is null";
        }
        if ($trip->getEndDate()!='') {
            $stmt.=" and dend_date='".$trip->getEndDate()."'";
        } elseif ($trip->getStatus()!=0) {
            return false;
        } else {
            $stmt.=" and dend_date is null";
        }
        $stmt.=" and badvance='".$ia."' and bhotel='".$ih;
        $stmt.="' and id_transp='".$trip->getIdTransp()."' and id_city= '".$trip->getIdCity()."' and id_start_city= '".$trip->getIdStartCity();
        $stmt.="' and istatus= '".$trip->getStatus()."' and sdescript= '".$trip->getDescript()."' and sdescript2= '".$trip->getDescript2()."' order by id desc";
        $res=$this->db->query($stmt);
        $w=array();
        while ($o=$res->fetch()) {
            $w[]=$o;
        }
        return $w;
    }


    
    /// metoda pobiera iles zdefiniowanych wierszy z delegacji uwzględniając status i pracownika
    /**Bierze pod uwage tylko statusy 0-4
    @param $tstatus tablica statusow, czyli np 100000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function countTripByTStatusTo5AndUser($tstatus, $id_user=null)
    {
        $filtr_='';
        //pierwszy war bez and
        $bool=false;
        for ($i=0;$i<5;$i++) {
            if ($tstatus[$i]==1) {
                if (!$bool) {
                    $filtr_.=' where istatus in ('.$i;
                    $bool=true;
                } else {
                    $filtr_.=", $i ";
                }
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        if ($id_user!==null) {
            $filtr_.=" and id_emp='".$id_user."' ";
        }
        $stmt="select count(*) from ttrip $filtr_ ";
        $res=$this->db->query($stmt);
        $o=$res->fetch();
        if ($o != null) {
            return $o['count'];
        } else {
            return false;
        }
    }

    /// metoda pobiera ileś zdefiniowanych wierszy z delegacji uwzględniając status i pracownika.
    /**Bierze pod uwage tylko status 5 i 6.
    @param $tstatus tablica statusow, bierze pod uwagę tylko 5 i 6 pozycje statusu
    @param $id_user id uzytkownika, ktorego pobiera sie delegacje, lub jesli nie podano usera pobiera wszystkich delegacje wszystkich userow
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function countTripByTStatusClosedAndUser($tstatus, $id_user=null)
    {
        $filtr_='';
        //pierwszy war bez and
        $bool=false;
        for ($i=5;$i<7;$i++) {
            if ($tstatus[$i]==1) {
                if (!$bool) {
                    $filtr_.=' where istatus in ('.$i;
                    $bool=true;
                } else {
                    $filtr_.=", $i ";
                }
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        if ($id_user!==null) {
            $filtr_.=" and id_emp='".$id_user."' ";
        }
        $stmt="select count(*) from ttrip $filtr_ ";
        $res=$this->db->query($stmt);
        $o=$res->fetch();
        if ($o!=false) {
            return $o['count'];
        } else {
            return false;
        }
    }


    /// metoda zwraca ilosc zdefiniowanych wierszy z delegacji uwzględniając status i osobę akceptującą
    /**
    @param $tstatus tablica statusow, czyli np 100000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_accept id osoby akceptującej, dla ktorej pobiera sie delegacje
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function countTripByTStatusClosedAndAccept($tstatus, $id_accept)
    {
        $filtr_='';
        //pierwszy war bez and
        $bool=false;
        for ($i=5;$i<7;$i++) {
            if ($tstatus[$i]==1) {
                if (!$bool) {
                    $filtr_.=' where istatus in ('.$i;
                    $bool=true;
                } else {
                    $filtr_.=", $i ";
                }
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        $filtr_.=" and id_accept='".$id_accept."' ";
        $stmt="select count(*) from ttrip $filtr_ ";
        $res=$this->db->query($stmt);
        $o=$res->fetch();
        if ($o!=false) {
            return $o['count'];
        } else {
            return false;
        }
    }

    /// metoda zwraca ilosc zdefiniowanych wierszy z delegacji uwzględniając status i osobę akceptującą
    /**
    @param $tstatus tablica statusow, czyli np 100000 - bierze tylko te delegacje, w ktorym status =0
    @param $id_accept id osoby akceptującej, dla ktorej pobiera sie delegacje
    @return tablicę obiektów typu Trip z wypelnionymi polami, lub false, jeśli operacja się nie powiedzie
    */
    public function countTripByTStatusTo5AndAccept($tstatus, $id_accept)
    {
        $filtr_='';
        //pierwszy war bez and
        $bool=false;
        for ($i=0;$i<5;$i++) {
            if ($tstatus[$i]==1) {
                if (!$bool) {
                    $filtr_.=' where istatus in ('.$i;
                    $bool=true;
                } else {
                    $filtr_.=", $i ";
                }
            }
        }
        if ($bool) {
            $filtr_.=")";
        } else {
            return false;
        }
        $filtr_.=" and id_accept='".$id_accept."' ";
        $stmt="select count(*) from ttrip $filtr_ ";
        $res=$this->db->query($stmt);
        $o=$res->fetch();
        if ($o!=false) {
            return $o['count'];
        } else {
            return false;
        }
    }

    
    ///metoda pobiera z bazy wszystkie dane o pobycie w hotelu na danej delegacji
    /** @param $id identyfikator delegacji
    @return obiekt typu TripHotel, lub w przypadku niepowodzenia false
    */
    public function getTripHotel($id)
    {
        $res = $this->db->prepare("select * from thotel_trip where id_trip=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\TripHotel");
        return $res->fetch();
    }


    ///metoda pobiera z bazy wszystkie dane hoteli wszystkich delegacji
    /**
    @return tablicę obiektów typu TripHotel
    */
    /*
    public function getAllTripHotel()
    {
        $res = $this->db->query("select * from thotel_trip");
        $res->setFetchMode(PDO::FETCH_CLASS,"Taavit\TravelRequest\Model\TripHotel");
        $w=array();
        while($o=$res->fetch())
        {
            $w[]=$o;
        }
        return $w;
    }
    */
    /** @} */

    //===========================hotel
    /** \defgroup Hotel Odczytywanie danych hoteli*/
    /**  @{ */
    ///metoda pobiera z bazy dane hotelu na podstawie jego identyfikatora
    /** @param $id identyfikator hotelu
            @return obiekt typu Trip, lub w przypadku złego id zwaracane jest false
    */
    public function getHotel($id)
    {
        $res = $this->db->prepare("select id, id_city,sname, bactive from thotel where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Hotel");
        return $res->fetch();
    }
    
    ///metoda zwraca informacje administracyjne o danym hotelu - kiedy i przez kogo został dodany, kto i kiedy go ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator hotelu
    */
    public function getHotelInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from thotel where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }

        ///metoda zmienia date, oraz osobe, ktora ostatnio dokonala modyfikacji Hotelu
    /** @param $id id hotelu
    @param $id_admin id osoby dokonujacej zmiany
    @return ilosc zmodyfikowanych wierszy
    */
    public function changedHotel($id, $id_admin)
    {
        $stmt="update thotel set dmodif_date=current_date, id_modif_by='".$id_admin."' where id=".$id;
        $ret=$this->db->exec($stmt);
        return $ret;
    }
    /** @} */
    
    ///metoda pobiera z bazy nazwę waluty, której identyfikator wchodzi jako parametr
    /** @param $id identyfikator waluty
    @return obiekt typu Name, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getCurr($id)
    {
        $res = $this->db->prepare("select id, sname, bactive from tcurrency where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Currency");
        return $res->fetch();
    }

        ///metoda zwraca informacje administracyjne o danej walucie - kiedy i przez kogo została dodana, kto i kiedy ją ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator waluty
    */
    public function getCurrInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from tcurrency where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    
    /** @} */

        ///metoda pobiera z bazy dane środka transportu, którego identyfikator wchodzi jako parametr
    /** @param $id identyfikator środka transportu
    @return obiekt typu Name, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getTransp($id)
    {
        $res = $this->db->prepare("select id, sname, bactive from ttransp where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Transport");
        return $res->fetch();
    }
        
    ///metoda zwraca informacje administracyjne o danym środku transportu - kiedy i przez kogo został dodany, kto i kiedy go ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator środka transportu
    */
    public function getTranspInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from ttransp where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    
    /** @} */

    //=====================ADVANCE -zaliczka
    /** \defgroup Adv Odczytywanie zaliczek*/
            /**  @{ */
    ///metoda pobiera z bazy dane zaliczki konkretnej delegacji, której identyfikator wchodzi jako parametr
    /** @param $tripid identyfikator delegacji
            @return obiekt typu Advance, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getAdvance($tripid)
    {
        $res = $this->db->prepare("select * from tadvance where id_trip=?");
        $res->execute(array( $tripid));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Advance");
        return $res->fetch();
    }
    /** @} */
    //============================city

    ///metoda pobiera z bazy dane miasta, którego identyfikator wchodzi jako parametr
    /** @param $id identyfikator miasta
    @return obiekt typu City, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getCity($id)
    {
        $res = $this->db->prepare("select id,id_country, sname, bactive from tcity where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\City");
        return $res->fetch();
    }

    ///metoda zwraca informacje administracyjne o danym mieście - kiedy i przez kogo zostało dodane, kto i kiedy je ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator miasta
    */
    public function getCityInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from tcity where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    /** @} */

    ///metoda pobiera z bazy dane państwa, którego identyfikator wchodzi jako parametr
    /** @param $id identyfikator państwa
    @return obiekt typu Name, lub w przypadku niepowodzenia zwaracane jest false
    */
    public function getCountry($id)
    {
        $res = $this->db->prepare("select id, sname, bactive from tcountry where id=?");
        $res->execute(array( $id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Country");
        return $res->fetch();
    }

    ///metoda zwraca informacje administracyjne o danym państwie - kiedy i przez kogo zostało dodane, kto i kiedy je ostatnio modyfikował
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator państwa
    */
    public function getCountryInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from tcountry where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    /** @} */

    
    //===========================admin
    /** \defgroup admin Odczytywanie informacji o administratorach*/
            /**  @{ */


    ///metoda zwraca informacje o administratorach - kiedy i przez kogo został dany administrator dodany.
    /** @return obiekt typu Informer lub w przypadku niepowodzenia false
    @param $id identyfikator państwa
    */
    public function getAdminInfo($id)
    {
        $res = $this->db->prepare("select id_created_by,dcreation_date,dmodif_date, id_modif_by from tcountry where id=?");
        $res->execute(array($id));
        $res->setFetchMode(PDO::FETCH_CLASS, "Taavit\TravelRequest\Model\Informer");
        return $res->fetch();
    }
    /** @} */
    
//==================================================================


    
    /** \defgroup Arch Archiwizowanie danych */
    /**  @{ */

    ///metoda archiwizująca dane pracownika. Operacja jest oparta na transakcji.
    /** Metoda wywyołuje metodę archEmployee_. Funkcja nie archiwizuje identyfikatorów osób, które mogą acceptować delegacje danemu pracownikowi.
    Aby poprawnie zarchiwizować wszystkie dane pracowników powinno się wywołać dla wszystkich pracownikow:archEmployee, archAccept oraz
    archEmpAccept dokładnie w takiej kolejności.
    @param $emp obiekt typu Employee, zawierający nowe dane pracownika
    @param $id_admin identyfikator osoby modyfikującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @param $pass hasło pracownika, jeśli nie zosatanie podane, to pozostanie stare hasło.
    @return false, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function archEmployee($emp, $id_admin, $creation_date=null, $id_created_by=null, $pass='')
    {
        $this->db->beginTransaction();
        $ret=$this->archEmployee_($emp, $id_admin, $creation_date, $id_created_by, $pass);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }


    ///metoda archiwizująca dane pracownika. Operacja nie jest oparta na transakcji.
    /**Nie archiwizuje identyfikatorów osób, które mogą acceptować delegacje danemu pracownikowi
    Aby poprawnie zarchiwizować wszystkie dane pracowników powinno się wywołać dla wszystkich pracownikow: archEmployee, archAccept oraz
    archEmpAccept dokładnie w takiej kolejności.
    @param $emp obiekt typu Employee, zawierający nowe dane pracownika
    @param $id_admin identyfikator osoby modyfikującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @param $pass hasło pracownika, jeśli nie zosatanie podane, to pozostanie stare hasło.
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function archEmployee_($emp, $id_admin, $creation_date=null, $id_created_by=null, $pass='')
    {
        $ret=1;

        if ($this->checkId($emp->getId())!=0) {
            $stmt="update temployee set slogin='".$emp->getLogin()."', ";
            if ($pass!='') {
                $stmt.="spass='".$pass."', ";
            }
            $stmt.="sforename='".$emp->getForename()."', ssurname='".$emp->getSurname();
            $stmt.="',smail='".$emp->getMail()."', sphone='".$emp->getPhone()."',dmodif_date=current_date, id_modif_by='".$id_admin;
            $stmt.="'   where id='".$emp->getId()."'";
            $ret=$this->db->exec($stmt);
        } else {
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into temployee(id,slogin, spass,sforename, ssurname,smail, sphone, id_created_by, dcreation_date, dmodif_date, id_modif_by )values('";
                $stmt.=$emp->getId()."','".$emp->getLogin()."','".$pass."','".$emp->getForename()."', '";
                $stmt.=$emp->getSurname()."','".$emp->getMail()."','".$emp->getPhone()."','".$creator."', '".$creation_date."', current_date, '".$id_admin ."')";
            } else {
                $stmt="insert into temployee(id,slogin, spass,sforename, ssurname,smail, sphone, id_created_by, dcreation_date, dmodif_date, id_modif_by )values('";
                $stmt.=$emp->getId()."','".$emp->getLogin()."','".$pass."','".$emp->getForename()."', '";
                $stmt.=$emp->getSurname()."','".$emp->getMail()."','".$emp->getPhone()."','".$creator."', current_date, current_date, '".$id_admin ."')";
            }
            $ret=$this->db->exec($stmt);
        }
        return $ret;
    }

    ///metoda archiwizująca środek transportu. Metoda oparta jest na transakcji.
    /**	Metoda wywyołuje metodę archTransp_.
    @param $name obiekt typu Name, zawierający nowe dane środka transportu. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
            @param $id_admin identyfikator osoby archiwizującej
            @param $creation_date data stworzenia obiektu
            @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function archTransp($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $this->db->beginTransaction();
        $ret=$this->archTransp_($name, $id_admin, $creation_date, $id_created_by);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda archiwizująca środek transportu. Metoda nie jest oparta na transakcji.
    /**	@param $name obiekt typu Name, zawierający nowe dane środka transportu. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function archTransp_($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        if ($this->checkIdTransp($name->getId())!=0) {
            $stmt="update ttransp set sname ='".$name->getName()."',dmodif_date=current_date, id_modif_by='".$id_admin."' where id='".$name->getId()."'";
            $ret=$this->db->exec($stmt);
        } else {
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into ttransp values('".$name->getId()."','".$name->getName()."','1','".$creator."','".$creation_date."', current_date,'".$id_admin."')";
            } else {
                $stmt="insert into ttransp values('".$name->getId()."','".$name->getName()."','1','".$creator."',current_date, current_date,'".$id_admin."')";
            }
            $ret=$this->db->exec($stmt);
        }
        return $ret;
    }

    ///metoda archiwizująca dane hotelu. Metoda oparta jest na transakcji.
    /**Metoda wywyołuje metodę archHotel_.
    @param $hotel obiekt typu Hotel, zawierający nowe dane hotelu. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function archHotel($hotel, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $this->db->beginTransaction();
        $ret=$this->archHotel_($hotel, $id_admin, $creation_date, $id_created_by);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda archiwizująca dane hotelu. Metoda nie jest oparta na transakcji.
    /**	@param $hotel obiekt typu Hotel, zawierający nowe dane hotelu. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function archHotel_($hotel, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $ret=1;
        if ($this->checkIdHotel($hotel->getId())!=0) {
            $stmt="update thotel set sname='".$hotel->getName()."', id_city='".$hotel->getIdCity()."',dmodif_date=current_date, id_modif_by='".$id_admin."' where id='".$hotel->getId()."'";
            $ret=$this->db->exec($stmt);
        } else {
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into thotel values('".$hotel->getId()."','".$hotel->getIdCity()."','".$hotel->getName()."', '1','".$creator."',' ".$creation_date."' , current_date,'".$id_admin."')";
            } else {
                $stmt="insert into thotel values('".$hotel->getId()."','".$hotel->getIdCity()."','".$hotel->getName()."', '1','".$creator."',current_date , current_date,'".$id_admin."')";
            }
            $ret=$this->db->exec($stmt);
        }
        return $ret;
    }

    ///metoda archiwizująca dane państw. Metoda oparta jest na transakcji.
    /**Metoda wywyołuje metodę archCountry_.
    @param $name obiekt typu Name, zawierający nowe dane kraju.  W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function archCountry($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $this->db->beginTransaction();
        $ret=$this->archCountry_($name, $id_admin, $creation_date, $id_created_by);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda archiwizująca dane państwa. Metoda nie jest oparta na transakcji.
    /**	@param $name obiekt typu Name, zawierający nowe dane państwa. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function archCountry_($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $ret=1;
        if ($this->checkIdCountry($name->getId())!=0) {
            $stmt="update tcountry set sname ='".$name->getName()."',dmodif_date=current_date, id_modif_by='".$id_admin."' where id='".$name->getId()."'";
            $ret=$this->db->exec($stmt);
        } else {
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into tcountry values('".$name->getId()."','".$name->getName()."','1','".$creator."','".$creation_date."' , current_date,'".$id_admin."')";
            } else {
                $stmt="insert into tcountry values('".$name->getId()."','".$name->getName()."','1','".$creator."',current_date , current_date,'".$id_admin."')";
            }
            $ret=$this->db->exec($stmt);
// 			echo "<br>".$stmt;
        }
        return $ret;
    }
        ///metoda archiwizująca waluty. Metoda oparta jest na transakcji.
    /**Metoda wywyołuje metodę archCurr_.
    @param $name obiekt typu Name, zawierający nowe dane waluty. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    public function archCurr($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $this->db->beginTransaction();
        $ret=$this->archCurr_($name, $id_admin, $creation_date, $id_created_by);
        if ($ret) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        return $ret;
    }

    ///metoda archiwizująca waluty. Metoda nie jest oparta na transakcji.
    /**	@param $name obiekt typu Name, zawierający nowe dane waluty. W archiwizacji nie jest brana pod uwagę zmienna bactive w obiektcie - zawsze ustawiana jest na 1
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy
    */
    protected function archCurr_($name, $id_admin, $creation_date=null, $id_created_by=null)
    {
        $ret=1;
        if ($this->checkIdCurr($name->getId())!=0) {
            $stmt="update tcurrency set sname='".$name->getName()."',dmodif_date=current_date, id_modif_by='".$id_admin."' where id='".$name->getId()."'";
            $ret=$this->db->exec($stmt);
        } else {
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into tcurrency values('".$name->getId()."','".$name->getName()."','1','".$creator."','".$creation_date."' , current_date,'".$id_admin."')";
            } else {
                $stmt="insert into tcurrency values('".$name->getId()."','".$name->getName()."','1','".$creator."',current_date , current_date,'".$id_admin."')";
            }
            $ret=$this->db->exec($stmt);
        }
        return $ret;
    }
    
        
    ///metoda archiwizująca identyfikatory administratorów
    /**
    @param $id identyfikator administratora
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
        @return  logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy, lub jesli jest juz taki wpis to zwracana jest wartość 1
    */
        public function archAdmin($id, $id_admin, $creation_date=null, $id_created_by=null)
        {
            if ($this->checkIdAdmin($id)!=0) {
                $stmt="update tadmin set dmodif_date=current_date,  id_modif_by='".$id_admin."' where id_emp='".$id."' ";
                return $this->db->exec($stmt);
            }
            if ($id_created_by!==null) {
                $creator=$id_created_by;
            } else {
                $creator=$id_admin;
            }
            if ($creation_date!==null) {
                $stmt="insert into tadmin values('".$id."','".$creator."','".$creation_date."' , current_date,'".$id_admin."')";
            } else {
                $stmt="insert into tadmin values('".$id."','".$creator."',current_date , current_date,'".$id_admin."')";
            }
            $ret=$this->db->exec($stmt);
            return $ret;
        }

    ///metoda archiwizująca identyfikatory osób uprawnionych do acceptacji delegacji.
    /** Aby poprawnie zarchiwizować wszystkie dane pracowników powinno się wywołać dla wszystkich pracownikow:archEmployee, archAccept oraz
    archEmpAccept dokładnie w takiej kolejności.
    @param $id zawiera identyfikator osoby uprawnionej do acceptacji delegacji
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return  logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy lub jesli jest juz taki wpis to zwracana jest wartość 1
    */
    public function archAccept($id, $id_admin, $creation_date=null, $id_created_by=null)
    {
        if ($this->checkIdAccept($id)!=0) {
            $stmt="update taccept set dmodif_date=current_date,  id_modif_by='".$id_admin."' where id_emp='".$id."' ";
            return $this->db->exec($stmt);
        }
        if ($id_created_by!==null) {
            $creator=$id_created_by;
        } else {
            $creator=$id_admin;
        }
        if ($creation_date!==null) {
            $stmt="insert into taccept values('".$id."','1','".$creator."','".$creation_date."' , current_date,'".$id_admin."')";
        } else {
            $stmt="insert into taccept values('".$id."','1','".$creator."',current_date , current_date,'".$id_admin."')";
        }
        return $this->db->exec($stmt);
    }
    
    ///metoda archiwizująca relację pracownik, osoba, które może zaacceptować danemu pracownikowi delegacje
    /**Aby poprawnie zarchiwizować wszystkie dane pracowników powinno się wywołać dla wszystkich pracownikow:archEmployee, archAccept oraz
    archEmpAccept dokładnie w takiej kolejności.
    @param $id_emp identyfikator pracownika
    @param $id_accept identyfikator osoby uprawnionej do acceptacji delegacji
    @param $id_admin identyfikator osoby archiwizującej
    @param $creation_date data stworzenia obiektu
    @param $id_created_by identyfikator osoby tworzacej obiekt, pod warunkiem, że jest ona inną osobą niż admin, którego id zostało podane
    @return  logiczne zero, jeśli operacja się nie powiedzie, w przeciwnym razie ilość zmodyfikowanych wierszy lub jesli jest juz taki wpis to zwracana jest wartość 1
    */
    public function archEmpAccept($id_emp, $id_accept, $id_admin, $creation_date=null, $id_created_by=null)
    {
        if ($this->checkIdEmpAccept($id_emp, $id_accept)!=0) {
            $stmt="update temployee_accept set dmodif_date=current_date,  id_modif_by='".$id_admin."' where id='".$id_emp."' and id_accept='".$id_accept."' ";
            return $this->db->exec($stmt);
        }
        if ($id_created_by!==null) {
            $creator=$id_created_by;
        } else {
            $creator=$id_admin;
        }
        if ($creation_date!==null) {
            $stmt="insert into temployee_accept values('".$id_emp."','".$id_accept."','".$creator."','".$creation_date."' , current_date,'".$id_admin."')";
        } else {
            $stmt="insert into temployee_accept values('".$id_emp."','".$id_accept."','".$creator."',current_date , current_date,'".$id_admin."')";
        }
        return $this->db->exec($stmt);
    }

    /// Metoda archiwizuje hasło pracownika
    /**	@return logiczne zero, gdy operacja się nie powiodła. Jeśli operacja się powiedzie zwrócona zostanie ilośc zmodyfikowanych wierszy
    @param $id identyfikator pracownika
    @param $pass zaszyfrowane hasło pracownika
    @param $id_admin identyfikator administratora
    */
    public function setPassw($id, $pass, $id_admin)
    {
        return $this->conn->executeUpdate(
            "update temployee set spass=:pass, dmodif_date=current_date, id_modif_by=:id_admin where id=:id",
            [
                'pass' => $pass,
                'id_admin' => $id_admin,
                'id' => $id
            ]
        );
    }
    /** @} */
}
