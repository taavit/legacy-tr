-- CREATE DATABASE main owner pracownik;
set datestyle to iso;
--===============================================
create sequence Semployee
		increment by 1
		minvalue 0
		start with 0;
		
create table temployee
(
	id integer primary key default nextval('Semployee'::text),
	slogin text not null unique,
	spass text not null,
	sforename text not null,	--imie
	ssurname text not null,	--nazwisko
	smail text,
	sphone text,
	id_created_by integer references temployee on delete cascade,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade
);

create table tactive
(
	id_emp integer references temployee on delete cascade not null unique,	--id prac do skasowania
	id_admin_deleg integer references temployee on delete cascade,	--id admina delegacji, ktory chce skasowac prac, lub nawiazal kontakt z kasujacym
	bdeleg boolean default null,	--stan kasowania za strony delegacji 0-chce, 1 moge juz skasowac
	sreason_deleg text default null,	--przyczyna, dlaczego admin delegacji chce go skasowac
	id_admin_time integer references temployee on delete cascade,	----id admina timesheeta, ktory chce skasowac prac, lub nawiazal kontakt z kasujacym
	btime boolean default null,	--stan kasowania za strony timesheeta 0-chce, 1 moge juz skasowac
	sreason_time text default null	--przyczyna, dlaczego admin timesheeta chce go skasowac
);
--przyczyne wypelnia tylko ten, ktory zaczal wpisywanie, zeby bylo wiadomo, kto zaczal
--===============================================



----1--------------------------------------------

create table tadmin
(
  id_emp integer references temployee on delete cascade not null unique,
 	id_created_by integer references temployee on delete cascade,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade
);


-----2-------------------------------------------

create sequence Stransp
increment by 1
minvalue 0
start with 0;


create table ttransp
(
 id integer primary key default nextval('Stransp'::text),
 sname  text not null unique,
 bactive boolean default true,
 	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);

------3------------------------------------------
create sequence Scountry
		increment by 1
		minvalue 0
		start with 0;


create table tcountry
(
  id integer primary key default nextval('Scountry'::text),
  sname  text unique,
  bactive boolean default true,
	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);

-----4-------------------------------------------

create sequence Scity
increment by 1
minvalue 0
start with 0;


create table tcity
(
  id integer primary key default nextval('Scity'::text),
  id_country integer references tcountry on delete cascade not null,
  sname  text,
  bactive boolean  default true,
 	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);

------5------------------------------------------

create table taccept
(
  id_emp integer references temployee on delete cascade not null unique,
  bactive boolean default true,
	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade
);


-----6-------------------------------------------

create table temployee_accept
(
	id integer references temployee on delete cascade not null,
	id_accept integer references taccept(id_emp) on delete cascade not null,
	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade
);

------7------------------------------------------

create sequence Strip_purp
		increment by 1
		minvalue 0
		start with 0;


create table ttrip_purp
(
	id integer primary key default nextval('Strip_purp'::text),
	sname text not null,
	bactive boolean  default true,
 	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);

create sequence Strip
		increment by 1
		minvalue 0
		start with 0;


create table ttrip
(
	id integer primary key default nextval('Strip'::text),
	id_emp integer references temployee on delete cascade,
	id_accept integer references taccept (id_emp) on delete cascade,
	id_purp integer references ttrip_purp on delete cascade,
	dstart_date date,
	dend_date date ,
	badvance boolean not null,
	bhotel boolean not null,
	id_transp integer references ttransp,
	id_start_city integer references tcity,
	id_city integer references tcity,
	istatus integer not null,
	sdescript text,
	sdescript2 text,
	dmodif date not null
	--komentarze
);

-------8-----------------------------------------

create sequence Shotel
		increment by 1
		minvalue 0
		start with 0;


create table thotel
(
  id integer primary key default nextval('Shotel'::text),
  id_city integer references tcity not null,
  sname text not null,
	bactive boolean  default true,
 	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);


--------9----------------------------------------

create table thotel_trip
(
  id_trip integer references ttrip  on delete cascade not null unique,
  id_hotel integer references thotel on delete cascade,
  dstart_date date ,
  dend_date date ,
  idays integer
);

---------10--------------------------------------

create sequence Scurrency
		increment by 1
		minvalue 0
		start with 0;

create table tcurrency
(
  id integer primary key default nextval('Scurrency'::text),
  sname text not null unique,
	bactive boolean  default true,
	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null
);


---------11--------------------------------------
create table tadvance
(
  id_trip integer references ttrip on delete cascade not null unique,
  svalue text ,
  id_currency integer references tcurrency on delete cascade ,
  bmoney boolean --1=gotowka, 0=przelew,
);




---------12--------------------------------------


create sequence Smape
		increment by 1
		minvalue 0
		start with 0;

 create table tmape
 (
	id integer primary key default nextval('Smape'::text),
	sshort_name text not null unique,
	sfull_name text,
	id_created_by integer references temployee on delete cascade not null,
	dcreation_date date not null,
	dmodif_date date not null,
	id_modif_by integer references temployee on delete cascade not null,
	id_owner integer references temployee on delete cascade not null
 );
--==============================================================================
--==============================================================================
--==============================================================================


insert into temployee(slogin, spass,sforename, ssurname,smail, sphone,dcreation_date,dmodif_date) values('ala', 'ala', 'Ala', 'Alanowska','ala@cos.pl','97-45-76',current_date, current_date);

insert into tadmin ( id_emp,  dcreation_date, dmodif_date)values (0,current_date, current_date);

insert into taccept(id_emp,  dcreation_date,dmodif_date,id_modif_by,  id_created_by)values (0,current_date,current_date,0, 0);

insert into temployee(slogin, spass,sforename, ssurname,smail, sphone,dcreation_date,dmodif_date,id_created_by,id_modif_by) values('beata', 'beata', 'Beata', 'Beatkowska', 'beata@cos.pl','99-45-76',current_date, current_date,0,0);

insert into taccept(id_emp,  dcreation_date,dmodif_date,id_modif_by,  id_created_by)values (1,current_date,current_date,0, 0);

insert into temployee(slogin, spass,sforename, ssurname,smail, sphone,dcreation_date,dmodif_date,id_created_by,id_modif_by) values('krysia', 'krysia', 'Krysia', 'Kryśkowska', 'krys@cos.pl','00-45-76',current_date, current_date,0,0);


insert into temployee_accept (id,id_accept,  dcreation_date,dmodif_date,id_modif_by, id_created_by) values (1,0,current_date,current_date,0, 0);
insert into temployee_accept (id,id_accept,  dcreation_date,dmodif_date,id_modif_by, id_created_by) values (2,1,current_date,current_date, 0,0);


insert into ttransp(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('samolot',0,current_date,current_date,0);
insert into ttransp(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('pociag',0,current_date,current_date,0);
insert into ttransp(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('samochód',0,current_date,current_date,0);


insert into tcountry(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Polska',0,current_date,current_date,0);
insert into tcountry(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Niemcy',0,current_date,current_date,0);
insert into tcountry(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Szwecja',0,current_date,current_date,0);


insert into tcity(sname,id_country,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Warszawa',0,0,current_date,current_date,0);
insert into tcity(sname,id_country,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Berlin',1,0,current_date,current_date,0);


insert into thotel(sname,id_city,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Hotel luksusowy w Warszawie',0,0,current_date,current_date,0);

insert into thotel(sname,id_city,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Hotel luksusowy w Berlinie',1,0,current_date,current_date,0);

insert into ttrip_purp (sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Trening',0,current_date,current_date,0);
insert into ttrip_purp (sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('Project',0,current_date,current_date,0);

insert into ttrip (id_emp, id_accept, id_purp, dstart_date,dend_date, badvance, bhotel, id_transp, id_city,id_start_city, istatus, sdescript, sdescript2,dmodif)
	values (1,0, 0, to_date('01.01.07','DD.MM.YY'), to_date('01 01 2007','DD MM YYYY'), false, false, 0,0,1,0, 'j','uwagi',to_date('01 01 2007','DD MM YYYY'));

insert into ttrip (id_emp, id_accept, id_purp, dstart_date,dend_date, badvance, bhotel, id_transp, id_city,id_start_city, istatus, sdescript, sdescript2,dmodif)
	values (2,1, 1, to_date('01.01.07','DD.MM.YY'), to_date('01 01 2007','DD MM YYYY'), true, true, 0,0,1,0, 'j','uwagi',to_date('01 01 2007','DD MM YYYY'));


insert into tcurrency(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('PLN',0,current_date,current_date,0);
insert into tcurrency(sname,id_created_by, dcreation_date,dmodif_date,id_modif_by) values ('EU',0,current_date,current_date,0);


insert into tadvance values(1,'100,00', 0, true);

insert into thotel_trip values (1,0, to_date('01.01.07','DD.MM.YY'), to_date('01 01 2007','DD MM YYYY'),5);




insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('travreq','Travel Request', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('MainMenu','Menu', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('administration','Administration panel', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('pageconfig','System configuration', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('adm_menu','Menu', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('permissions','Permissions managment', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('userman','User managment', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('delegation','Delegation history', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('ListUser','User list', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('TripForm','Delegation form', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('tripdet','Delegation detail', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('modifyuserform','Modify/Add user form', 0,current_date,current_date,0,0);
insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('delegationman','Delegation manager', 0,current_date,current_date,0,0);

insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('adduserform','Add user', 0,current_date,current_date,0,0);










insert into tmape (sshort_name, sfull_name,id_created_by,dcreation_date,dmodif_date,id_modif_by, id_owner) values ('user_info','User info', 0,current_date,current_date,0,0);

alter table tactive drop COLUMN sreason_deleg;
alter table tactive drop COLUMN sreason_time;
