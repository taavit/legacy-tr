<?php
declare(strict_types=1);

namespace Taavit\TravelRequest\Model;
/**
 * Obiekt zawiera dane hotelu. Pozwala na ich edycję w niertwałej pamięci
 * @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
 * @date 11-11-2007
 */

class Hotel
{
    ///identyfikator nazwy
    protected $id;

    ///nazwa
    protected $sname;

    ///Czy obiekt oczekuje na skasowanie
    protected $bactive;

    ///identyfikator miasta w ktorym jest hotel
    protected $id_city;

    /** @return identyfikator*/
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->sname;
    }

    /**
     * @return boolean zmienną mówiącą, czy obiekt oczekuje na skasowanie 0- nie, 1-tak
     */
    public function getActive()
    {
        return $this->bactive;
    }

    /** metoda przypisuje identyfikator
    @param $a identyfikator
    @return void
    */
    public function setId($a)
    {
        $this->id=$a;
    }

    /** metoda przypisuje nazwę
    @param $a nazwa
    @return void
    */
    public function setName($a)
    {
        $this->sname=$a;
    }

    /** metoda przypisuje zmienną mówiącą, czy dany obiekt oczekuje na skasowanie
    @param $a 1-czeka na skasowanie, 0 nie czeka
    @return void
    */
    public function setActive($a)
    {
        $this->bactive=$a;
    }

    /** @return identyfikator miasta, w którym jest hotel*/
    public function getIdCity()
    {
        return $this->id_city;
    }


    /** metoda przypisuje hotelowi identyfikator miasta, w którym się znajduje
    @param $a identyfikator miasta, w którym jest hotel
    @return void
    */
    public function setIdCity($a)
    {
        $this->id_city=$a;
    }
}
