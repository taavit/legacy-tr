<?php
declare(strict_types=1);

/// Plik zawierający strukturę danych do przetrzymywania informacji o pracowniku

namespace Taavit\TravelRequest\Model;

///klasa zawierająca dane pracownika
/**
obiekt zawiera dane pracownika. Pozwala na ich edycję w niertwałej pamięci
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/
class Employee
{
    ///id pracownika w bazie
    protected $id;

    ///tablica id pracowników w bazie akceptujacych danemu pracownikowi delegacje
    protected $id_accept;

    ///login pracownika
    protected $slogin;

    ///imie pracownika
    protected $sforename;

    ///nazwisko pracownika
    protected $ssurname;

    ///adres e-mail pracownika
    protected $smail;

    ///telefon kontaktowy pracownika
    protected $sphone;

    
    /**	@return identyfikator pracownika*/
    public function getId()
    {
        return $this->id;
    }

    /**	@return tablica identyfikatorow osob mogacych zaakceptowac delegację danemu pracownikowi*/
    public function getIdAccept()
    {
        return $this->id_accept;
    }

    /** @return login pracownika*/
    public function getLogin()
    {
        return $this->slogin;
    }

    /** @return imię pracownika*/
    public function getForename()
    {
        return $this->sforename;
    }

    /** @return nazwisko pracownika*/
    public function getSurname()
    {
        return $this->ssurname;
    }

    /** @return telefon pracownika*/
    public function getPhone()
    {
        return $this->sphone;
    }

    /** @return adres e-mail pracownika*/
    public function getMail()
    {
        return $this->smail;
    }

    
    /** metoda przypisuje pracownikowi identyfikator
    @param $a identyfikator pracownika
    @return void
    */
    public function setId($a)
    {
        $this->id=$a;
    }

    /** metoda przypisuje pracownikowi tablicę identyfikatorów osób akceptujących delegacje
    @param $a tablica identyfikatorów
    @return void
    */
    public function setIdAccept($a)
    {
        $this->id_accept=$a;
    }

    /** metoda przypisuje pracownikowi login
    @param $a login pracownika
    @return void
    */
    public function setLogin($a)
    {
        $this->slogin=$a;
    }
    
    /** metoda przypisuje pracownikowi imię
    @param $a imię pracownika
    @return void
    */
    public function setForename($a)
    {
        $this->sforename=$a;
    }

    /** metoda przypisuje pracownikowi nazwisko
    @param $a nazwisko pracownika
    @return void
    */
    public function setSurname($a)
    {
        $this->ssurname=$a;
    }

    /** metoda przypisuje pracownikowi nr telefonu
    @param $a nr telefonu pracownika
    @return void
    */
    public function setPhone($a)
    {
        $this->sphone=$a;
    }

    /** metoda przypisuje pracownikowi adres e-mail
    @param $a adres e-mail pracownika
    @return void
    */
    public function setMail($a)
    {
        $this->smail=$a;
    }
}
