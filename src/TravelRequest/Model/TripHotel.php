<?php
declare(strict_types=1);

/*
 * This file is part of Travel Request
 *
 * (c) Dawid Królak <taavit@gmail.com> & Anna Iga Okrasa <isia8578@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Taavit\TravelRequest\Model;

/**
 * Plik zawierający strukturę danych do przetrzymywania informacji o zakwaterowaniu w hotelu podczas delegacji
 * Klasa zawierająca pola opisujące zakwaterowanie w danym hotelu
 * Obiekt pozwala na ich edycję w niertwałej pamięci.
 *
 * @author Anna Iga Okrasa <isia8578@gmail.com>
 * @author Dawid Królak <taavit@gmail.com>
 * @date 17-11-2007
 * @date 06-02-2017
 */
class TripHotel
{
    /**
     * Identyfikator podróży
     */
    protected $id_trip;

    /**
     * Identyfikator hotelu
     */
    protected $id_hotel;

    /**
     * Data zameldowania
     */
    protected $dstart_date;

    /**
     * Data wymeldowania
     */
    protected $dend_date;

    /**
     * Ilość dób spędzonych w hotelu
     */
    protected $idays;

    /**
     * Get trip id
     *
     * @return integer identyfikator podróży
     */
    public function getIdTrip()
    {
        return $this->id_trip;
    }

    /**
     * Get hotel id
     *
     * @return integer identyfikator hotelu
     */
    public function getIdHotel()
    {
        return $this->id_hotel;
    }

    /**
     * Get start date
     *
     * @return \DateTime start date
     */
    public function getStartDate()
    {
        return $this->dstart_date;
    }

    /**
     * Get end date
     *
     * @return \DateTime end date
     */
    public function getEndDate()
    {
        return $this->dend_date;
    }

    /**
     * Get amount of days spent in hotel
     *
     * @return integer ilość dób spędzonych w hotelu
     */
    public function getDays()
    {
        return $this->idays;
    }

    /**
     * Metoda przypisuje identyfikator podróży
     *
     * @param integer $a identyfikator podróży
     *
     * @return void
     */
    public function setIdTrip($a)
    {
        $this->id_trip=$a;
    }

    /**
     * Metoda przypisuje identyfikator hotelu
     *
     * @param integer $a identyfikator hotelu
     *
     * @return void
     */
    public function setIdHotel($a)
    {
        $this->id_hotel=$a;
    }

    /**
     * Arriving in hotel
     *
     * @param \DateTime $startDate arriving hotel
     *
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->dstart_date = $startDate;
    }

    /**
     * Leaving hotel date
     *
     * @param \DateTime $endDate
     *
     * @return void
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->dend_date = $endDate;
    }

    /**
     * Set number of days spend in hotel
     *
     * @param integer $days
     *
     * @return void
     */
    public function setDays(int $days)
    {
        $this->idays = $days;
    }
}
