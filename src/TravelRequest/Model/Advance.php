<?php
declare(strict_types=1);

namespace Taavit\TravelRequest\Model;

class Advance
{
    ///identyfikator delegacji, której tyczy się zaliczka
    protected $id_trip;

    ///wartosc zaliczki
    protected $svalue;

    ///identyfikator waluty zaliczki
    protected $id_currency;

    ///zmienna mówiąca, czy zaliczka ma być wypłacona gotówką, czy przelewem <br>false- przelew<br>true- gotówka
    protected $bmoney;

    /**	@return identyfikator podrózy*/
    public function getIdTrip()
    {
        return $this->id_trip;
    }

    /**
     * @return integer amount
     */
    public function getValue()
    {
        return $this->svalue;
    }

    /**	@return identyfikator waluty*/
    public function getIdCurr()
    {
        return $this->id_currency;
    }

    /**
     * @return zmienna mówiącą, czy zaliczka ma zostać wypłacona gotówką
     */
    public function getIfMoney()
    {
        return $this->bmoney;
    }


    /** metoda przypisuje identyfikator podróży
     * @param $a identyfikator delegacji
     * @return void
     */
    public function setIdTrip($a)
    {
        $this->id_trip=$a;
    }

    /** metoda przypisuje wartość zaliczki
    @param $a wartość zaliczki
    @return void
    */
    public function setValue($a)
    {
        $this->svalue=$a;
    }

    /** metoda przypisuje identyfikator waluty, w której ma być wypłacona zaliczka
    @param $a identyfikator pracownika
    @return void
    */
    public function setIdCurr($a)
    {
        $this->id_currency=$a;
    }

    /**
     * metoda przypisuje zmienną mówiącą, czy zaliczka ma zostać wypłacona gotówką
     *
     * @param $a false- przelew true- gotowka
     * @return void
     */
    public function setIfMoney($a)
    {
        $this->bmoney = $a;
    }
}
