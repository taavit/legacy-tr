<?php
declare(strict_types=1);

///Klasa przechywywująca dane o tym, kiedy dany obiekt zosatał stworzony, przez kogo, kto i kidey go ostatnio modyfikował

namespace Taavit\TravelRequest\Model;

/**
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 20-11-2007
*/
class Informer extends MinInformer
{

    ///data ostatniej modyfikacji obiektu
    protected $dmodif_date;

    ///identyfikator osoby, która jako ostatnia modyfikowała obiekt
    protected $id_modif_by;


    /** @return datę ostatniej modyfikacji obiektu*/
    public function getModifDate()
    {
        return $this->dmodif_date;
    }

    /** @return identyfikator osoby po raz ostatni modyfikującej obiekt*/
    public function getModifBy()
    {
        return $this->id_modif_by;
    }


    /** metoda przypisuje datę ostatniej modyfikacji obiektu
    @param $a login data ostatniej modyfikacji obiektu w formacie YYYY-MM-DD
    @return void
    */
    public function setModifDate($a)
    {
        $this->dmodif_date=$a;
    }
    
    /** metoda przypisuje identyfikator osoby po raz ostatni modyfikującej obiekt
    @param $a identyfikator osoby po raz ostatni modyfikującej obiekt
    @return void
    */
    public function setModifBy($a)
    {
        $this->id_modif_by=$a;
    }
}