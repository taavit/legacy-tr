<?php
declare(strict_types=1);

/// Plik zawierający strukturę danych do przetrzymywania informacji o delegacji

namespace Taavit\TravelRequest\Model;

///Klasa służąca do przechowywania danych pojedynczej delegacji
class Trip
{
    const STATE_DRAFT = 0;
    const STATE_WAITING = 1;
    const STATE_APPROVED = 2;
    const STATE_UNAPPROVED = 3;
    const STATE_SETTLED = 4;
    const STATE_CLOSED = 5;
    const STATE_CLOSED_BY_PROGRAM = 6;

    ///identyfikator delegacji
    protected $id;

    ///idnetyfikator pracownika, który pojechał na delegację
    protected $id_emp;

    ///identyfikator pracownika, który zaakceptował delegację
    protected $id_accept;

    ///identyfikator celu delegacji
    protected $id_purp;

    ///data początku delegacji
    protected $dstart_date;

    ///data końca delegacji
    protected $dend_date;

    ///zmienna mówiąca, czy pracownik, chciał zaliczkę; 0-nie; 1-tak
    protected $badvance;

    ///zmienna mówiąca, czy pracownik zamówił pobyt w hotelu
    protected $bhotel;

    ///identyfikator transportu, jaki wybrał pracownik
    protected $id_transp;

    ///identyfikator miasta, do którego jedzie pracownik
    protected $id_city;

    ///miasto skąd zaczyna się delegację
    protected $id_start_city;

    ///status delegacji
    /**	0-kopia robocza Edytowalna
    1- oczekuje na zaakceptowanie
    2- zaakceptowana
    3- niezaakceptowana Edytowalna
    4- rozliczona
    5- zamknięta poprawnie
    6- zamknięta przez program*/
    protected $istatus;

    ///komentarz pracownika
    protected $sdescript;

    ///komentarz osoby akceptującej
    protected $sdescript2;

    ///data ostatniej modyfikacji
    protected $dmodif;

    
    ///null, jeśli pracownik nie chciał zaliczki, w przeciwnym wypadku obiekt typu Advance
    protected $advance = null;
    
    ///null, jeśli pracownik nie zamiawiał pobytu w hotelu, w przecwnym wypadku obiekt typu TripHotel
    protected $triphotel = null;

    /** @return identyfikator podróży*/
    public function getId()
    {
        return $this->id;
    }

    /** @return identyfikator pracownika*/
    public function getIdEmployee()
    {
        return $this->id_emp;
    }

    /** @return identyfikator osoby akceptującej*/
    public function getIdAccept()
    {
        return $this->id_accept;
    }

    /** @return cel delegacji*/
    public function getIdPurp()
    {
        return $this->id_purp;
    }

    /** @return datę rozpoczęcia delegacji*/
    public function getStartDate()
    {
        return $this->dstart_date;
    }

    /** @return datę zakończenia delegacji*/
    public function getEndDate()
    {
        return $this->dend_date;
    }

    /** @return datę ostatniej modyfikacji*/
    public function getModifDate()
    {
        return $this->dmodif;
    }

    /** @return zmienną mówiącą, czy pracownik życzył sobie zaliczkę 0-nie, 1- tak*/
    public function getIfAdvance()
    {
        return $this->badvance;
    }

    /** @return zmienną mówiącą, czy pracownik zamiawiał pobyt w hotelu 0-nie, 1- tak*/
    public function getIfHotel()
    {
        return $this->bhotel;
    }

    /** @return identyfikator środka transportu*/
    public function getIdTransp()
    {
        return $this->id_transp;
    }

    /** @return identyfikator miasta do którego jest delegacja*/
    public function getIdCity()
    {
        return $this->id_city;
    }

    /** @return identyfikator miasta, z którego zaczęto delegację*/
    public function getIdStartCity()
    {
        return $this->id_start_city;
    }
    
    /** @return status delegacji*/
    public function getStatus()
    {
        return $this->istatus;
    }

    /** @return komentarz pracownika*/
    public function getDescript()
    {
        return $this->sdescript;
    }

    /** @return komentarz osoby akceptującej*/
    public function getDescript2()
    {
        return $this->sdescript2;
    }
    
    /** @return obiekt typu Advance, z danymi zaliczki, pod warunkiem, że pracownik dostał zaliczkę, w przeciwnym wypadku false*/
    public function getAdvance()
    {
        return $this->advance;
    }

    /** @return obiekt typu TripHotel, z danymi dot. pobytu w hotelu, pod warunkiem, że pracownik zamawiał pobyt w hotelu, w przeciwnym wypadku false*/
    public function getTripHotel()
    {
        return $this->triphotel;
    }

    /** metoda przypisuje identyfikator podrózy
    @param $a identyfikator podrózy
    @return void
    */
    public function setId($a)
    {
        $this->id=$a;
        if ($this->badvance) {
            $this->advance->setIdTrip($a);
        }
        if ($this->bhotel) {
            $this->triphotel->setIdTrip($a);
        }
    }

    /** metoda przypisuje identyfikator pracownika
    @param $a identyfikator pracownika
    @return void
    */
    public function setIdEmployee($a)
    {
        $this->id_emp=$a;
    }

    /** metoda przypisuje identyfikator osoby akceptującej
    @param $a identyfikator osoby akceptującej
    @return void
    */
    public function setIdAccept($a)
    {
        $this->id_accept=$a;
    }

    /** metoda przypisuje cel delegacji
    @param $a cel delegacji
    @return void
    */
    public function setIdPurp($a)
    {
        $this->id_purp=$a;
    }

    /** metoda przypisuje datę rozpoczęcia delegacji
    @param $a data rozpoczęcia delegacji
    @return void
    */
    public function setStartDate($a)
    {
        $this->dstart_date=$a;
    }

    /** metoda przypisuje datę zakończenia delegacji
    @param $a data zakończenia delegacji
    @return void
    */
    public function setEndDate($a)
    {
        $this->dend_date=$a;
    }

    /** metoda przypisuje datę ostatniej modyfikacji
    @param $a data ostatniej delegacji
    @return void
    */
    public function setModifDate($a)
    {
        $this->dmodif=$a;
    }

    /** metoda przypisuje zmienną mówiącą o tym, czy pracownik otrzyma zaliczkę
    @param $a zmienna mówiąca o tym, czy pracownik otrzyma zaliczkę; 0o-nie; 1-tak
    @return void
    */
    public function setIfAdvance($a)
    {
        $this->badvance=$a;
    }

    /** metoda przypisuje zmienną mówiącą, czy pracownik zamawiał pobyt w hotelu
    @param $a zmienna mówiąca, czy pracownik zamawiał pobyt w hotelu
    @return void
    */
    public function setifHotel($a)
    {
        $this->bhotel=$a;
    }

    /** metoda przypisuje identyfikator środka transportu
    @param $a identyfikator środka transportu
    @return void
    */
    public function setIdTransp($a)
    {
        $this->id_transp=$a;
    }

    /** metoda przypisuje identyfikator miasta
    @param $a identyfikator miasta
    @return void
    */
    public function setIdCity($a)
    {
        $this->id_city=$a;
    }

    /** metoda przypisuje identyfikator miasta, z którego ma się rozpocząć delegacja
    @param $a identyfikator miasta
    @return void
    */
    public function setIdStartCity($a)
    {
        $this->id_start_city=$a;
    }
    
    /** metoda przypisuje status podróży
    @param $a status podróży
    @return void
    */
    public function setStatus($a)
    {
        $this->istatus=$a;
    }

    /** metoda przypisuje komentarz pracownika
    @param $a komentarz pracownika
    @return void
    */
    public function setDescript($a)
    {
        $this->sdescript=$a;
    }

    /** metoda przypisuje komentarz osoby akceptującej delegację
    @param $a komentarz osoby akceptującej delegację
    @return void
    */
    public function setDescript2($a)
    {
        $this->sdescript2=$a;
    }


    /** metoda przypisuje dane zaliczki.
    @param $a obiekt typu Advance, w którym są dane zaliczki
    @return void
    */
    public function setAdvance($a)
    {
        $this->advance=$a;
    }

    /** metoda przypisuje dane dot. pobytu w hotelu
    @param $a obiekt typu TripHotel z nowymi danymi, dot. pobytu w hotelu
    @return void
    */
    public function setTripHotel($a)
    {
        $this->triphotel=$a;
    }
}
