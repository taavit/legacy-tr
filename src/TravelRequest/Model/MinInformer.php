<?php
declare(strict_types=1);

/// Plik zawierający struktury danych do przetrzymywania informacji o dacie stworzenia, modyfikacji oraz pracowniku który stworzył wpis i ostatnio modyfikował

namespace Taavit\TravelRequest\Model;

///Klasa przechowuje dane o tym, kiedy i przez kogo obiekt został stworzony
/**
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 20-11-2007
*/
class MinInformer
{
    ///identyfikator osoby, która stworzyła dany obiekt
    protected $id_created_by;

    ///data stworzenia obiektu
    protected $dcreation_date;
        
    /** @return identyfikator osoby, która stworzyła dany obiekt*/
    public function getCreatedBy()
    {
        return $this->id_created_by;
    }

    /** @return datę stworzenia obiektu*/
    public function getCreationDate()
    {
        return $this->dcreation_date;
    }

        
    /** metoda przypisuje identyfikator osoby, która stworzyła dany obiekt
    @param $a identyfikator osoby, która stworzyła dany obiekt
    @return void
    */
    public function setCreatedBy($a)
    {
        $this->id_created_by=$a;
    }

    /** metoda przypisuje datę stworzenia obiektu
    @param $a data stworzenia obiektu w formacie YYYY-MM-DD
    @return void
    */
    public function setCreationDate($a)
    {
        $this->dcreation_date=$a;
    }
}
