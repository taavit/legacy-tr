<?php
declare(strict_types=1);

namespace Taavit\TravelRequest\Model;

///Klasa zawierająca pola na dane miasta.
/** Obiekt pozwala na ich edycję w niertwałej pamięci. Dziedziczy po klasie Name.
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007 */
class City
{
    ///identyfikator nazwy
    protected $id;

    ///nazwa
    protected $sname;

        ///Czy obiekt oczekuje na skasowanie
    protected $bactive;

    /** @return identyfikator*/
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->sname;
    }

    /**
     * @return boolean zmienną mówiącą, czy obiekt oczekuje na skasowanie 0- nie, 1-tak
     */ 
    public function getActive()
    {
        return $this->bactive;
    }

    /** metoda przypisuje identyfikator
    @param $a identyfikator
    @return void
    */
    public function setId($a)
    {
        $this->id=$a;
    }

    /** metoda przypisuje nazwę
    @param $a nazwa
    @return void
    */
    public function setName($a)
    {
        $this->sname=$a;
    }

    /** metoda przypisuje zmienną mówiącą, czy dany obiekt oczekuje na skasowanie
    @param $a 1-czeka na skasowanie, 0 nie czeka
    @return void
    */
    public function setActive($a)
    {
        $this->bactive=$a;
    }

    ///identyfikator państwa
    protected $id_country;

    /** @return identyfikator kraju, w którym leży miasto*/
    public function getIdCountry()
    {
        return $this->id_country;
    }

    /** metoda przypisuje identyfikator państwa, w którym leży miasto
    @param $a identyfikator państwa
    @return void
        */
    public function setIdCountry($a)
    {
        $this->id_country=$a;
    }
}
