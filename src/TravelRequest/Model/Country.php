<?php
declare(strict_types=1);

// Plik zawierający strukturę danych do przetrzymywania informacji składających się z nazwy, identyfikatora i statusu aktywności

namespace Taavit\TravelRequest\Model;

///Klasa zawierająca identyfikator oraz nazwę.
/**Wykorzystywana jest np. w trzymaniu indeksów walut oraz ich nazw, lub indeksów państw oraz ich nazw.
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 09-11-2007
*/
class Country
{

    ///identyfikator nazwy
    protected $id;

    ///nazwa
    protected $sname;

        ///Czy obiekt oczekuje na skasowanie
    protected $bactive;

    /** @return identyfikator*/
    public function getId()
    {
        return $this->id;
    }

    /** @return nazwę*/
    public function getName()
    {
        return $this->sname;
    }

    /** @return zmienną mówiącą, czy obiekt oczekuje na skasowanie 0- nie, 1-tak*/
    public function getActive()
    {
        return $this->bactive;
    }

    /** metoda przypisuje identyfikator
    @param $a identyfikator
    @return void
    */
    public function setId($a)
    {
        $this->id=$a;
    }

    /** metoda przypisuje nazwę
    @param $a nazwa
    @return void
    */
    public function setName($a)
    {
        $this->sname=$a;
    }

    /** metoda przypisuje zmienną mówiącą, czy dany obiekt oczekuje na skasowanie
    @param $a 1-czeka na skasowanie, 0 nie czeka
    @return void
    */
    public function setActive($a)
    {
        $this->bactive=$a;
    }
}
