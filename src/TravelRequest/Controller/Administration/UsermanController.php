<?php
namespace Taavit\TravelRequest\Controller\Administration;

/// plik zawiera klasę 'userman'
/** @file userman.php */

use Taavit\TravelRequest\Model\Employee;

///Klasa generująca stronę zarządzania użytkownikami, oraz wykonująca operacje na bazie pracowników
/**
Obiekt zawiera stronę do zarządzania użytkownikami. Pozwala na kasowanie, dodawanie oraz modyfikację użytkowników.
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/

class UsermanController extends \TPage
{
    private $twig;

    ///Konstruktor rejestrujący funkcje xajaxowe
    public function __construct($twig)
    {
        global $xajax;
        parent::__construct();
        $xajax->registerFunction('X_ShowUsrGrid');
        $xajax->registerFunction('X_DelUser');
        isset($_GET['page']) ? $this->page=1 : $this->page=$_GET['page'];
        $this->twig = $twig;
    }

    ///Funkcja modyfikująca użytkownika na podstawie danych z tabeli $_POST, funkcja zwraca wynik do zmiennej $_SESSION['msg']
    public function modifyUser()
    {
        $emp=$this->db->getEmployee($_GET['id']);
        $emp->setLogin(strip_tags($_POST['login']));
        $emp->setForename(strip_tags($_POST['name']));
        $emp->setSurname(strip_tags($_POST['surname']));
        $emp->setPhone(strip_tags($_POST['telephone']));
        $emp->setMail(strip_tags($_POST['mail']));
        if (!isset($_POST['acc'])) {
            $_POST['acc']='';
        }
        foreach ($this->db->getAllAccept(true) as $empl) {
            if (isset($_POST['id'.$empl->getId()])) {
                if ($this->db->isEmpAccept($emp->getId(), $empl->getId(), $db)) {
                    if ($db==2) {
                        $this->db->restoreEmpAccept($emp->getId(), $empl->getId(), $db, $this->usr->getId_logged());
                    }
                } else {
                    $this->db->addEmpAccept($emp->getId(), $empl->getId(), $this->usr->getId_logged());
                }
            }
            if (!isset($_POST['id'.$empl->getId()])) {
                $this->db->delEmpAccept($emp->getId(), $empl->getId(), $this->usr->getId_logged());
            }
        }
        $this->db->setEmployee($emp, $_SESSION['user']->getId_logged(), '');

        $_SESSION['err']=TR_TEXT_USER_CHANGED_SUCCSESFUL;

        redirect("index2.php?appname=travreq&page=administration&a=userman");
    }

    ///Funkcja służąca do zmiany hasła, hasło zmieniane jest na podstawie tablicy $_POST
    public function ChangePass()
    {
        if (isset($_POST['pass'])&&$_POST['pass']!=''&& ($_POST['pass']==$_POST['pass2']) && isset($_POST['id'])) {
            $w= $this->db->setPassw($_POST['id'], md5($_POST['pass']), $this->usr->getId_logged());
            if ($w==0) {
                $_SESSION['err']=TR_TEXT_CHANGE_PASS_FAILED;
            } else {
                $_SESSION['err']=TR_TEXT_CHANGE_PASS_SUCCESS;
            }
        } else {
            $_SESSION['err']=TR_TEXT_CHANGE_PASS_NOT_THE_SAME;
        }
        redirect("index2.php?appname=travreq&page=administration&a=userman");
    }
    
    ///Funkcja kasująca użytkownika
    /**
    @param $id id użytkownika do skasowania
    @return lista użytkowników po skasowaniu
    */
    public function delUser($id)
    {
        $w=$this->db->delEmployee($id, $this->usr->getId_logged());
        return $this->ListAllUsers();
    }

    ///Funkcja do dodawania użytkownika na podstawie danych z tablicy $_POST
    public function addUser()
    {
        $failed=false;
        $emp=new Employee;

        if (isset($_POST['login']) && $_POST['login']!='') {
            if ($this->db->checkLogin($_POST['login'])) {
                $failed=true;
                $_SESSION['err']=TR_TEXT_USER_LOGIN_EXISTS;
            }
        }

        if (!$failed) {
            if (
        isset($_POST['name'])&&$_POST['name']!=''&&
        isset($_POST['surname'])&&$_POST['surname']!=''&&
        isset($_POST['telephone'])&&$_POST['telephone']!=''&&
        isset($_POST['login'])&&$_POST['login']!=''&&
        isset($_POST['mail'])&&$_POST['mail']!='' &&
        isset($_POST['pass'])&&$_POST['pass']!=''&&
        ($_POST['pass']==$_POST['pass2'])) {
                $emp->setForename($_POST['name']);
                $emp->setSurname($_POST['surname']);
                $emp->setPhone($_POST['telephone']);
                $emp->setMail($_POST['mail']);
                $emp->setLogin($_POST['login']);
            
                if ($this->db->isEmployeeIN($emp, $zm)) {
                    $this->db->restoreEmployee($emp->getId(), $zm, $_SESSION['user']->getId_logged());
                } else {
                    $this->db->addEmployee($emp, $this->usr->getId_logged(), md5($_POST['pass']));
                }
            
                foreach ($this->db->getAllAccept(true) as $empl) {
                    if (isset($_POST['id'.$empl->getId()])) {
                        $this->db->addEmpAccept($this->db->getIdByLogin($emp->getLogin()), $empl->getId(), $this->usr->getId_logged());
                    } else {
                        $this->db->delEmpAccept($emp->getId(), $empl->getId(), $this->usr->getId_logged());
                    }
                }
            } else {
                $_SESSION['msg']=TR_TEXT_USER_ALL_FIELDS;
                $failed=true;
            }
        }

        if (\Assertion\Assert::email($_POST['mail'])) {
            $_SESSION['msg']=TR_TEXT_USER_WRONG_EMAIL;
            $failed=true;
        }

        if ($failed) {
            $emp->setForename($_POST['name']);
            $emp->setSurname($_POST['surname']);
            $emp->setPhone($_POST['telephone']);
            $emp->setMail($_POST['mail']);
            $emp->setLogin($_POST['login']);
            $w=array();
            foreach ($this->db->getAllAccept(true) as $empl) {
                if (isset($_POST['id'.$empl->getId()])) {
                    $w[]=$empl->getId();
                }
            }
            $emp->setIdAccept($w);
            $_SESSION['temp_usr']=$emp;
            redirect("index2.php?page=administration&a=userman&b=adduserform");
        } else {
            redirect("index2.php?appname=travreq&page=administration&a=userman");
        }
    }
    
    ///Funkcja generująca formularz modyfikacji użytkownika na podstawie zmiennej $_GET['id']
    /**
    @return wygenerowany formularz
    */
    public function ShowModifyForm()
    {
        $tpl=new \TTemplate;
        if (isset($_GET['id'])) {
            $_SESSION['lastActEmp']=$_GET['id'];
            $emp=$this->db->getEmployee($_GET['id']);
        } else {
            $emp=$this->db->getEmployee($_SESSION['lastActEmp']);
        }

        $tpl->setTplFile(__DIR__.'/../../../app/Resources/views/forms/modform.tpl');
        $tpl->addRepVar('{name}', $emp->getForename());
        $tpl->addRepVar('{surname}', $emp->getSurname());
        $tpl->addRepVar('{login}', $emp->getLogin());
        $tpl->addRepVar('{telephone}', $emp->getPhone());
        $tpl->addRepVar('{email}', $emp->getMail());
        $tpl->addRepVar('{op}', 'modify');
        $tpl->addRepVar('{oph}', 'modifypass');
        $tpl->addRepVar('{id}', $emp->getId());
        $editbut=new \TButton;
        $editbut->setBType('submit');
        $editbut->setWidth(60);
        $editbut->setCaption(TR_TEXT_BUTTON_SAVE);
        $tpl->addRepVar('{modifybut}', $editbut->Show());
        $tpl->addRepVar('{addbut}', '');
        $chpassbut=new \TButton;
        $chpassbut->setBType('submit');
        $chpassbut->setCaption(TR_TEXT_USER_CHPASS);
        $tpl->addRepVar('{chpass}', $chpassbut->Show());
        $tpl->addRepVar('TR_TEXT_USER_FORENAME', TR_TEXT_USER_FORENAME);
        $tpl->addRepVar('TR_TEXT_USER_SURNAME', TR_TEXT_USER_SURNAME);
        $tpl->addRepVar('TR_TEXT_USER_MAIL', TR_TEXT_USER_MAIL);
        $tpl->addRepVar('TR_TEXT_USER_LOGIN', TR_TEXT_USER_LOGIN);
        $tpl->addRepVar('TR_TEXT_USER_PHONE', TR_TEXT_USER_PHONE);
        $tpl->addRepVar('TR_TEXT_USER_PASS2', TR_TEXT_USER_PASS2);
        $tpl->addRepVar('TR_TEXT_USER_PASS', TR_TEXT_USER_PASS);
        $tpl->addRepVar('TR_TEXT_ADD_USER', TR_TEXT_ADD_USER);
        $tpl->addRepVar('TR_TEXT_BUTTON_BACK', TR_TEXT_BUTTON_BACK);
        $tpl->addRepVar('TR_TEXT_ACC_LIST', TR_TEXT_ACC_LIST);
        if ($this->db->isAccept($emp->getId(), $ct)) {
            if ($ct==1) {
                $tpl->addRepVar('{accept}', 'checked');
            } else {
                $tpl->addRepVar('{accept}', '');
            }
        }
        $flista='';
        foreach ($this->db->getAllAccept(true) as $empl) {
            if ($emp->getId()==$empl->getId()) {
                continue;
            }
            $tplrow=new \TTemplate;
            $tplrow->setTplFile(__DIR__.'/../../../app/Resources/views/forms/listaccrow.tpl');
            $tplrow->addRepVar('{name}', $empl->getForename());
            $tplrow->addRepVar('{surname}', $empl->getSurname());
            $tplrow->addRepVar('{id}', $empl->getId());
            if ($this->db->checkIdEmpAccept($emp->getId(), $empl->getId())) {
                $tplrow->addRepVar('{checked}', 'checked');
            } else {
                $tplrow->addRepVar('{checked}', '');
            }
                
            $tplrow->prepare();
            $flista.=$tplrow->getOutputText();
        }
        $tpl->addRepVar('{listaccept}', $flista);
        $tpl->prepare();
        return $tpl->getOutputText();
    }

    ///Funkcja generująca formularz dodawania użytkownika
    /**
    @return wygenerowany formularz
    */
    public function ShowAddForm()
    {
        $tpl=new \TTemplate;
        $tpl->setTplFile(__DIR__.'/../../../app/Resources/views/forms/addform.tpl');
        if (isset($_SESSION['temp_usr'])) {
            $msg=$_SESSION['msg'];
            $tpl->addRepVar("{msg}", $msg);
            unset($_SESSION['msg']);
            $semp=$_SESSION['temp_usr'];
            unset($_SESSION['temp_usr']);
            $tpl->addRepVar('{name}', $semp->getForename());
            $tpl->addRepVar('{surname}', $semp->getSurname());
            $tpl->addRepVar('{login}', $semp->getLogin());
            $tpl->addRepVar('{telephone}', $semp->getPhone());
            $tpl->addRepVar('{email}', $semp->getMail());
        } else {
            $tpl->addRepVar("{msg}", '');
            $tpl->addRepVar('{name}', '');
            $tpl->addRepVar('{surname}', '');
            $tpl->addRepVar('{login}', '');
            $tpl->addRepVar('{telephone}', '');
            $tpl->addRepVar('{email}', '');
        }
        
        $tpl->addRepVar('{op}', 'adduser');
        $tpl->addRepVar('TR_TEXT_USER_FORENAME', TR_TEXT_USER_FORENAME);
        $tpl->addRepVar('TR_TEXT_USER_SURNAME', TR_TEXT_USER_SURNAME);
        $tpl->addRepVar('TR_TEXT_USER_MAIL', TR_TEXT_USER_MAIL);
        $tpl->addRepVar('TR_TEXT_USER_LOGIN', TR_TEXT_USER_LOGIN);
        $tpl->addRepVar('TR_TEXT_USER_PHONE', TR_TEXT_USER_PHONE);
        $tpl->addRepVar('TR_TEXT_USER_PASS2', TR_TEXT_USER_PASS2);
        $tpl->addRepVar('TR_TEXT_USER_PASS', TR_TEXT_USER_PASS);
        $tpl->addRepVar('TR_TEXT_ADD_USER', TR_TEXT_ADD_USER);
        $tpl->addRepVar('TR_TEXT_BUTTON_BACK', TR_TEXT_BUTTON_BACK);
        $tpl->addRepVar('TR_TEXT_ACC_LIST', TR_TEXT_ACC_LIST);

        $chpassbut=new \TButton;
        $chpassbut->setBType('submit');
        $chpassbut->setCaption(TR_TEXT_BUTTON_ADD);
        $chpassbut->setWidth('60');
        $tpl->addRepVar('{addbut}', $chpassbut->Show());
        $flista='';
        $allaccept=$this->db->getAllAccept(true);
        foreach ($allaccept as $emp) {
            $tplrow=new \TTemplate;
            $tplrow->setTplFile(__DIR__.'/../../../app/Resources/views/forms/listaccrow.tpl');
            $tplrow->addRepVar('{name}', $emp->getForename());
            $tplrow->addRepVar('{surname}', $emp->getSurname());
            $tplrow->addRepVar('{id}', $emp->getId());
            if (isset($semp)) {
                if (in_array($emp->getId(), $semp->getIdAccept())) {
                    $tplrow->addRepVar('{checked}', 'checked="checked"');
                } else {
                    $tplrow->addRepVar('{checked}', '');
                }
            }
            $tplrow->prepare();
            $flista.=$tplrow->getOutputText();
        }
        $tpl->addRepVar('{listaccept}', $flista);
        $tpl->prepare();
        return $tpl->getOutputText();
    }


    ///Funkcja sterująca, oraz wyświetlająca właściwy formularz
    /**
    @return wygenerowana strona
    */
    public function show()
    {
        if (!isset($_GET['b'])) {
            $_GET['b']='ListUser';
        }
        switch ($_GET['b']) {
            case 'modifyuserform':
                return $this->ShowModifyForm();
            case 'adduserform':
                return $this->ShowAddForm();
            case 'deluser':
                return $this->DelUser($_GET['id']);
            case 'adduser':
                return $this->AddUser();
            case 'modify':
                return $this->ModifyUser();
            case 'chpass':
                return $this->ChangePass();
            default:
                $data = [];
                if (isset($_SESSION['err'])) {
                    $data['error'] = "<div id='msg'>".$_SESSION['err']."</div>";
                    unset($_SESSION['err']);
                } else {
                    $data['error'] = '';
                }
                $data['grid'] = ShowUsrGrid(0);
                $data['pagesbottom'] = generatePages();
                $data['pagestop'] = generatePages();
                $addbut=new \TButton;
                $addbut->setAction('window.location="?page=administration&a=userman&b=adduserform"');
                $addbut->setCaption(TR_TEXT_BUTTON_NEW_USER);
                $addbut->setWidth(50);
                $data['addbut'] = $addbut->Show();
                return $this->twig->render('administration/userman.html.twig', $data);
        }
    }
}
///Funkcja generująca listę strony
/**
@param $act aktualna strona
@return wygenerowany kod listy strona
*/

function generatePages($act = 0)
{
    $db=new \MainPDO;
    $db->connect();
    $ap=0;

    $act=ceil($act / CS_ROWS_PER_PAGE);
    $cpages=ceil($db->countActiveEmployee() / CS_ROWS_PER_PAGE);
    $links='<div class="pageslist" onclick="xajax_X_ShowUsrGrid(0);">'. TR_TEXT_PAGE_FIRST .' </div>';
    $prev=$act-1;
    if ($act!=0) {
        $links.='<div class="pageslist" onclick="xajax_X_ShowUsrGrid('.$prev*CS_ROWS_PER_PAGE.');">'. TR_TEXT_PAGE_PREVIOUS .' </div>';
    } else {
        $links.='<div class="pageslist" style="color:gray">'. TR_TEXT_PAGE_PREVIOUS .' </div>';
    }

    if ($act<5) {
        $beg=0;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $beg=$act-4;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $links.=' ...';
    }
    for ($i=$beg; $i<$end; $i++) {
        $ap=$i + 1;
        $active='';
        if ($i==$act) {
            $active='style="font-weight:bolder;text-decoration:underline;"';
        } else {
            $active='';
        }
        $links.='<div class="pageslist" onclick="xajax_X_ShowUsrGrid('. $i*CS_ROWS_PER_PAGE .');"'.$active.' >'. $ap .' </div>';
    }
    $next=$act+1;
    if ($act<$cpages-5) {
        $links.='... ';
    }
    if ($act!=$cpages-1) {
        $links.='<div class="pageslist" onclick="xajax_X_ShowUsrGrid('.$next*CS_ROWS_PER_PAGE.');">'. TR_TEXT_PAGE_NEXT .' </div>';
    } else {
        $links.='<div class="pageslist" style="color:gray">'. TR_TEXT_PAGE_NEXT .' </div>';
    }
    $links.='<div class="pageslist" onclick="xajax_X_ShowUsrGrid('.($cpages-1)*CS_ROWS_PER_PAGE.');">'. TR_TEXT_PAGE_LAST .' </div>';
    return $links;
}

///Funkcja generująca tabelkę użytkowników
/**
@param $st numer od którego mają być wczytane dane
@return tablica użytkowników
*/

function ShowUsrGrid($st = 0)
{
    $db=new \MainPDO;
    $db->connect();
    $usrtable=new \TDataGrid("UserList");
    $emplist=$db->getSomeActiveEmployee(CS_ROWS_PER_PAGE, $st);
    $i=0;
    
    $headers[]=TR_TEXT_LOGIN_USERNAME;
    $headers[]=TR_TEXT_USER_FORENAME;
    $headers[]=TR_TEXT_USER_SURNAME;
    $headers[]=TR_TEXT_USER_MAIL;
    $headers[]=TR_TEXT_USER_PHONE;
    $headers[]=TR_TEXT_CREATED_BY;
    $headers[]=TR_TEXT_CREATION_DATE;
    $headers[]=TR_TEXT_MODIFIED_BY;
    $headers[]=TR_TEXT_MODIFICATION_DATE;
    
    $width[]=50;
    $width[]=80;
    $width[]=80;
    $width[]=80;
    $width[]=80;
    $width[]=100;
    $width[]=85;
    $width[]=100;
    $width[]=75;
    $buttons;
    foreach ($emplist as $emp) {
        $data[$i][]=$emp->getLogin(); //username
        $data[$i][]=$emp->getForename();//forename
        $data[$i][]=$emp->getSurname();//surname
        $data[$i][]=$emp->getMail();//mail
        $data[$i][]=$emp->getPhone();//phone
        
        $data[$i][]=$db->getName($db->getEmployeeInfo($emp->getId())->getCreatedBy()); //createdby
        $data[$i][]=$db->getEmployeeInfo($emp->getId())->getCreationDate(); //
        $data[$i][]=$db->getName($db->getEmployeeInfo($emp->getId())->getModifBy());
        $data[$i][]=$db->getEmployeeInfo($emp->getId())->getModifDate();
        
        $zmienna=$db->getEmployeeInfo($emp->getId());
        $delbutton=new \TButton;
        $modbutton=new \TButton;
        $modbutton->setCaption(TR_TEXT_BUTTON_MOD);
        $delbutton->setCaption(TR_TEXT_BUTTON_DEL);
        $modbutton->setAction('window.location="?page=administration&a=userman&b=modifyuserform&id='.$emp->getId().'"');
        $delbutton->setAction('xajax_X_DelUser('.$emp->getId().');');
        $delbutton->setWidth(50);
        $modbutton->setWidth(50);
        $buttons[$i][]=$modbutton;
        $buttons[$i][]=$delbutton;
        $ids[]=$emp->getId();
        $i++;
    }
    $usrtable->setHeaders($headers);
    $usrtable->setData($data);
    $usrtable->setIndexes($ids);
    $usrtable->setButtons($buttons);
    $usrtable->setColumnsWidth($width);
    return $usrtable->Show();
}
///Xajaxowa funkcja generująca tabelkę użytkowników
/**
@param $st numer pierwszego wiersza od którego mają zostać wyświetlone dane
@return kod xml dla xajaxa zawierająca tabelkę
*/
function X_ShowUsrGrid($st)
{
    $obj=new XajaxResponse;
    $obj->addAssign('grid', 'innerHTML', ShowUsrGrid($st));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($st));
    return $obj->getXML();
}

///Xajaxowa funkcja kasująca użytkownika
/**
@param $id id użytkownika do skasowania
@return kod xml dla xajaxa zawierająca tabelkę użytkowników po skasowaniu
*/
function X_DelUser($id)
{
    $db=new \MainPDO;
    $db->connect();
    $db->delEmployee($id, $_SESSION['user']->getId_logged());
    $obj=new XajaxResponse;
    $obj->addAssign('grid', 'innerHTML', ShowUsrGrid(0));
    $obj->addAssign('pagestop', 'innerHTML', generatePages(0));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages(0));
    return $obj->getXML();
}
