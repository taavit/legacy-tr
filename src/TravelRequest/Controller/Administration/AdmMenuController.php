<?php
namespace Taavit\TravelRequest\Controller\Administration;

/**
 * Obiekt zawiera stronę z menu administratora
 *
 * @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
 * @date 11-01-2008
 * @date 14-02-2017
*/

class AdmMenuController
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function show()
    {
        return $this->twig->render(
            'administration/admmenu.html.twig',
            ['blocked' => $_SESSION['blocked']]
        );
    }
}
