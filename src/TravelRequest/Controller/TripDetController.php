<?php
/// plik zawiera klasę 'tripdet'
/** @file tripdet.php */
namespace Taavit\TravelRequest\Controller;

use Symfony\Component\HttpFoundation\Request;

///Klasa generująca szeczeguły delegacji
        /**
        Klasa generuje szczegóły delegacji
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
        @date 17-11-2007
        */

class TripDetController extends \TPage
{
    ///szablon strony
    protected $twig;
    ///uzywane do okreslenia ktora baza. Rowne albo obnjj mainPDO, albo Arch
    protected $db2;
    ///Konstruktor ustawiający szablon strony, oraz rejestrujący funkcje xajaxowe
    public function __construct($xajax, $twig)
    {
        parent::__construct();
        $this->twig = $twig;
        $xajax->registerFunction('X_AcceptDel');
        $xajax->registerFunction('X_RejectDel');
        $xajax->registerFunction('X_CloseDel');
        $xajax->registerFunction('X_DelDelegation');
    }
    ///Funkcja generująca kod strony
    /**
    @return wygenerowany kod strony
    */
    public function show(Request $request)
    {
        $ok=true;

        if ($request->query->has('id')) {
            if (is_numeric($_GET['id']) && (strpos($_GET['id'], '.')===false) && (strpos($_GET['id'], '.')===false)) {
                $_SESSION['lastActDel']=$request->query->get('id');
                $trip=$this->db->getTrip($request->query->get('id'));
            } else {
                $trip=false;
            }
        } else {
            $trip=$this->db->getTrip($_SESSION['lastActDel']);
        }

        if ($trip === false) {
            $trip=$this->db->getArch()->getTrip($_GET['id']);
            $this->db2=$this->db->getArch();
        } else {
            $this->db2=$this->db;
        }
        if ($trip !== false) {
            if ($this->usr->getIsAdmin()) {
                return $this->showDetails($trip, 3);
            } elseif ($trip->getIdAccept()==$this->usr->getId_logged()) {
                $who = 1;
                if ($trip->getStatus() == 1) {
                    $who = 2;
                }
                return $this->showDetails($trip, $who);
            } elseif ($trip->getIdEmployee() == $this->usr->getId_logged()) {
                return $this->showDetails($trip, 1);
            } else {
                $ok = false;
            }
        } else {
            $ok = false;
        }
    
        if (!$ok) {
            return TR_TEXT_ERROR_PAGE_NOT_ALLOWED;
        }
    }

    ///Metoda generująca kod detali delegacji
    /**
    @param $trip id delegacji której szczegóły chcemy wyświetlić
    @param $who kto chce wyświetlić /3-użytkownik,2-osoba akceptująca,1-administrator
    */
    public function showDetails($trip, $who)
    {
        $data['TR_TRIP_DETAIL_EMPLOYEE'] = TR_TRIP_DETAIL_EMPLOYEE;
        $data['TR_FORM_TRIP_ACCEPT_PERSON'] = TR_FORM_TRIP_ACCEPT_PERSON;
        $data['TR_FORM_TRIP_START_DATE'] = TR_FORM_TRIP_START_DATE;
        $data['TR_FORM_TRIP_END_DATE'] = TR_FORM_TRIP_END_DATE;
        $data['TR_FORM_TRIP_AIM'] = TR_FORM_TRIP_AIM;
        $data['TR_TEXT_COUNTRY_TO'] = TR_TEXT_COUNTRY_TO;
        $data['TR_TEXT_CITY_TO'] = TR_TEXT_CITY_TO;
        $data['TR_TEXT_COUNTRY_FROM'] = TR_TEXT_COUNTRY_FROM;
        $data['TR_TEXT_CITY_FROM'] = TR_TEXT_CITY_FROM;
        $data['TR_FORM_TRIP_TRANSPORT'] = TR_FORM_TRIP_TRANSPORT;
        $data['TR_FORM_TRIP_HOTEL'] = TR_FORM_TRIP_HOTEL;
        $data['TR_TEXT_ADVANCE'] = TR_TEXT_ADVANCE;
        $data['TR_FORM_TRIP_COMMENT2'] = TR_FORM_TRIP_COMMENT2;
        $data['TR_FORM_TRIP_COMMENT'] = TR_FORM_TRIP_COMMENT;
        $data['TR_TEXT_ID'] = TR_TEXT_ID;
        $data['TR_TEXT_TRIP_STATUS'] = TR_TEXT_TRIP_STATUS;

        $data['id'] = 'tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT);
        switch ($trip->getStatus()) {
            case 0:
                $data['status'] = TR_TEXT_TRIP_STATUS_0;
                break;
            case 1:
                $data['status'] = '<span class="swaiting">'.TR_TEXT_TRIP_STATUS_1.'</span>';
                break;
            case 2:
                $data['status'] = '<span class="saccepted">'.TR_TEXT_TRIP_STATUS_2.'</span>';
                break;
            case 3:
                $data['status'] = '<span class="scancelled">'.TR_TEXT_TRIP_STATUS_3.'</span>';
                break;
            case 4:
                $data['status'] = TR_TEXT_TRIP_STATUS_4;
                break;
            case 5:
                $data['status'] = TR_TEXT_TRIP_STATUS_5;
                break;
            case 6:
                $data['status'] = TR_TEXT_TRIP_STATUS_6;
                break;
        }

        $data['name_employee'] = $this->db2->getName($trip->getIdEmployee());
        $data['name_accept'] = $this->db2->getName($trip->getIdAccept());
        $data['start_date'] = $trip->getStartDate();
        $data['end_date'] = $trip->getEndDate();
        $data['purp'] = $this->db2->getPurp($trip->getIdPurp())->getName();
        $c = $this->db2->getCity($trip->getIdCity());
        $data['city'] = $c->getName();
        $data['country'] = $this->db2->getCountry($c->getIdCountry())->getName();
        $c=$this->db2->getCity($trip->getIdStartCity());
        $data['city_from'] = $c->getName();
        $data['country_from'] = $this->db2->getCountry($c->getIdCountry())->getName();
        $data['transp'] = $this->db2->getTransp($trip->getIdTransp())->getName();
        if ($who == 2) {
            $out='<textarea id="comment2" name="comment2" rows=5 cols=50>'.$trip->getDescript2().'</textarea>';
        } else {
            $out=$trip->getDescript2();
        }
        $data['comment2'] = $out;
        $data['comment'] = $trip->getDescript();
        if ($trip->getIfAdvance()) {
            $advance['TR_FORM_TRIP_ADVANCE_VALUE'] = TR_FORM_TRIP_ADVANCE_VALUE;
            // $hot=file_get_contents(__DIR__.'/../../../app/Resources/views/delegation/advance.tpl');
            // $hot=str_replace('TR_FORM_TRIP_ADVANCE_VALUE', TR_FORM_TRIP_ADVANCE_VALUE, $hot);
            $advance['TR_FORM_TRIP_ADVANCE_HOW_TO_PAY'] = TR_FORM_TRIP_ADVANCE_HOW_TO_PAY;
            $advance['advance_value'] = $trip->getAdvance()->getValue();
            $advance['advance_curr'] = $this->db2->getCurr($trip->getAdvance()->getIdCurr())->getName();
            if ($trip->getAdvance()->getIfMoney()) {
                $advance['advance_pay'] = TR_FORM_TRIP_ADVANCE_MONEY;
            } else {
                $advance['advance_pay'] = TR_FORM_TRIP_ADVANCE_NO_MONEY;
            }
            $data['advance'] = $twig->render('delegation/advance.html.twig', $advance);
        } else {
            $data['advance'] = TR_TEXT_IS_ADVANCE_FALSE;
        }
        if ($trip->getIfHotel()) {
            $hot=file_get_contents(__DIR__.'/../../../app/Resources/views/delegation/hotel.tpl');
            $hot=str_replace('TR_TRIP_HOTEL_NAME', TR_TRIP_HOTEL_NAME, $hot);
            $hot=str_replace('TR_FORM_TRIP_HOTEL_START_DATE', TR_FORM_TRIP_HOTEL_START_DATE, $hot);
            $hot=str_replace('TR_FORM_TRIP_HOTEL_END_DATE', TR_FORM_TRIP_HOTEL_END_DATE, $hot);
            $hot=str_replace('TR_FORM_TRIP_HOTEL_DAYS', TR_FORM_TRIP_HOTEL_DAYS, $hot);
            $hot=str_replace('{hotel_name}', $this->db2->getHotel($trip->getTripHotel()->getIdHotel())->getName(), $hot);
            $hot=str_replace('{hotel_start_date}', $trip->getTripHotel()->getStartDate(), $hot);
            $hot=str_replace('{hotel_end_date}', $trip->getTripHotel()->getEndDate(), $hot);
            $hot=str_replace('{hotel_days}', $trip->getTripHotel()->getDays(), $hot);
            $data['hotel'] = $hot;
        } else {
            $data['hotel'] = TR_TEXT_IS_HOTEL_FALSE;
        }
        $out='';
        switch ($who) {
            case 1:
                if ($trip->getStatus() == 0 || $trip->getStatus() == 3) {
                    $button = new TButton;
                    $button->setCaption(TR_TEXT_BUTTON_MOD);
                    $button->setAction('window.location="index2.php?page=TripForm&id='.$trip->getId().'"');
                    $button->setWidth(50);
                    $button2 = new TButton;
                    $button2->setCaption(TR_TEXT_BUTTON_DEL);
                    $button2->setAction('xajax_X_DelDelegation('.$trip->getId().')');
                    $button2->setWidth(50);
                    $out = $button->Show()." &nbsp&nbsp&nbsp".$button2->Show();
                } else {
                    $out='';
                }
                break;
            case 2:
                $accbut=new TButton;
                $rejbut=new TButton;
                $accbut->setCaption(TR_TEXT_BUTTON_ACCEPT);
                $accbut->setAction('xajax_X_AcceptDel('.$trip->getId().',document.getElementById("comment2").value)');
                $rejbut->setCaption(TR_TEXT_BUTTON_REJECT);
                $rejbut->setAction('xajax_X_RejectDel('.$trip->getId().',document.getElementById("comment2").value)');
                $out=$accbut->Show().$rejbut->Show();
                break;
            case 3:
                if ($trip->getStatus()<5) {
                    $button=new \TButton;
                    $button->setCaption(TR_TEXT_BUTTON_CLOSE);
                    $button->setAction('xajax_X_CloseDel('.$trip->getId().')');
                    $button->setWidth(50);
                    $out=$button->Show();
                }
                break;
        }
        $data['buttons'] = $out;
        return $this->twig->render('delegation/delegation_details.html.twig', $data);
    }
}

///Xajaxowa funkcja akceptująca delegację
/**
@param $id id delegacji która ma być zaakceptowana
@param $desc2 komentarz dotyczący tej delegacji
@return kod xml dla xajaxa
*/
function X_AcceptDel($id, $desc2)
{
    $db=new MainPDO;
    $db->connect();
    $t=$db->getTrip($id);
    if ($t->getIdAccept()==$_SESSION['user']->getId_logged() && ($t->getStatus()==1 || $t->getStatus()==3)) {
        $db->setDescript2($id, $desc2, $t->getIdAccept());
        $db->setTripStatus($id, 2);
        $mail=new main_mail;
        $mail->init();
        $mail->sendToEmployee($id);
        $mail->sendToTravelComp($id);
        $mail->sendToFinance($id);
    }
    $obj=new XajaxResponse();
    $obj->addRedirect("index2.php?page=acceptator");
    return $obj->getXML();
}

///Xajaxowa funkcja odrzucająca delegację
/**
@param $id id delegacji która ma być odrzucona
@param $desc2 komentarz dotyczący tej delegacji
@return kod xml dla xajaxa
*/

function X_RejectDel($id, $desc2)
{
    $db=new MainPDO;
    $db->connect();
    $t=$db->getTrip($id);
    if ($t->getIdAccept()==$_SESSION['user']->getId_logged() && ($t->getStatus()==1 || $t->getStatus()==3)) {
        //SEND MAIL
        $db->setDescript2($id, $desc2, $t->getIdAccept());
        $db->setTripStatus($id, 3);
        $mail=new main_mail;
        $mail->init();
        $mail->sendToEmployee($id);
    }
    $obj=new XajaxResponse();
    $obj->addRedirect("index2.php?page=acceptator");
    return $obj->getXML();
}

///Xajaxowa funkcja zamykająca delegację nie będącą kopią roboczą
/**
@param $id id delegacji która ma być zamknięta
@return kod xml dla xajaxa
*/
function X_CloseDel($id)
{
    $obj=new XajaxResponse;
    $db=new MainPDO;
    $db->connect();
    $trip=$db->getTrip($id);
    if ($_SESSION['user']->getIsAdmin() && $trip->getStatus()!=0) {
        $db->closeTrip($id, $_SESSION['user']->getId_logged());
    }
    $obj->addRedirect("index2.php?page=administration&a=delegationman");
    return $obj->getXML();
}
///Xajaxowa funkcja kasująca delegację
/**
@param $id id delegacji która ma być skasowana
@return kod xml dla xajaxa
*/
function X_DelDelegation($id)
{
    $obj=new XajaxResponse;
    $db=new MainPDO;
    $db->connect();
    $trip=$db->getTrip($id);
    if ($_SESSION['user']->getId_logged()==$trip->getIdEmployee() && ($trip->getStatus()==0 || $trip->getStatus()==3)) {
        $db->delTrip($id);
        $obj->addRedirect("index2.php?page=delegation");
    }
    return $obj->getXML();
}
