<?php
/// plik zawiera klasę 'MainMenu'
/** @file MainMenu.php */
namespace Taavit\TravelRequest\Controller;

///Klasa menu głównego
/**
obiekt zawiera stronę menu głównego. Wczytuje zadany moduł panelu
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 11-11-2007
*/

class MainMenuController
{
    ///Tworzenie obiektu
    public function __construct($user, $db, $twig)
    {
        $this->usr = $user;
        $this->twig = $twig;
        $this->db = $db;
    }

    ///Funkcja wyświetlająca stronę
    /**
    @return string zawierający wygenerowaną stronę
    */
    public function show()
    {
        $res='';
        if ($this->db->getAllCountries(true) !== null) {
            $buttons[]=new \TMenuButton(TR_TEXT_MENU_NEW_DELEGATION, '?page=TripForm');
        }
        $buttons[]=new \TMenuButton(TR_TEXT_MENU_DELEGATION_HISTORY, '?page=delegation');
        if ($this->usr->getIsAdmin()) {
            $buttons[]=new \TMenuButton(TR_TEXT_MENU_ADMINISTRATOR_PANEL, '?page=administration');
        }
        if ($this->usr->getIsAccept()) {
            $buttons[]=new \TMenuButton(TR_TEXT_MENU_ACCEPTATOR_PANEL, '?page=acceptator');
        }
        $buttons[]=new \TMenuButton(TR_TEXT_MENU_USER_INFO, '?page=user_info');

        return $this->twig->render('mainmenu.html.twig', ['entries' => $buttons]);
    }
}
