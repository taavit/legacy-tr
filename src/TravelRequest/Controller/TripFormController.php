<?php
/// plik zawiera klasę 'TripForm'
/** @file TripForm.php */
namespace Taavit\TravelRequest\Controller;

///Klasa generująca formularz delegacji
        /**
        Klasa generuje formularz delegacji
        @author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
        @date 17-11-2007
        */

class TripFormController extends \TPage
{
    ///Szablon formularza
    private $twig;

    public function __construct($xajax, $twig)
    {
        parent::__construct();
        $xajax->registerFunction("X_Hotel");
        $xajax->registerFunction(["X_Advance", "X_Advance", self::CLASS]);
        $xajax->registerFunction("X_Country");
        $xajax->registerFunction("X_Country2");
        $this->twig = $twig;
    }
///Funkcja generująca kod strony
/**
@return kod strony
*/
    public function show()
    {
        $popup=0;
        $set=false;
        $data = [];
        
        if (isset($_SESSION['trip'])) {
            $set=true;
            $trip=$_SESSION['trip'];
            unset($_SESSION['trip']);
        }

        if (isset($_GET['id'])) {
            $trip=$this->db->getTrip($_GET['id']);
            if ($trip==false) {
                $_SEESION['msg']=TR_TEXT_ERROR_PAGE_NOT_ALLOWED;
                redirect('index2.php?page=delegation');
            }
            if ($trip->getIdEmployee()==$this->usr->getId_logged() && ($trip->getStatus()==0 || $trip->getStatus()==3)) {
                $set=true;
                $_SESSION['tripid']=$_GET['id'];
            } else {
                $_SEESION['msg']=TR_TEXT_ERROR_PAGE_NOT_ALLOWED;
                redirect('index2.php?page=delegation');
            }
        }

        $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
        $opt='';
        if ($this->usr->getIsAccept()) {
            $var=$this->db->getAllAccept(true);
            $popup=1;
            foreach ($var as $a) {
                if ($a->getId()!=$this->usr->getId_logged()) {
                    $opt.=str_replace('{option}', $a->getForename()." ".$a->getSurname(), $option);
                    $opt=str_replace('{option_value}', $a->getId(), $opt);
                    if ($set && $a->getId()==$trip->getIdAccept()) {
                        $opt=str_replace('{selected}', 'selected="selected"', $opt);
                        $popup=0;
                    } else {
                        $opt=str_replace('{selected}', '', $opt);
                    }
                }
            }
            if ($popup==1 && $set) {
                $popup=2;
            }
        } else {
            $var=$this->db->getEmployee($this->usr->getId_logged());
            if ($popup!=2) {
                $popup=1;
            }
            foreach ($var->getIdAccept() as $a) {
                $opt.=str_replace('{option}', $this->db->getName($a), $option);
                $opt=str_replace('{option_value}', $a, $opt);
                if ($set) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                    if ($popup!=2) {
                        $popup=0;
                    }
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            }
            if ($popup==1 && $set) {
                $popup=2;
            }
        }
        $data['accept_option'] = $opt;

        //date
        if ($set) {
            $data['start_date_value'] = $trip->getStartDate();
            $data['end_date_value'] = $trip->getEndDate();
            $data['comment_value'] = $trip->getDescript();
            if (!$trip->getIfHotel()) {
                $data['hotel_div'] = '';
            }
            if (!$trip->getIfAdvance()) {
                $data['advance_div'] = '';
            }
        } else {
            $data['start_date_value'] = '';
            $data['end_date_value'] = '';
            $data['comment_value'] = '';
            $data['checked_is_hotel'] = '';
            $data['checked_is_advance'] = '';
            $data['hotel_div'] = '';
            $data['advance_div'] = '';
        }
        $opt='';
        $var=$this->db->getAllPurp(true);
        if ($popup!=2) {
            $popup=1;
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set && $trip->getIdPurp()==$a->getId()) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
                if ($popup!=2) {
                    $popup=0;
                }
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
        }
        if ($popup==1 && $set) {
            $popup=2;
        }
        $data['purpose_option'] = $opt;
        $opt='';
        $var=$this->db->getAllTransp(true);
        if ($popup!=2) {
            $popup=1;
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set) {
                if ($a->getId()==$trip->getIdTransp()) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                    if ($popup!=2) {
                        $popup=0;
                    }
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
        }
        $data['transport_sel'] = $opt;

        $opt='';
        $var=$this->db->getAllCountries(true);
        if ($set) {
            $city=$this->db->getCity($trip->getIdCity());
        }
        if ($popup!=2) {
            $popup=1;
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set && $city!=false) {
                if ($a->getId()==$city->getIdCountry()) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                    if ($popup!=2) {
                        $popup=0;
                    }
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
        }
        if ($popup==1 && $set) {
            $popup=2;
        }
        $data['country_option'] = $opt;
        $opt='';
        if ($set) {
            $city_from=$this->db->getCity($trip->getIdStartCity());
        }
        if ($popup!=2) {
            $popup=1;
        }
        $i=0;
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set && $city_from!=false) {
                if ($a->getId()==$city_from->getIdCountry()) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                    if ($popup!=2) {
                        $popup=0;
                    }
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            } elseif ($i==1) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
            $i++;
        }
        if ($popup==1 && $set) {
            $popup=2;
        }
        $data['country_option_from'] = $opt;
        
        $select=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/selectcity.tpl');
        $opt='';
        $war=$var[1];
        if ($set) {
            $var=$this->db->getCitiesOfCountry($city->getIdCountry(), true);
        } else {
            $var=$this->db->getCitiesOfCountry($var[0]->getId(), true);
        }
        if ($popup!=2) {
            $popup=1;
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set&& $a->getId()==$trip->getIdCity()) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
                if ($popup!=2) {
                    $popup=0;
                }
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
        }
        if ($popup==1 && $set) {
            $popup=2;
        }
        $opt=str_replace('{city_option}', $opt, $select);
        $data['select_city'] = $opt;
        $select=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/select.tpl');
        $opt='';
        if ($set && $city_from!=false) {
            $var=$this->db->getCitiesOfCountry($city_from->getIdCountry(), true);
        } else {
            $var=$this->db->getCitiesOfCountry($war->getId(), true);
        }
        if ($popup!=2) {
            $popup=1;
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            if ($set&& $a->getId()==$trip->getIdStartCity()) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
                if ($popup!=2) {
                    $popup=0;
                }
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
        }
        if ($popup==1 && $set) {
            $popup=2;
        }

        $select=str_replace('{select_name}', 'city_from', $select);
        $opt=str_replace('{option}', $opt, $select);
        $data['select_city_from'] = $opt;
        if ($set) {
            $data = array_merge($data, $this->showExt($trip));
        }
        
        $data['TR_FORM_TRIP_ACCEPT_PERSON'] = TR_FORM_TRIP_ACCEPT_PERSON;
        $data['TR_ADD_TRIP'] = TR_ADD_TRIP;
        $data['TR_FORM_TRIP_START_DATE'] = TR_FORM_TRIP_START_DATE;
        $data['TR_FORM_TRIP_END_DATE'] = TR_FORM_TRIP_END_DATE;
        $data['TR_FORM_TRIP_AIM'] = TR_FORM_TRIP_AIM;
        $data['TR_FORM_TRIP_TRANSPORT'] = TR_FORM_TRIP_TRANSPORT;
        $data['TR_FORM_TRIP_IS_HOTEL'] = TR_FORM_TRIP_IS_HOTEL;
        $data['TR_FORM_TRIP_IS_ADVANCE'] = TR_FORM_TRIP_IS_ADVANCE;
        $data['TR_FORM_TRIP_COMMENT'] = TR_FORM_TRIP_COMMENT;
        $data['TR_FORM_TRIP_SEND_FORM'] = TR_FORM_TRIP_SEND_FORM;
        $data['TR_FORM_TRIP_CLEAN_FORM'] = TR_FORM_TRIP_CLEAN_FORM;
        $data['TR_FORM_TRIP_SAVE_FORM'] = TR_FORM_TRIP_SAVE_FORM;
        $data['TR_TEXT_COUNTRY_TO'] = TR_TEXT_COUNTRY_TO;
        $data['TR_TEXT_COUNTRY_FROM'] = TR_TEXT_COUNTRY_FROM;
        $data['TR_TEXT_CITY_TO'] = TR_TEXT_CITY_TO;
        $data['TR_TEXT_CITY_FROM'] = TR_TEXT_CITY_FROM;
        
    
        $data['TR_CALENDAR_LINK'] = TR_CALENDAR_LINK;

        $data['TR_CALENDER_MOUNTH_10'] = '"'.TR_CALENDER_MOUNTH_10.'"';
        $data['TR_CALENDER_MOUNTH_11'] = '"'.TR_CALENDER_MOUNTH_11.'"';
        $data['TR_CALENDER_MOUNTH_12'] = '"'.TR_CALENDER_MOUNTH_12.'"';
        $data['TR_CALENDER_MOUNTH_1'] = '"'.TR_CALENDER_MOUNTH_1.'"';
        $data['TR_CALENDER_MOUNTH_2'] = '"'.TR_CALENDER_MOUNTH_2.'"';
        $data['TR_CALENDER_MOUNTH_3'] = '"'.TR_CALENDER_MOUNTH_3.'"';
        $data['TR_CALENDER_MOUNTH_4'] = '"'.TR_CALENDER_MOUNTH_4.'"';
        $data['TR_CALENDER_MOUNTH_5'] = '"'.TR_CALENDER_MOUNTH_5.'"';
        $data['TR_CALENDER_MOUNTH_6'] = '"'.TR_CALENDER_MOUNTH_6.'"';
        $data['TR_CALENDER_MOUNTH_7'] = '"'.TR_CALENDER_MOUNTH_7.'"';
        $data['TR_CALENDER_MOUNTH_8'] = '"'.TR_CALENDER_MOUNTH_8.'"';
        $data['TR_CALENDER_MOUNTH_9'] = '"'.TR_CALENDER_MOUNTH_9.'"';

        $data['TR_CALENDER_LETTER_OF_DAY_1'] = '"'.TR_CALENDER_LETTER_OF_DAY_1.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_2'] = '"'.TR_CALENDER_LETTER_OF_DAY_2.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_3'] = '"'.TR_CALENDER_LETTER_OF_DAY_3.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_4'] = '"'.TR_CALENDER_LETTER_OF_DAY_4.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_5'] = '"'.TR_CALENDER_LETTER_OF_DAY_5.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_6'] = '"'.TR_CALENDER_LETTER_OF_DAY_6.'"';
        $data['TR_CALENDER_LETTER_OF_DAY_7'] = '"'.TR_CALENDER_LETTER_OF_DAY_7.'"';

        $data['TR_CALENDER_TODAY'] = '"'.TR_CALENDER_TODAY.'"';
        if (isset($_SESSION['msg'])) {
            $data['error'] = $_SESSION['msg'];
            unset($_SESSION['msg']);
        } elseif ($popup==2) {
            $data['error'] = TR_FORM_TRIP_OLD_SETTINGS;
        } else {
            $data['error'] = '';
        }
        return $this->twig->render('forms/tripform.html.twig', $data);
    }
///Funckja generująca kod rozszerzonego formularza
/**
funckja modyfikuje szablon $this->t
*/
    public function showExt($trip)
    {
        $data = [];
        if ($trip->getIfHotel()) {
            $data['checked_is_hotel'] = 'checked="checked"';
            $t=new TTemplate;
            $t->setTplFile(__DIR__.'/../../../app/Resources/views/forms/triphotelform.tpl');
            $h=file_get_contents("templates/forms/selecthotel.tpl");
            $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
            $opt='';
            $t->addRepVar('TR_FORM_TRIP_HOTEL_START_DATE', TR_FORM_TRIP_HOTEL_START_DATE);
            $t->addRepVar('TR_FORM_TRIP_HOTEL_END_DATE', TR_FORM_TRIP_HOTEL_END_DATE);
            $t->addRepVar('TR_FORM_TRIP_HOTEL_DAYS', TR_FORM_TRIP_HOTEL_DAYS);
            $t->addRepVar('TR_FORM_TRIP_HOTEL', TR_FORM_TRIP_HOTEL);
            $t->addRepVar('TR_CALENDAR_LINK', TR_CALENDAR_LINK);
            $t->addRepVar('TR_TRIP_COUNT_DAYS', TR_TRIP_COUNT_DAYS);

            $t->addRepVar('{hotel_end_date_value}', $trip->getTripHotel()->getEndDate());
            $t->addRepVar('{hotel_start_date_value}', $trip->getTripHotel()->getStartDate());
            $t->addRepVar('{hotel_days_value}', $trip->getTripHotel()->getDays());
            $var=$this->db->getHotelByCityId($trip->getIdCity(), true);
            foreach ($var as $a) {
                $opt.=str_replace('{option}', $a->getName(), $option);
                $opt=str_replace('{option_value}', $a->getId(), $opt);
                if ($a->getId()==$trip->getTripHotel()->getIdHotel()) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            }
            $h=str_replace('{hotel_option}', $opt, $h);
            $t->addRepVar('{selecthotel}', $h);
            $t->prepare();
            $data['hotel_div'] = $t->getOutputText();
        } else {
            $data['checked_is_hotel'] = '';
        }
            
        if ($trip->getIfAdvance()) {
            $data['checked_is_advance'] = 'checked="checked"';
            $t=new TTemplate;
            $t->setTplFile(__DIR__.'/../../../app/Resources/views/forms/tripadvanceform.tpl');
            $t->addRepVar('{advance_val}', $trip->getAdvance()->getValue());
            $t->addRepVar('TR_FORM_TRIP_ADVANCE_VALUE', TR_FORM_TRIP_ADVANCE_VALUE);
            $t->addRepVar('TR_FORM_TRIP_ADVANCE_MONEY', TR_FORM_TRIP_ADVANCE_MONEY);
            $t->addRepVar('TR_FORM_TRIP_ADVANCE_HOW_TO_PAY', TR_FORM_TRIP_ADVANCE_HOW_TO_PAY);
    
            $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
            $opt='';
            $var=$this->db->getAllCurr(true);
            if (count($var)==0) {
                return $obj->getXML();
            }
            foreach ($var as $a) {
                $opt.=str_replace('{option}', $a->getName(), $option);
                $opt=str_replace('{option_value}', $a->getId(), $opt);
                if ($a->getId()==$trip->getAdvance()->getIdCurr()) {
                    $opt=str_replace('{selected}', 'selected="selected"', $opt);
                } else {
                    $opt=str_replace('{selected}', '', $opt);
                }
            }
            $t->addRepVar('{currency_option}', $opt);


            if ($trip->getAdvance()->getIfMoney()) {
                $t->addRepVar('{is_money_checked}', "checked='checked'");
            } else {
                $t->addRepVar('{is_money_checked}', '');
            }

            $opt='';
            // 0 -przelew
            $opt.=str_replace('{option}', TR_FORM_TRIP_ADVANCE_NO_MONEY, $option);
            $opt=str_replace('{option_value}', 0, $opt);
            if ($trip->getAdvance()->getIfMoney()==0) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
            //1 - gotowka
            $opt.=str_replace('{option}', TR_FORM_TRIP_ADVANCE_MONEY, $option);
            $opt=str_replace('{option_value}', 1, $opt);
            if ($trip->getAdvance()->getIfMoney()==1) {
                $opt=str_replace('{selected}', 'selected="selected"', $opt);
            } else {
                $opt=str_replace('{selected}', '', $opt);
            }
            $t->addRepVar('{money_radio}', 'ale');
            $t->prepare();
            $data['advance_div'] = $t->getOutputText();
        } else {
            $data['checked_is_advance'] = '';
        }
        return $data;
    }
    ///Xajaxowa funkcja wyświetlająca dane zaliczki
    /**
    @return xml dla xajaxa, zawierajcy dane zaliczki
    */
    public static function X_Advance()
    {
        $obj=new xajaxResponse();
        $db=new MainPDO;
        $db->connect();
        $t=new TTemplate;
        $t->setTplFile(__DIR__.'/../../../app/Resources/views/forms/tripadvanceform.tpl');
        $t->addRepVar('{advance_val}', '');
        $t->addRepVar('TR_FORM_TRIP_ADVANCE_VALUE', TR_FORM_TRIP_ADVANCE_VALUE);
        $t->addRepVar('TR_FORM_TRIP_ADVANCE_MONEY', TR_FORM_TRIP_ADVANCE_MONEY);
        $t->addRepVar('TR_FORM_TRIP_ADVANCE_HOW_TO_PAY', TR_FORM_TRIP_ADVANCE_HOW_TO_PAY);

        $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
        $opt='';
        $var=$db->getAllCurr(true);
        if (count($var)==0) {
            return $obj->getXML();
        }
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            $opt=str_replace('{selected}', '', $opt);
        }
        $t->addRepVar('{currency_option}', $opt);
        $opt='';

        // 0 -przelew
        $opt.=str_replace('{option}', TR_FORM_TRIP_ADVANCE_NO_MONEY, $option);
        $opt=str_replace('{option_value}', 0, $opt);
        $opt=str_replace('{selected}', '', $opt);
        //1 - gotowka
        $opt.=str_replace('{option}', TR_FORM_TRIP_ADVANCE_MONEY, $option);
        $opt=str_replace('{option_value}', 1, $opt);
        $opt=str_replace('{selected}', '', $opt);
        $t->addRepVar('{money_radio}', $opt);

        
        $t->prepare();
        $obj->addAssign('advance', 'innerHTML', $t->getOutputText());
        return $obj->getXML();
    }
}

///Xajaxowa funkcja wyświetlająca parametry hotelu
/**
@param $id_city id miasta
@param $start data startowa pobytu w hotelu
@param $end data zakończenia pobytu w hotelu
@return xml dla xajaxa, zawierajcy dane listy
*/
function X_Hotel($id_city, $start, $end)
{
    $db=new MainPDO;
    $db->connect();
    $t=new TTemplate;
    $obj=new xajaxResponse();
    $t->setTplFile(__DIR__.'/../../../app/Resources/views/forms/triphotelform.tpl');
    $h=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/selecthotel.tpl');
    $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
    $opt='';
    $t->addRepVar('TR_FORM_TRIP_HOTEL_START_DATE', TR_FORM_TRIP_HOTEL_START_DATE);
    $t->addRepVar('TR_FORM_TRIP_HOTEL_END_DATE', TR_FORM_TRIP_HOTEL_END_DATE);
    $t->addRepVar('TR_FORM_TRIP_HOTEL_DAYS', TR_FORM_TRIP_HOTEL_DAYS);
    $t->addRepVar('TR_FORM_TRIP_HOTEL', TR_FORM_TRIP_HOTEL);
    $t->addRepVar('TR_CALENDAR_LINK', TR_CALENDAR_LINK);
    $t->addRepVar('TR_TRIP_COUNT_DAYS', TR_TRIP_COUNT_DAYS);

    $t->addRepVar('{hotel_end_date_value}', $end);
    $t->addRepVar('{hotel_start_date_value}', $start);
    $t->addRepVar('{hotel_days_value}', '');
    $var=$db->getHotelByCityId($id_city, true);
    if (count($var)==0) {
        $obj->addAssign('hotel', 'innerHTML', TR_FORM_TRIP_NO_HOTEL_IN_THIS_CTIY);
        return $obj->getXML();
    }
    foreach ($var as $a) {
        $opt.=str_replace('{option}', $a->getName(), $option);
        $opt=str_replace('{option_value}', $a->getId(), $opt);
        $opt=str_replace('{selected}', '', $opt);
    }
    $h=str_replace('{hotel_option}', $opt, $h);
    $t->addRepVar('{selecthotel}', $h);
    $t->addRepVar('{hotel_days_value}', '');
    $t->prepare();
    $obj->addAssign('hotel', 'innerHTML', $t->getOutputText());
    return $obj->getXML();
}


///Xajaxowa funkcja wyświetlająca hotele z danego państwa
/**
@param $id_country id państwa
@param $is_hotel
@return xml dla xajaxa, zawierajcy dane listy
*/
function X_Country($id_country, $is_hotel)
{
    $db=new MainPDO;
    $db->connect();
    $obj=new xajaxResponse();
    $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
    $select=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/selectcity.tpl');
    $h=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/selecthotel.tpl');
    $opt='';
    $var=$db->getCitiesOfCountry($id_country, true);
    if (count($var)==0) {
        $opt.=str_replace('{option}', '', $option);
        $opt=str_replace('{option_value}', -1, $opt);
        $opt=str_replace('{selected}', '', $opt);
    } else {
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            $opt=str_replace('{selected}', '', $opt);
        }
    }
    $opt=str_replace('{city_option}', $opt, $select);
    

    $obj->addAssign('cityd', 'innerHTML', $opt);
    if ($is_hotel!='false') {
        $opt='';
        if (count($var)!=0) {
            $v=$db->getHotelByCityId($var[0]->getId(), true);
            if (count($var)==0) {
                $opt.=str_replace('{option}', '', $option);
                $opt=str_replace('{option_value}', -1, $opt);
                $opt=str_replace('{selected}', '', $opt);
            } else {
                foreach ($v as $a) {
                    $opt.=str_replace('{option}', $a->getName(), $option);
                    $opt=str_replace('{option_value}', $a->getId(), $opt);
                    $opt=str_replace('{selected}', '', $opt);
                }
            }
        } else {
            $opt.=str_replace('{option}', '', $option);
            $opt=str_replace('{option_value}', -1, $opt);
            $opt=str_replace('{selected}', '', $opt);
        }
        $h=str_replace('{hotel_option}', $opt, $h);
        $obj->addAssign('selecthotel', 'innerHTML', $h);
    }
    return $obj->getXML();
}


///Xajaxowa funkcja wyświetlająca miasta zdanego państwa
/**
@param $id_country id państwa
@return kod xml dla xajaxa
*/
function X_Country2($id_country)
{
    $db=new MainPDO;
    $db->connect();
    $obj=new xajaxResponse();
    $option=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/formoption.tpl');
    $select=file_get_contents(__DIR__.'/../../../app/Resources/views/forms/select.tpl');
    $opt='';
    $var=$db->getCitiesOfCountry($id_country, true);
    if (count($var)==0) {
        $opt.=str_replace('{option}', '', $option);
        $opt=str_replace('{option_value}', -1, $opt);
        $opt=str_replace('{selected}', '', $opt);
    } else {
        foreach ($var as $a) {
            $opt.=str_replace('{option}', $a->getName(), $option);
            $opt=str_replace('{option_value}', $a->getId(), $opt);
            $opt=str_replace('{selected}', '', $opt);
        }
    }
    $select=str_replace('{select_name}', 'city_from', $select);
    $opt=str_replace('{option}', $opt, $select);

    $obj->addAssign('cityd_from', 'innerHTML', $opt);
    return $obj->getXML();
}
