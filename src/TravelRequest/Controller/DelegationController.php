<?php
/// plik zawiera klasę 'delegation'
/** @file delegation.php */
namespace Taavit\TravelRequest\Controller;

///Klasa zawierająca stronę z historią delegacji
/**
@author Anna Iga Okrasa (isia8578@gmail.com), Dawid Królak (taavit@gmail.com)
@date 12-11-2007
*/

use Taavit\TravelRequest\Model\TripHotel;
use Taavit\TravelRequest\Model\Advance;
use Taavit\TravelRequest\Model\Trip;

class DelegationController
{
    private $page;
    ///Konstruktor strony, inicjalizujący xajaxowe funkcje X_ShowGrid oraz X_DelDel
    public function __construct($user, $xajax, $twig)
    {
        $this->usr = $user;
        $this->twig = $twig;
        $xajax->registerFunction('X_ShowGrid');
        $xajax->registerFunction('X_DelDel'); //DELete DELegation
        isset($_GET['page']) ? $this->page=1 : $this->page=$_GET['page'];
    }
    
    public function Show()
    {
        if (!isset($_GET['a'])) {
            $_GET['a']='';
        }
        if (!isset($_GET['s'])) {
            $_GET['s']=-1;
        }
        switch ($_GET['a']) {
            case 'add':
                if ($_POST['form_button']==TR_FORM_TRIP_SEND_FORM) {
                    $m=$this->addDelegation();
                } elseif ($_POST['form_button']==TR_FORM_TRIP_SAVE_FORM) {
                    $m=$this->saveDelegation();
                }
                $data['grid'] = ShowDelBox(0);
                $data['filtres'] = generateFiltres();
                if (isset($_SESSION['msg'])) {
                    $data['error2'] = $_SESSION['msg'];
                } else {
                    $data['error2'] = '';
                }
                $data['pages'] = generatePages();
                return $this->twig->render('delegation.html.twig', $data);
            case 'tripdet':
                return LoadnRunMod('', 'tripdet');
            
            default:
            $data['grid'] = ShowDelBox(0);
            $data['filtres'] = generateFiltres();
            if (isset($_SESSION['trip_from_msg'])) {
                unset($_SESSION['msg']);
                $_SESSION['ERR'] = $_SESSION['trip_from_msg'];
                unset($_SESSION['trip_from_msg']);
                $data['error2'] = '';
            } elseif (isset($_SESSION['msg'])) {
                $data['error2'] = $_SESSION['msg'];
            } else {
                $data['error2'] = '';
            }
            $data['pages'] = generatePages();
            return $this->twig->render('delegation.html.twig', $data);
        }
    }

///Funkcja dodająca delegacje na podstawie danych z tablicy $_POST
/**
///ewentualny komunikat o błędzie zostaje zapisany do zmiennej sesyjnej $_SESSION['msg']
*/
    public function addDelegation()
    {
        $set=false;
        $trip=new Trip;
        $check=false;
        $msg='';
        if (isset($_SESSION['tripid'])) {
            $trip->setId($_SESSION['tripid']);
        }
        $trip->setIdEmployee($this->usr->getId_logged());
        if (isset($_POST['accept'])) {
            $trip->setIdAccept($_POST['accept']);
        } else {
            $check=true;
            $msg=TR_FORM_TRIP_NO_ID_ACCEPT;
        }
        if (isset($_POST['aim'])) {
            $trip->setIdPurp(strip_tags($_POST['aim']));
        }
        if (isset($_POST['start_date'])) {
            $date=explode('-', $_POST['start_date']);
            if (count($date)!=3) {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_START_DATA_TYPE;
                $b=false;
            } else {
                $b=true;
            }
            if ($b) {
                if (checkdate($date[1], $date[2], $date[0])) {
                    $trip->setStartDate($_POST['start_date']);
                    $start_dated=mktime(0, 0, 0, $date[1], $date[2], $date[0]);
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_WRONG_START_DATA_TYPE;
                }
            }
        } else {
            $check=true;
            $msg=TR_FORM_TRIP_WRONG_START_DATA_TYPE;
        }
        
        if ($_POST['end_date']!='') {
            $date=explode('-', $_POST['end_date']);
            if (count($date)!=3) {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
                $b=false;
            } else {
                $b=true;
            }
            if ($b) {
                if (checkdate($date[1], $date[2], $date[0])) {
                    $trip->setEndDate($_POST['end_date']);
                    $end_dated=mktime(0, 0, 0, $date[1], $date[2], $date[0]);
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
                }
            } else {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
            }
        } else {
            $check=true;
            $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
        }

        if (!$check) {
            if ($end_dated-$start_dated<=0) {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_ORDER_DATES;
            }
        }

        
        if (isset($_POST['is_advance'])) {
            if ($_POST['is_advance']=='on') {
                $trip->setIfAdvance(true);
                $adv=new Advance;
                if ($trip->getId()!=false) {
                    $adv->setIdTrip($trip->getId());
                }
                if ($_POST['advance_value']!='') {
                    //walidacja kwoty
                    if (is_numeric($_POST['advance_value']) && (strpos($_POST['advance_value'], '.')===false) && (strpos($_POST['advance_value'], '.')===false)) {
                        $adv->setValue($_POST['advance_value']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_ADVANCE_VALUE_MUST_BE_NUMERIC;
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_ADVANCE_VALUE;
                }
                if ($_POST['currency']!='') {
                    $adv->setIdCurr($_POST['currency']);
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_CURRENCY_ID;
                }
                if ($_POST['money_radio']==1) {
                    $adv->setIfMoney(true);
                } else {
                    $adv->setIfMoney(false);
                }
                $trip->setAdvance($adv);
            }
        } else {
            $trip->setIfAdvance(false);
        }

        if (isset($_POST['is_hotel'])) {
            if ($_POST['is_hotel']=='on') {
                $h=new TripHotel;
                if ($trip->getId()!=false) {
                    $h->setIdTrip($trip->getId());
                }
                $trip->setIfHotel(true);
                if (isset($_POST['hotelsel'])) {
                    if (isset($_POST['city'])&&$_POST['city']>=0) {
                        $dbh=$this->db->getHotel($_POST['hotelsel']);
                        if ($dbh->getIdCity()==$_POST['city']) {
                            $h->setIdHotel($_POST['hotelsel']);
                        } else {
                            $check=true;
                            $msg=TR_FORM_TRIP_NO_MATCH_HOTEL_CITY;
                        }
                        if ($check) {
                            $h->setIdHotel($_POST['hotelsel']);
                        }
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_HOTEL_ID;
                }

                if (isset($_POST['hotel_start_date'])!='') {
                    $date=explode('-', $_POST['hotel_start_date']);
                    if (count($date)!=3) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE;
                        $b=false;
                    } else {
                        $b=true;
                    }
                    if ($b) {
                        if (checkdate($date[1], $date[2], $date[0])) {
                            $h->setStartDate(new \DateTime($_POST['hotel_start_date']));
                            $start_date=mktime(0, 0, 0, $date[1], $date[2], $date[0]);
                        } else {
                            $check=true;
                            $msg=TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE;
                        }
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE;
                }


                if (isset($_POST['hotel_end_date'])!='') {
                    $date=explode('-', $_POST['hotel_end_date']);
                    if (count($date)!=3) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE;
                        $b=false;
                    } else {
                        $b=true;
                    }
                    if ($b) {
                        if (checkdate($date[1], $date[2], $date[0])) {
                            $h->setEndDate(new \DateTime($_POST['hotel_end_date']));
                            $end_date=mktime(0, 0, 0, $date[1], $date[2], $date[0]);
                        } else {
                            $check=true;
                            $msg=TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE;
                        }
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE;
                }

                if (!$check) {
                    if ($end_date-$start_date<=0) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_ORDER_DATES;
                    }
                }
                if (!$check) {
                    if ($start_dated-$start_date<0) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_TRIP_ORDER_START_DATES;
                    }
                }
                if (!$check) {
                    if ($end_dated-$end_date<0) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_TRIP_ORDER_END_DATES;
                    }
                }
                
                if (isset($_POST['hotel_days'])) {
                    if ($_POST['hotel_days']!='') {
                        if (is_numeric($_POST['hotel_days'])  && (strpos($_POST['hotel_days'], '.')===false) && (strpos($_POST['hotel_days'], '.')===false)) {
                            $temp=($end_date-$start_date)/(60*60*24);
                            if ($temp>$_POST['hotel_days']+1 || $temp<$_POST['hotel_days']-1) {
                                $check=true;
                                $msg=TR_FORM_TRIP_BAD_HOTEL_DAYS;
                            }
                            $h->setDays($_POST['hotel_days']);
                        } else {
                            $check=true;
                            $msg=TR_FORM_TRIP_DAYS_MUST_BE_NUMERIC;
                        }
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_NO_HOTEL_DAYS;
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_HOTEL_DAYS;
                }
                $trip->setTripHotel($h);
            }
        } else {
            $trip->setIfHotel(false);
        }
        
        if (isset($_POST['tran_radio'])) {
            $trip->setIdTransp($_POST['tran_radio']);
        } else {
            $check=true;
            $msg=TR_FORM_TRIP_NO_TRIP_ID;
        }
        //sprawdz czy dane mistao jest w danym panstwie
        if (isset($_POST['city']) && isset($_POST['country'])) {
            if ($_POST['city']!='' && $_POST['country']!='') {
                $c=$this->db->getCity($_POST['city']);
                if ($c!=false) {
                    if ($c->getIdCountry()==$_POST['country']) {
                        $trip->setIdCity($_POST['city']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_NO_MATCH_CITY_COUNTRY;
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_CITY_OR_COUNTRY;
                }
            } else {
                $check=true;
                $msg=TR_FORM_TRIP_NO_CITY_OR_COUNTRY;
            }
        }
        
        if (isset($_POST['city_from']) && isset($_POST['country_from'])) {
            if ($_POST['city_from']!='' && $_POST['country_from']!='') {
                $c=$this->db->getCity($_POST['city_from']);
                if ($c!=false) {
                    if ($c->getIdCountry()==$_POST['country_from']) {
                        $trip->setIdStartCity($_POST['city_from']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_NO_MATCH_CITY_COUNTRY;
                    }
                } else {
                    $check=true;
                    $msg=TR_FORM_TRIP_NO_CITY_OR_COUNTRY;
                }
            } else {
                $check=true;
                $msg=TR_FORM_TRIP_NO_CITY_OR_COUNTRY;
            }
        }
        if ($trip->getIdCity()!='') {
            if ($trip->getIdCity()==$trip->getIdStartCity()) {
                $check=true;
                $msg=TR_FORM_TRIP_CITY_FROM_CITY_TO_ARE_THE_SAME;
            }
        }
        $trip->setStatus(1);

        if (isset($_POST['comment'])) {
            $trip->setDescript(htmlspecialchars(strip_tags(trim($_POST['comment']))));
        }
        if ($check) {
            $_SESSION['trip']=$trip;
            $_SESSION['msg']=$msg;
            redirect('index2.php?page=TripForm');
        } else {
            if (isset($_SESSION['tripid'])) {
                $ret=$this->db->setTrip($trip);
                unset($_SESSION['tripid']);
                $set=true;
            } else {
                $ret=$this->db->addTrip($trip);
            }
            $_SESSION['msg']= TR_TRIP_FORM_DELEGATION_REQUEST_FAILED;
            if ($ret==1) {
                $_SESSION['trip_from_msg']= TR_TRIP_FORM_DELEGATION_REQUEST_SEND;
                unset($_SESSION['msg']);
                $mail=new \main_mail;
                $mail->init();
                if (!$set) {
                    $id=$this->db->getIdOfTrip($trip);
                    $id=$id[0]['id'];
                } else {
                    $id=$trip->getId();
                }
                // $mail->sendToSupervisor($id);
            }
            if ($ret==-1) {
                $_SESSION['msg']= TR_TRIP_FORM_DELEGATION_REQUEST_ALREADY_SENT_TODAY;
            }
            redirect('index2.php?page=delegation');
        }
    }

///Funkcja zapisująca zmienioną delegacje na podstawie danych z tablicy $_POST
/**
@return ewentualny komunikat o błędzie
*/
    public function saveDelegation()
    {
        $trip=new Trip;
        $check=false;
        if (isset($_SESSION['tripid'])) {
            $trip->setId($_SESSION['tripid']);
        }
        $trip->setIdEmployee($this->usr->getId_logged());
        if ($_POST['accept']!='') {
            $trip->setIdAccept($_POST['accept']);
        }
        if ($_POST['start_date']!='') {
            $date=explode('-', $_POST['start_date']);
            if (count($date)!=3) {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_START_DATA_TYPE;
            }
            if (checkdate($date[1], $date[2], $date[0])) {
                $trip->setStartDate($_POST['start_date']);
            } else {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_START_DATA_TYPE;
            }
        }
        if ($_POST['end_date']!='') {
            $date=explode('-', $_POST['end_date']);
            if (count($date)!=3) {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
            }
            if (checkdate($date[1], $date[2], $date[0])) {
                $trip->setEndDate($_POST['end_date']);
            } else {
                $check=true;
                $msg=TR_FORM_TRIP_WRONG_END_DATA_TYPE;
            }
        }
        $trip->setIdPurp(strip_tags($_POST['aim']));
        $trip->setIdCity($_POST['city']);
        $trip->setIdStartCity($_POST['city_from']);
        $trip->setDescript(htmlspecialchars(strip_tags(trim($_POST['comment']))));
        $trip->setIdTransp($_POST['tran_radio']);
        if (isset($_POST['is_hotel'])) {
            if ($_POST['is_hotel']=='on') {
                $trip->setIfHotel(true);
                $h=new TripHotel;
                if ($trip->getId()!=false) {
                    $h->setIdTrip($trip->getId());
                }
                $h->setIdHotel($_POST['hotelsel']);
                if ($_POST['hotel_start_date']!='') {
                    $date=explode('-', $_POST['hotel_start_date']);
                    if (count($date)!=3) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE;
                    }
                    if (checkdate($date[1], $date[2], $date[0])) {
                        $h->setStartDate($_POST['hotel_start_date']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_START_DATA_TYPE;
                    }
                }
                if ($_POST['hotel_end_date']!='') {
                    $date=explode('-', $_POST['hotel_end_date']);
                    if (count($date)!=3) {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE;
                    }
                    if (checkdate($date[1], $date[2], $date[0])) {
                        $h->setEndDate($_POST['hotel_end_date']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_WRONG_HOTEL_END_DATA_TYPE;
                    }
                }
            
                if ($_POST['hotel_days']!='') {
                    if (is_numeric($_POST['hotel_days'])  && (strpos($_POST['hotel_days'], '.')===false) && (strpos($_POST['hotel_days'], '.')===false)) {
                        $h->setDays($_POST['hotel_days']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_DAYS_MUST_BE_NUMERIC;
                    }
                }
                $trip->setTripHotel($h);
            } else {
                $trip->setIfHotel(false);
            }
        }
        if (isset($_POST['is_advance'])) {
            if ($_POST['is_advance']=='on') {
                $trip->SetIfAdvance(true);
                $adv=new Advance;
                if ($trip->getId()!=false) {
                    $adv->setIdTrip($trip->getId());
                }
                if ($_POST['advance_value']!='') {
                    if (is_numeric($_POST['advance_value']) && (strpos($_POST['advance_value'], '.')===false) && (strpos($_POST['advance_value'], '.')===false)) {
                        $adv->setValue($_POST['advance_value']);
                    } else {
                        $check=true;
                        $msg=TR_FORM_TRIP_ADVANCE_VALUE_MUST_BE_NUMERIC;
                    }
                }
                $adv->setIdCurr($_POST['currency']);
                $trip->setAdvance($adv);
            } else {
                $trip->setIfAdvance(false);
            }
        }
        $trip->setStatus(0);
        

        if (isset($_SESSION['tripid'])) {
            $ret=$this->db->setTrip($trip);
            $_SESSION['trip_from_msg']=TR_TRIP_FORM_DELEGATION_REQUEST_SAVED;
            unset($_SESSION['tripid']);
            redirect('index2.php?page=delegation');
        } elseif ($check) {
            $_SESSION['trip']=$trip;
            $_SESSION['msg']=$msg;
            redirect('index2.php?page=TripForm');
        } else {
            $ret=$this->db->addTrip($trip);
        }
        if ($ret==1) {
            $_SESSION['trip_from_msg']=TR_TRIP_FORM_DELEGATION_REQUEST_SAVED;
            redirect('index2.php?page=delegation');
        }
        if ($ret==-1) {
            $_SESSION['trip']=$trip;
            $_SESSION['msg']=TR_TRIP_FORM_DELEGATION_REQUEST_ALREADY_SENT_TODAY;
            redirect('index2.php?page=TripForm');
        }
        return TR_TRIP_FORM_DELEGATION_REQUEST_FAILED;
    }
}

    function ShowDelBox($start=0, $status=array(1,1,1,1,1,0,0), $message='')
    {
        $db=new \MainPDO;
        $db->connect();
        $i=0;
        $ld=new \TDataGrid('ListTrips');
        $trips=$db->getSomeTripByTStatusAndUser($status, CS_ROWS_PER_PAGE, $start, $_SESSION['user']->getId_logged());
        $buttons=array();
        $headers[]=TR_TEXT_ID;
        $headers[]=TR_TEXT_ACCEPTATOR;
        $headers[]=TR_TEXT_CITY_FROM;
        $headers[]=TR_TEXT_COUNTRY_FROM;
        $headers[]=TR_TEXT_CITY_TO;
        $headers[]=TR_TEXT_COUNTRY_TO;
        $headers[]=TR_TEXT_TRIP_START_DATE;
        $headers[]=TR_TEXT_TRIP_END_DATE;
        $headers[]=TR_TEXT_TRIP_STATUS;
        $headers[]=TR_TEXT_TRIP_PURPOSE;


        $data=array();
        $ids;
        foreach ($trips as $trip) {
            $data[$i][]='tr'.str_pad($trip->getId(), 5, '0', STR_PAD_LEFT);
            $data[$i][]=$db->getName($trip->getIdAccept());
            $city=$db->getCity($trip->getIdCity());
            $data[$i][]=$city->getName();
            $data[$i][]=$db->getCountry($city->getIdCountry())->getName();
            $city=$db->getCity($trip->getIdStartCity());
            $data[$i][]=$city->getName();
            $data[$i][]=$db->getCountry($city->getIdCountry())->getName();
            $button=new \TButton;
            $button->setCaption(TR_TEXT_BUTTON_DETAILS);
            $button->setAction('redirect("index2.php?page=delegation&a=tripdet&id='.$trip->getId().'")');
            $button->setWidth(50);
            $buttons[$i][]=$button;
            if ($trip->getStatus()==0 || $trip->getStatus()==3) {
                $button2=new \TButton;
                $button2->setWidth(50);
                $button2->setCaption(TR_TEXT_BUTTON_MOD);
                $button2->setAction('redirect("index2.php?page=TripForm&id='.$trip->getId().'")');
                $buttons[$i][]=$button2;
                $button3=new \TButton;
                $button3->setWidth(50);
                $button3->setCaption(TR_TEXT_BUTTON_DEL);
                $button3->setAction('xajax_X_DelDel('.$trip->getId().','.$start.',generateFilter())');
                $buttons[$i][]=$button3;
                $buttons[$i][0]->setEndline(true);
            }
            $data[$i][]=$trip->getStartDate();
            $data[$i][]=$trip->getEndDate();
            switch ($trip->getStatus()) {
                case 0:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_0;
                        break;
                case 1:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_1;
                        break;
                case 2:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_2;
                        break;
                case 3:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_3;
                        break;
                case 4:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_4;
                        break;
                case 5:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_5;
                        break;
                case 6:
                        $data[$i][]=TR_TEXT_TRIP_STATUS_6;
                        break;
            }
            $data[$i][]=$db->getPurp($trip->getIdPurp())->getName();

            $ids[]=$trip->getId();
            $i++;
        }

        $ld->setHeaders($headers);
        $ld->setData($data);
        $ld->setButtons($buttons);
        return $ld->Show();
    }

function generateFiltres()
{
    $content='<div id="filters">';
    $content.='<input type="checkbox" name="s0" id="s0" checked onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_0;
    $content.='<input type="checkbox" name="s1" id="s1" checked onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_1;
    $content.='<input type="checkbox" name="s2" id="s2" checked onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_2;
    $content.='<input type="checkbox" name="s3" id="s3" checked onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_3;
    $content.='<input type="checkbox" name="s4" id="s4" checked onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_4;
    $content.='<input type="checkbox" name="s5" id="s5" onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_5;
    $content.='<input type="checkbox" name="s6" id="s6" onclick="xajax_X_ShowGrid(0,generateFilter())"/>'.TR_TEXT_TRIP_STATUS_6;
    $content.='</div>';
    return $content;
}

function generatePages(array $status = [1, 1, 1, 1, 1, 0, 0], $act = 0)
{
    $db=new \MainPDO;
    $db->connect();
    $ap=0;
    $cpages=ceil($db->countTripByTStatusAndUser($status, $_SESSION['user']->getId_logged()) / CS_ROWS_PER_PAGE);
    $act=ceil($act/CS_ROWS_PER_PAGE);
    $links='<span class="pageslist" onclick="xajax_X_ShowGrid(0,generateFilter());">'. TR_TEXT_PAGE_FIRST .' </span>';
    $prev=$act-1;
    if ($act!=0) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowGrid('.$prev*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_PREVIOUS .' </span>';
    }

    if ($act<5) {
        $beg=0;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $beg=$act-4;
        if ($act+5<$cpages) {
            $end=$act+5;
        } else {
            $end=$cpages;
        }
    }
    if ($act>=5) {
        $links.='<span class="pageslist"> ...</span>';
    }
    for ($i=$beg;$i<$end;$i++) {
        $ap=$i+1;
        $active='';
        if ($i==$act) {
            $active='class="activePage" style="font-weight:bolder;text-decoration:underline;"';
        } else {
            $active='class="nonActivePage"';
        }
        $links.='<span class="pageslist" onclick="xajax_X_ShowGrid('. $i*CS_ROWS_PER_PAGE .',generateFilter());"'.$active.' >'. $ap .' </span>';
    }
    $next=$act+1;
    if ($act<$cpages-5) {
        $links.='<span class="pageslist"> ...</span>';
    }
    if ($act!=$cpages-1) {
        $links.='<span class="pageslist" onclick="xajax_X_ShowGrid('.$next*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_NEXT .' </span>';
    } else {
        $links.='<span class="pageslist" style="color:gray">'. TR_TEXT_PAGE_NEXT .' </span>';
    }
    $links.='<span class="pageslist" onclick="xajax_X_ShowGrid('.($cpages-1)*CS_ROWS_PER_PAGE.',generateFilter());">'. TR_TEXT_PAGE_LAST .' </span>';
    
    return $links;
}


function X_DelDel($id, $st, $status)
{
    $obj=new XajaxResponse;
    $db=new MainPDO;
    $db->connect();
    $trip=$db->getTrip($id);
    if ($_SESSION['user']->getId_logged()==$trip->getIdEmployee() && ($trip->getStatus()==0 || $trip->getStatus()==3)) {
        $db->delTrip($id);
    }
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}

function X_ShowGrid($st, $status=array(1,1,1,1,1,1,1), $msg='')
{
    $obj=new XajaxResponse;
    $obj->addAssign('grid', 'innerHTML', ShowDelBox($st, $status, $msg));
    $obj->addAssign('pagestop', 'innerHTML', generatePages($status, $st));
    $obj->addAssign('pagesbottom', 'innerHTML', generatePages($status, $st));
    return $obj->getXML();
}
